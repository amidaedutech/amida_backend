module.exports = {
    apps: [{
        name: 'SkillAdda API',
        script: './bin/www',
        watch: true,
        env: {
            NODE_ENV: 'development',
            PORT: '8888',
            NODE_HOST: 'http://localhost',
            CLIENT_HOST: 'http://localhost:4200'
        },
        env_production: {
            NODE_ENV: 'production',
            PORT: '8888',
            NODE_HOST: 'https://SkillAddaedutech.com',
            CLIENT_HOST: 'https://SkillAddaedutech.com'
        }
    }],

    // deploy: {
    //     production: {
    //         user: 'node',
    //         host: '148.66.134.185',
    //         ref: 'origin/master',
    //         repo: 'git@github.com:repo.git',
    //         path: '/var/www/production',
    //         'post-deploy': 'npm install && pm2 reload ecosystem.config.js --env production'
    //     }
    // }
};