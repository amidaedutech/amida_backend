let createError = require('http-errors');
var log = require('../utils/winston');
module.exports = function(app) {
    // catch 404 and forward to error handler
    app.use(function(req, res, next) {
        next(createError(404));
    });

    // error handler
    app.use(function(err, req, res, next) {
        // set locals, only providing error in development
        res.locals.message = err.message;
        res.locals.error = req.app.get('env') === 'development' ? err : {};
        if (err) {
            log.error(`${err.status || 500} - ${err} - ${req.originalUrl} - ${req.method} - ${req.ip}`);

        }

        // render the error page
        res.status(err.status || 500);
        res.render('error');
    });

    function customError(err) {
        let errorObj = {

        }
        return errorObj[err.message];
    }
}