'use strict';
const express = require('express');
const router = express.Router();
const AssessmentCtrl = require('../controller/assessment');
var auth = require('../service/verify-token');
router.post('/answer', AssessmentCtrl.saveAnswer);
router.post('/result', AssessmentCtrl.result);
router.post('/getassessment', AssessmentCtrl.assessmentById);
router.post('/userid', AssessmentCtrl.assessmentByUserId);
router.get('/', AssessmentCtrl.fetchAll);
router.get('/:id', AssessmentCtrl.getAssessementById);
router.get('/get/questions/:id', AssessmentCtrl.getQuestionByAssessment);
router.get('/question/:id', AssessmentCtrl.getQuestionById);
router.get('/user/:id', AssessmentCtrl.getAssessementsByUser);
router.post('/save', auth, AssessmentCtrl.saveAssessment);
router.post('/save/questions', auth, AssessmentCtrl.saveQuestions);
router.delete('/:id', auth, AssessmentCtrl.removeAssessment);
router.delete('/question/:id', auth, AssessmentCtrl.removeQuestion);
router.post('/add/assessment/', auth, AssessmentCtrl.addAssessmemntToStudents);
router.post('/complete/assessment', AssessmentCtrl.completeAssessment);
router.post('/list', AssessmentCtrl.list)
router.post('/publish', AssessmentCtrl.publishAss)
router.post('/fetchList', AssessmentCtrl.fechList)
router.post('/underchecking', AssessmentCtrl.assUnderChecking)


module.exports = router;