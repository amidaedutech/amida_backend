'use strict';
const express = require('express');
const router = express.Router();
const ChatCtrl = require('../controller/chatbox');


/* GET users listing. */
router.post('/chatusers', ChatCtrl.getChatUsers);
router.post('/chatbyuser', ChatCtrl.getMsgsByChatId);
router.post('/chatcurruser', ChatCtrl.getCurrentUser);

module.exports = router;