'use strict';
const express = require('express');
const router = express.Router();
const FaqCtrl = require('../controller/faq');

/* GET users listing. */
router.get('/', FaqCtrl.fetch);
router.post('/', FaqCtrl.createFaq);
router.get('/:id', FaqCtrl.fetchById);
router.post('/one', FaqCtrl.fetchByOne);
module.exports = router;