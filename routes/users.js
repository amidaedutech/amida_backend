'use strict';
const express = require('express');
const router = express.Router();
const UsersCtrl = require('../controller/users');
var auth = require('../service/verify-token');

/* GET users listing. */
router.post('/', UsersCtrl.fetch);
router.get('/:id', UsersCtrl.fetchById);
router.delete('/:id', UsersCtrl.deleteById);
router.post('/register', UsersCtrl.register);
router.put('/:id', auth, UsersCtrl.update);
router.post('/login', UsersCtrl.login);
router.post('/save/details', UsersCtrl.save);
router.post('/social/login', UsersCtrl.socialLogin);
router.post('/password', auth, UsersCtrl.updatePassword);
router.post('/profile/image', UsersCtrl.uploadImage);
router.post('/logout', UsersCtrl.logout);
router.post('/forgetpwd', UsersCtrl.forgetPassword);
router.post('/resetpwd', UsersCtrl.resetPwd);
router.get('/me', UsersCtrl.verifyToken);
router.post('/refresh', UsersCtrl.refreshToken);
router.post('/contactus', UsersCtrl.contactus);
router.post('/finquiry', UsersCtrl.fetchInq);
router.post('/fidinquiry', UsersCtrl.fetchByIdInq);
router.post('/dinquiry', UsersCtrl.deleteByIdInq);
router.post('/uinquiry', auth, UsersCtrl.updateInq);
router.post('/isactive', auth, UsersCtrl.isActive);
router.post('/mynoti', UsersCtrl.notiByTo);
router.post('/mynotic', auth, UsersCtrl.notiByToCount);
router.post('/readnoti', UsersCtrl.readNoti);
router.post('/sendmailverify', UsersCtrl.sendMailForVerify);
router.post('/mailverify', UsersCtrl.mailVerify);
router.post('/findone', UsersCtrl.fetchByOne);
router.post('/uautocom', UsersCtrl.fetchForAutoCompl)


module.exports = router;