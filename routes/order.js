'use strict';
const express = require('express');
const router = express.Router();
const OrderCtrl = require('../controller/order');
var auth = require('../service/verify-token');
/* GET users listing. */
router.post('/', OrderCtrl.fetch);
router.get('/:id', OrderCtrl.fetchById);
router.get('/txnid/:id', OrderCtrl.fetchByTxnId);
router.post('/one', auth, OrderCtrl.fetchByOne);
router.post('/review', OrderCtrl.createReview);
router.get('/reviews/:id', OrderCtrl.getReviews);
router.delete('/reviews/:id', auth, OrderCtrl.deleteReview);
router.post('/reviews/all', OrderCtrl.fetchAllReviews);
router.post('/placed', auth, OrderCtrl.save);
router.post('/course', auth, OrderCtrl.getPurchasedCourse);
router.get('/relatedcourse/:id', OrderCtrl.getRelatedCourse);

module.exports = router;