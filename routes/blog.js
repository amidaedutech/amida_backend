'use strict';
const express = require('express');
const router = express.Router();
const BlogCtrl = require('../controller/blog');
const auth = require('../service/verify-token');

/* GET users listing. */
router.post('/', BlogCtrl.fetch);
router.get('/:id', BlogCtrl.fetchById);
router.post('/delete', auth, BlogCtrl.deleteById);
router.post('/addblog', auth, BlogCtrl.addBlog);
router.post('/ubimage', BlogCtrl.uploadBImage);
router.post('/review', BlogCtrl.createReview);
router.get('/reviews/:id', BlogCtrl.getReviews);
router.delete('/reviews/:id', auth, BlogCtrl.deleteReview);
router.post('/reviews/all', BlogCtrl.fetchAllReviews);
router.post('/blogcat/add', auth, BlogCtrl.saveCat);
router.post('/blogrootcat/add', auth, BlogCtrl.saveRooCat);
router.delete('/blogcat/delete/:id', auth, BlogCtrl.deleteCat);
router.delete('/blogrootcat/delete/:id', auth, BlogCtrl.deleteRooCat);
router.get('/blogcat/all', BlogCtrl.fetchCat);
router.get('/blogrootcat/all', BlogCtrl.fetchRootCat);
router.get('/blogcat/catroot', BlogCtrl.fetchCatRootCat);
module.exports = router;