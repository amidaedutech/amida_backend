'use strict';
const express = require('express');
const router = express.Router();
const StuReportCtrl = require('../controller/student-report');

/* GET users listing. */
router.post('/one', StuReportCtrl.fetchByOne);
//router.post('/register', UsersCtrl.register);
//router.put('/:id', UsersCtrl.update);
module.exports = router;