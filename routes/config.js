'use strict';
var indexRouter = require('./index');
var usersRouter = require('./users');
var rolesRouter = require('./roles');
var coursesRouter = require('./courses');
var contentRouter = require('./content');
var categoryRouter = require('./category');
var rootCategoryRouter = require('./root-category');
var subjectRouter = require('./subject');
var cartRouter = require('./cart');
var reportRouter = require('./report');
var orderRouter = require('./order');
var faqRouter = require('./faq');
var videoRouter = require('./video');
var paymentRouter = require('./payment');
var studentReportRouter = require('./stureport');
var assessmentRouter = require('./assessment');
var adminRouter = require('./admin');
var blogRouter = require('./blog');
var chatRouter = require('./chat');
var chatBotRouter = require('./chatbot');
var jobsRouter = require('./jobs');
module.exports = function(app) {
    const handlers = [
        { path: '/', router: indexRouter },
        { path: '/api/users', router: usersRouter },
        { path: '/api/roles', router: rolesRouter },
        { path: '/api/courses', router: coursesRouter },
        { path: '/api/content', router: contentRouter },
        { path: '/api/category', router: categoryRouter },
        { path: '/api/rootcategory', router: rootCategoryRouter },
        { path: '/api/subject', router: subjectRouter },
        { path: '/api/cart', router: cartRouter },
        { path: '/api/report', router: reportRouter },
        { path: '/api/order', router: orderRouter },
        { path: '/api/faq', router: faqRouter },
        { path: '/api/video', router: videoRouter },
        { path: '/api/payment', router: paymentRouter },
        { path: '/api/stdreport', router: studentReportRouter },
        { path: '/api/assessment', router: assessmentRouter },
        { path: '/api/admin', router: adminRouter },
        { path: '/api/blog', router: blogRouter },
        { path: '/api/chat', router: chatRouter },
        { path: '/api/chatbot', router: chatBotRouter },
        { path: '/api/jobs', router: jobsRouter }
    ];
    handlers.forEach(function(route, index) {
        app.use(route.path, route.router);
    });
};