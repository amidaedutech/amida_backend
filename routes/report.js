'use strict';
const express = require('express');
const router = express.Router();
const ReportCtrl = require('../controller/report');


/* GET users listing. */
router.get('/home', ReportCtrl.homePageReport);
router.get('/:id', ReportCtrl.fetchById);
router.post('/one', ReportCtrl.fetchByOne);
//router.post('/register', UsersCtrl.register);
//router.put('/:id', UsersCtrl.update);
module.exports = router;