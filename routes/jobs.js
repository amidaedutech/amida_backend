'use strict';
const express = require('express');
const router = express.Router();
const JobsCtrl = require('../controller/jobs');
var auth = require('../service/verify-token');
/* GET users listing. */
router.post('/', JobsCtrl.fetch);
router.post('/fetchapply', JobsCtrl.fetchApplyJobs);
router.get('/:id', JobsCtrl.fetchById);
router.post('/delete', auth, JobsCtrl.deleteById);
router.post('/addjobs', auth, JobsCtrl.addJobs);
router.post('/upresume', JobsCtrl.uploadResume);
router.post('/apply', JobsCtrl.applyJob);
router.post('/delapplyjob', auth, JobsCtrl.deleteApplyJob);
router.post('/jobconfig', auth, JobsCtrl.jobConfigAdd);
router.post('/jcondel', auth, JobsCtrl.jobConfigRemove);
router.post('/fechjobconf', JobsCtrl.jobFetchConfig);
router.post('/updateapplyjob', JobsCtrl.updateApplyJob);
module.exports = router;