'use strict';
const express = require('express');
const router = express.Router();
const rolesCtrl = require('../controller/roles');
/* GET users listing. */
router.get('/', rolesCtrl.fetch);
router.post('/', rolesCtrl.save);
router.put('/:id', rolesCtrl.update);
module.exports = router;