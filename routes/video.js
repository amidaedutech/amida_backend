'use strict';
const express = require('express');
const router = express.Router();
const VideoCtrl = require('../controller/video');

/* GET users listing. */
router.get('/', VideoCtrl.download);
router.post('/', VideoCtrl.upload);
module.exports = router;