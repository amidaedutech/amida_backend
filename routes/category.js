'use strict';
const express = require('express');
const router = express.Router();
const CategoryCtrl = require('../controller/category');
var auth = require('../service/verify-token');

/* GET users listing. */
router.get('/', CategoryCtrl.fetch);
router.get('/:id', CategoryCtrl.fetchById);
router.post('/one', CategoryCtrl.fetchByOne);
router.get('/rootcategory/:id', CategoryCtrl.fetchByRootCategory);
router.get('/routes/childcategories', CategoryCtrl.rootCategoriesWithCategories);
router.post('/save/details', auth, CategoryCtrl.SaveDetails);
router.post('/upload/image', CategoryCtrl.uploadCategoryImage);
router.post('/update', CategoryCtrl.updateById);
router.delete('/:id', auth, CategoryCtrl.deleteById);

module.exports = router;