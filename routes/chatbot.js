'use strict';
const express = require('express');
const router = express.Router();
const ChatBotCtrl = require('../controller/chatbot');


/* GET users listing. */
router.post('/runmainbot', ChatBotCtrl.runMainBot);
router.post('/runchapterbot', ChatBotCtrl.runChapterBot);
router.post('/getcourses', ChatBotCtrl.getCourses);
router.post('/getwikianswers', ChatBotCtrl.getWikiAnswers);
router.get('/getchats/:id', ChatBotCtrl.getChats);
router.post('/savemessages', ChatBotCtrl.saveMessages);

module.exports = router;