'use strict';
const express = require('express');
const router = express.Router();
const multiparty = require('connect-multiparty'),
    multipartyMiddleware = multiparty();
const ContentCtrl = require('../controller/content');
var auth = require('../service/verify-token');

/* GET users listing. */
router.get('/', ContentCtrl.fetch);
router.post('/', auth, ContentCtrl.save);
router.post('/upload/', ContentCtrl.upload);
router.get('/:id', ContentCtrl.fetchById);
router.get('/demourl/:id', ContentCtrl.getDemoUrl)
router.post('/vd', auth, ContentCtrl.deleteV);
router.post('/vdc', auth, ContentCtrl.deleteContent);
router.post('/savecont', ContentCtrl.saveContent);
//router.post('/delete', ContentCtrl.remove);
//router.delete('/deleteall/:id', ContentCtrl.deleteAll);
module.exports = router;