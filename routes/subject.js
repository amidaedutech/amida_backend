'use strict';
const express = require('express');
const router = express.Router();
const SubjectCtrl = require('../controller/subject');
var auth = require('../service/verify-token');

/* GET users listing. */
router.get('/', SubjectCtrl.fetch);
router.get('/:id', SubjectCtrl.fetchById);
router.post('/one', SubjectCtrl.fetchByOne);
router.get('/category/:id', SubjectCtrl.fetchByCategory);
router.delete('/:id', auth, SubjectCtrl.deleteById);
//router.post('/register', UsersCtrl.register);
//router.put('/:id', UsersCtrl.update);
module.exports = router;