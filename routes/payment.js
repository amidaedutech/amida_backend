'use strict';
const express = require('express');
const router = express.Router();
const paymentsCtrl = require('../controller/payments');
/* GET users listing. */
router.post('/initpay', paymentsCtrl.initPayment);
// router.post('/payu', paymentsCtrl.payment);
router.get('/getstatus/:txnid', paymentsCtrl.getPaymentStatus);
router.get('/gethash/', paymentsCtrl.getHash);
router.post('/callback', paymentsCtrl.paymentCallback);
router.post('/refundpay', paymentsCtrl.paymentRefund);
router.post('/ordermanually', paymentsCtrl.orderManually)
module.exports = router;