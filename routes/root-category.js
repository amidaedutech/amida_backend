'use strict';
const express = require('express');
const router = express.Router();
const RootCategoryCtrl = require('../controller/root-category');
var auth = require('../service/verify-token');

/* GET users listing. */
router.get('/', RootCategoryCtrl.fetch);
router.get('/:id', RootCategoryCtrl.fetchById);
router.delete('/:id', auth, RootCategoryCtrl.deleteById);
module.exports = router;