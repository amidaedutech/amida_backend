'use strict';
const express = require('express');
const router = express.Router();
const CartCtrl = require('../controller/cart');
var auth = require('../service/verify-token');

/* GET users listing. */
router.get('/', CartCtrl.fetch);
router.post('/', auth, CartCtrl.save);
router.get('/:id', CartCtrl.fetchById);
router.post('/one', auth, CartCtrl.fetchByOne);
router.post('/cartcount', CartCtrl.fetchCartCount);
router.post('/delete', auth, CartCtrl.remove);
router.delete('/deleteall/:id', auth, CartCtrl.deleteAll);
module.exports = router;