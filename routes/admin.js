'use strict';
const express = require('express');
const router = express.Router();
const AdminCtrl = require('../controller/admin');
var auth = require('../service/verify-token');
const DashboardCtrl = require('../controller/dashboard');
/* GET users listing. */
router.post('/homesliderdn', AdminCtrl.homeSlideDown);
router.post('/homesliderup', AdminCtrl.homeSlideUp);
router.post('/homesd', auth, AdminCtrl.deleteHSlider);

router.post('/studentd', DashboardCtrl.student);
router.post('/admind', DashboardCtrl.admin);
router.post('/countsd', DashboardCtrl.getCounts);
router.post('/imagebypath', AdminCtrl.imageByPath);
router.post('/uploadhtml', AdminCtrl.uploadHtmlContent);
router.post('/dfile', AdminCtrl.downlFile);
router.post('/deletefile', auth, AdminCtrl.deleteFile);

module.exports = router;