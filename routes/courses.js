'use strict';
const express = require('express');
const router = express.Router();
const CoursesCtrl = require('../controller/courses');
const FreeMatCtrl = require('../controller/freemat');
var auth = require('../service/verify-token');

/* GET users listing. */
router.post('/', CoursesCtrl.fetch);
router.get('/homelist', CoursesCtrl.getCourses);
router.post('/one', CoursesCtrl.fetchByOne);
router.get('/all', CoursesCtrl.getAll);
router.get('/:id', CoursesCtrl.fetchById);
router.get('/type/:type', CoursesCtrl.fetchByType);
router.post('/filter', CoursesCtrl.getFilter);
router.post('/upload/image', CoursesCtrl.uploadCourseImage);
router.post('/save/course', auth, CoursesCtrl.saveCourse);
router.post('/remove', auth, CoursesCtrl.deleteById);
router.post('/currcourse', CoursesCtrl.getLatestCourse);
router.post('/upload/docs', CoursesCtrl.docsUpload);
router.post('/freemat/fetch', FreeMatCtrl.fetch);
router.post('/freemat/byId', FreeMatCtrl.fetchById);
router.post('/freemat/save', auth, FreeMatCtrl.save);
router.post('/freemat/upimg', FreeMatCtrl.uploadBImage);
router.post('/freemat/delete', FreeMatCtrl.deleteById);

//router.post('/register', UsersCtrl.register);
//router.put('/:id', UsersCtrl.update);
module.exports = router;