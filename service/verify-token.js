var jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens
var config = require('../bin/server-config'); // get our config file
const Auth = require('../models/auth');

function verifyToken(req, res, next) {
    var token = req.headers.authorization;
    if (!token)
        return res.status(403).send('Your not authorized user, please logout and login again!!');
    jwt.verify(token, config.secret, function(err, decoded) {
        if (err) {
            return res.status(403).send('Your Access Token has been expired in the device, Please login again.');
        } else {
            Auth.findOne({ token: token }, function(err, authdoc) {
                if (authdoc) {
                    req.userId = decoded.id;
                    next();
                } else {
                    return res.status(403).send('Your not authorized user, please logout and login again!!');
                }
            });
        }
    });
}

module.exports = verifyToken;