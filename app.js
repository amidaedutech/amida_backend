let express = require('express');
let app = express();
global.__root = __dirname + '/';

app.get('/api', function(req, res) {
    res.status(200).send('API works.');
});
//application configuration...
require('./bin/config')(app);

//mongodb configuration...
require('./bin/db')();

//router configuration...
require('./routes/config')(app);

//server error configuration...
require('./error/server')(app);

module.exports = app;