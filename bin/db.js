let mongoose = require('mongoose');
let events = require('events');
let eventEmitter = new events.EventEmitter();
const User = require('../models/users');
const config = require('./server-config');
var bcrypt = require('bcryptjs');
module.exports = function() {
    var connect = function() {
        var options = {};
        mongoose.connect('mongodb+srv://dharmaveera:dharmaveera@cluster0.6uovg.mongodb.net/amida?retryWrites=true&w=majority', { useUnifiedTopology: true, useNewUrlParser: true });
    };
//mongodb+srv://dharmaveera:dharmaveera@cluster0.6uovg.mongodb.net/SkillAdda?retryWrites=true&w=majority
//mongodb+srv://Vd4vennam:Vd4vennam@cluster0.ulcey.mongodb.net/TradeTech?authSource=admin&replicaSet=Cluster0-shard-0&w=majority&readPreference=primary&appname=MongoDB%20Compass%20Community&retryWrites=true&ssl=true
    connect();
    mongoose.connection.on('disconnected', connect);
    mongoose.connection.on('error', console.error.bind(console, 'connection error:'));
    mongoose.set('useNewUrlParser', true);
    // mongoose.set('useFindAndModify', false);
    mongoose.set('useCreateIndex', true);
    mongoose.connection.once('open', function callback() {
        console.log('DB Connected');
        //eventEmitter.emit('db-connection-established');//cneck this code
        User.findOne({ email: config.superAdmin.email }, function(err, user) {
            if (err) console.log('Error on the server while setting default user.');
            if (!user) {
                let hashedPassword = bcrypt.hashSync(config.superAdmin.password, 8);
                User.create({
                    status: 'Active',
                    role: config.superAdmin.role,
                    username: config.superAdmin.username,
                    email: config.superAdmin.email,
                    hashedPassword: hashedPassword,
                    emailVerify: true,
                    phoneVerify: true
                }, function(err, user) {
                    if (err) console.log('Error on the server while setting default user / ' + err.toString());
                });
            }
        });
        User.findOne({ email: config.admin.email }, function(err, user) {
            if (err) console.log('Error on the server while setting default user.');
            if (!user) {
                let hashedPassword = bcrypt.hashSync(config.admin.password, 8);
                User.create({
                        status: 'Active',
                        role: config.admin.role,
                        username: config.admin.username,
                        email: config.admin.email,
                        hashedPassword: hashedPassword,
                        emailVerify: true,
                        phoneVerify: true
                    },
                    function(err, user) {
                        if (err) console.log('Error on the server while setting default user / ' + err.toString());
                    });
            }
        });
    });

    return mongoose;
};