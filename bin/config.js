var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var express = require('express');
var path = require('path');
var logger = require('morgan');
const cors = require('cors');
const busboy = require('connect-busboy');

module.exports = function(app) {


    app.use(cookieParser());
    app.use(methodOverride());
    app.use(bodyParser.json({ limit: '50mb' }));
    app.use(busboy({
        highWaterMark: 2 * 1024 * 1024, // Set 2MiB buffer
    }));
    app.use(bodyParser.urlencoded({ limit: '200mb', extended: false, parameterLimit: 1000000 }));
    //const whitelist = [config.frontendUrl];
    const corsOptions = {
    
      credentials: true,
        origin: function(origin, callback){
            callback(null, true)
        }
        // origin: 'https://SkillAddaedutech.com',
      //  optionsSuccessStatus: 200
    };
   
    app.use(cors(corsOptions)); //Enable CORS
    app.options('*', cors(corsOptions)); // Enable CORS Pre-Flight

    // view engine setup
    app.set('views', path.join(__dirname, '../views'));
    app.set('view engine', 'jade');
    // app.engine('html', engine.mustache);
    // app.set('view engine', 'html');
    app.use(logger('dev'));
    app.use(express.static(path.join(__dirname, 'public')));
    app.use(express.static(__dirname + '/public/images'));
};