#!/usr/bin/env node

/**
 * Module dependencies.
 */
var cluster = require('cluster');
var app = require('../app');
var debug = require('debug')('SkillAdda-server:server');
var http = require('http');
var https = require('https');
var fs = require('fs');
var workers = process.env.WORKERS || require('os').cpus().length;
/**
 * Get port from environment and store in Express.
 */
var privateKey = fs.readFileSync(__dirname + '/private.key', 'utf8');
var certificate = fs.readFileSync(__dirname + '/server.cert', 'utf8');
var ca = fs.readFileSync(__dirname + '/ca.crt', 'utf8');
var credentials = {
    key: privateKey,
    cert: certificate,
    ca: ca,
    requestCert: false,
    rejectUnauthorized: false
};
var port = normalizePort(process.env.PORT || '8888');
app.set('port', port);

/**
 * Create HTTP server.
 */
var server = {};

if (cluster.isMaster) {

    console.log('start cluster with %s workers', workers);
    require('../utils/scheduler');
    for (var i = 0; i < workers; ++i) {
        var worker = cluster.fork().process;
        console.log('worker %s started.', worker.pid);
    }

    cluster.on('exit', function(worker) {
        console.log('worker %s died. restart...', worker.process.pid);
        cluster.fork();
    });

} else {
    /**
     * Listen on provided port, on all network interfaces.
     */

    if (process.env.NODE_ENV === 'production') {
        var server = https.createServer(credentials, app);
        server.listen(port);
        require('../controller/socketconfig').serverSocket(server);
        server.on('error', onError);
        server.on('listening', onListening);
    } else {
        var server = http.createServer(app);
        server.listen(port);
        require('../controller/socketconfig').serverSocket(server);
        server.on('error', onError);
        server.on('listening', onListening);
    }
}

/**
 * Handle Uncaught Exceptions..
 */

process.on('uncaughtException', function(err) {
    console.error((new Date).toUTCString() + ' uncaughtException:', err.message);
    console.error(err.stack);
    process.exit(1);
})

/**
 * Normalize a port into a number, string, or false.
 */
function normalizePort(val) {
    var port = parseInt(val, 10);

    if (isNaN(port)) {
        // named pipe
        return val;
    }

    if (port >= 0) {
        // port number
        return port;
    }

    return false;
}

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
    if (error.syscall !== 'listen') {
        throw error;
    }

    var bind = typeof port === 'string' ?
        'Pipe ' + port :
        'Port ' + port;

    // handle specific listen errors with friendly messages
    switch (error.code) {
        case 'EACCES':
            console.error(bind + ' requires elevated privileges');
            process.exit(1);
            break;
        case 'EADDRINUSE':
            console.error(bind + ' is already in use');
            process.exit(1);
            break;
        default:
            throw error;
    }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
    var addr = server.address();
    var bind = typeof addr === 'string' ?
        'pipe ' + addr :
        'port ' + addr.port;
    debug('Listening on ' + bind);
}