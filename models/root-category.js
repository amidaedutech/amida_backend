/* global module */
'use strict';
var _ = require('lodash');
var mongoose = require('mongoose');
var baseSchema = require('./base-schema');
var Schema = mongoose.Schema;

var fields = {
    name: { type: String, unique: true },
    code: String,
    img: String
};


for (var key in baseSchema) {
    fields[key] = baseSchema[key];
}

module.exports = mongoose.model('root-categories', new Schema(fields, {
    toObject: { virtuals: true },
    toJSON: { virtuals: true }
}));