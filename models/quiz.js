/* global module */
'use strict';
var _ = require('lodash');
var mongoose = require('mongoose');
var baseSchema = require('./base-schema');
var Schema = mongoose.Schema;

var fields = {
    name: String,
    description: String,
    questions: [{
        question: String,
        options: [{
            key: String,
            option: String
        }],
        answer: String
    }],
    courseId: {
        type: Schema.ObjectId
    }
};


for (var key in baseSchema) {
    fields[key] = baseSchema[key];
}

module.exports = mongoose.model('quizes', new Schema(fields, {
    toObject: { virtuals: true },
    toJSON: { virtuals: true }
}));

