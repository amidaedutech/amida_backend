'use strict';
var _ = require('lodash');
var mongoose = require('mongoose');
var baseSchema = require('./base-schema');
var Schema = mongoose.Schema;
var fields = {
    courseId: Schema.ObjectId,
    title: String,
    seq: Number,
    videoUrl: [{
        title: String,
        path: String,
        type: { type: String },
        duration: Number,
        vseq: Number,
        vFlag: { type: Boolean, default: false },
    }],
    duration: String,
    content: String,
    docUrl: [{
        title: String,
        video: String,
        type: { type: String }
    }],
    image: [{ type: String }]
}

for (var key in baseSchema) {
    fields[key] = baseSchema[key];
}

module.exports = mongoose.model('contents', new Schema(fields, {
    toObject: { virtuals: true },
    toJSON: { virtuals: true }
}));