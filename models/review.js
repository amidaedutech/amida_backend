/* global module */
'use strict';
var _ = require('lodash');
var mongoose = require('mongoose');
var baseSchema = require('./base-schema');
var Schema = mongoose.Schema;

var fields = {
    review: String,
    rating: Number,
    reviwedBy: { //	Comments, typically from users.
        type: Schema.ObjectId,
        ref: require('./users')
    },
    course: {
        type: Schema.ObjectId,
        ref: require('./course'),
        index: true
    },
    location: {
        ip: String,
        place: String,
        country: String
    }
};


for (var key in baseSchema) {
    fields[key] = baseSchema[key];
}

module.exports = mongoose.model('reviews', new Schema(fields, {
    toObject: { virtuals: true },
    toJSON: { virtuals: true }
}));