/* global module */
'use strict';
var _ = require('lodash');
var mongoose = require('mongoose');
var baseSchema = require('./base-schema');
var Schema = mongoose.Schema;

var fields = {
    code: String,
    description: String,
    type: String, //percentage, amount
    value: String, 
    startDate: Date,
    endDate: Date
};


for (var key in baseSchema) {
    fields[key] = baseSchema[key];
}

module.exports = mongoose.model('promos', new Schema(fields, {
    toObject: { virtuals: true },
    toJSON: { virtuals: true }
}));