'use strict';
var _ = require('lodash');
var mongoose = require('mongoose');
var baseSchema = require('./base-schema');
var Schema = mongoose.Schema;

var fields = {
    txnid: String,
    roid: String, //razorpay order id
    mode: String,
    status: String, //created -authorized -captured -refunded -failed
    unmappedstatus: String,
    mihpayid: String,
    amount: Number,
    payuMoneyId: String,
    bank_ref_num: String,
    bankcode: String,
    name_on_card: String,
    cardnum: String, //'401200XXXXXX1112'
    cardnums: String,
    error: String, //'E000',
    error_Message: String, //'No Error',
    error_reason: String,
    error_source: String,
    error_step: String,
    encryptedPaymentId: String,
    signature: String,
    discount: String,
    net_amount_debit: String,
    amount_split: String,
    // posId: String,
    // invoiceNumber: String,
    comments: String,
    currency: String
};

for (var key in baseSchema) {
    fields[key] = baseSchema[key];
}

module.exports = mongoose.model('payments', new Schema(fields, {
    toObject: { virtuals: true },
    toJSON: { virtuals: true }
}));