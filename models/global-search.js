'use strict';
var mongoose = require('mongoose');
var baseSchema = require('./base-schema');
var Schema = mongoose.Schema;

var fields = {
    text: { type: String, unique: true },
};

for (var key in baseSchema) {
    fields[key] = baseSchema[key];
}

module.exports = mongoose.model('gsearchs', new Schema(fields, {
    toObject: { virtuals: true },
    toJSON: { virtuals: true }
}).index({ text: 1 }));