/* global module */
'use strict';
var mongoose = require('mongoose');
var baseSchema = require('./base-schema');
var Schema = mongoose.Schema;

var fields = {
    type: { type: String, enum: ['Inquiry', 'Purchase', 'Assessment', 'Machine'] },
    msg: String,
    to: {
        type: Schema.ObjectId,
        ref: require('./users')
    },
    status: {
        type: String,
        default: 'Unread',
        enum: ['Deleted', 'Unread', 'Read'],
        required: true
    }
};

for (var key in baseSchema) {
    fields[key] = baseSchema[key];
}

module.exports = mongoose.model('notifications', new Schema(fields, {
    toObject: { virtuals: true },
    toJSON: { virtuals: true }
}));