/* global module */
'use strict';
var mongoose = require('mongoose');
var baseSchema = require('./base-schema');
var Schema = mongoose.Schema;
var mp = require('mongoose-paginate');
var fields = {
    status: { type: String, default: 'Active', enum: ['Active', 'Inactive'] },
    title: { type: String, unique: true },
    category: String, // HR, IT, Hardware
    location: String,
    summary: String,
    type: { type: String, default: 'Full Time', enum: ['Contract', 'Full Time', 'Part Time'] },
    jd: String
};

for (var key in baseSchema) {
    fields[key] = baseSchema[key];
}

module.exports = mongoose.model('jobs', new Schema(fields, {
    toObject: { virtuals: true },
    toJSON: { virtuals: true }
}).plugin(mp));