'use strict';
var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');
var baseSchema = require('./base-schema');
var Schema = mongoose.Schema;
var fields = {
    courseType: String,
    rootCategory: {
        type: Schema.ObjectId,
        ref: require('./root-category')
    },
    rootCategoryName: String,
    category: {
        type: Schema.ObjectId,
        ref: require('./category')
    },
    categoryName: String,
    subject: {
        type: Schema.ObjectId,
        ref: require('./subject')
    },
    subjectName: String,
    subjectCode: String,
    price: Number, //{actualPrice: 123,price}
    coursePrerequisites: String,
    about: String, //	The subject matter of the content.
    summary: String,
    aggregateRating: Number,
    totalRating: Number,
    headline: String, //title of the course
    name: { type: String, unique: true },
    tutor: { //	Comments, typically from users.
        type: Schema.ObjectId,
        autopopulate: true
    },
    htmlcontent: String,
    htmlFlag: { type: Boolean, default: false },
    htmlpath: String,
    discussionUrl: String,
    keywords: String, //Keywords or tags used to describe this content.
    version: String,
    description: String,
    image: String,
    demoUrl: { type: String },
    totalDuration: String,
    enrollments: Number,
    benefits: String,
    base64String: String,
    level: String, //beginner, intermediate, advanced
    status: String,
    docs: [String] //Active, Inactive
};

for (var key in baseSchema) {
    fields[key] = baseSchema[key];
}

module.exports = mongoose.model('courses', new Schema(fields, {
    toObject: { virtuals: true },
    toJSON: { virtuals: true }
}).plugin(mongoosePaginate));