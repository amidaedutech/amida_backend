'use strict';
var _ = require('lodash');
var mongoose = require('mongoose');
var baseSchema = require('./base-schema');
var Schema = mongoose.Schema;

var fields = {
    text: String,
    category: String,
    chatId: String, 
    status: { type: String, default: 'unread' } //  enum: ['read', 'unread']
};

for (var key in baseSchema) {
    fields[key] = baseSchema[key];
}

module.exports = mongoose.model('chatmsgs', new Schema(fields, {
    toObject: { virtuals: true },
    toJSON: { virtuals: true }
}));