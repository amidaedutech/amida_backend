/* global module */
'use strict';
var mongoose = require('mongoose');
var baseSchema = require('./base-schema');
var Schema = mongoose.Schema;

var fields = {
    title: { type: String, required: true, trim: true },
    content: { type: String, required: true, trim: true },
    category: String,
    rootCategory: String,
    image: String,
    summary: String,
    videoLinks: [String],
    created: { type: Date, default: Date.now },
    createdBy: { type: Schema.Types.ObjectId },
    modified: { type: Date },
    modifiedBy: { type: Schema.Types.ObjectId },
    aggregateRating: Number,
    totalRating: Number,
};


for (var key in baseSchema) {
    fields[key] = baseSchema[key];
}

module.exports = mongoose.model('blogs', new Schema(fields, {
    toObject: { virtuals: true },
    toJSON: { virtuals: true }
}));