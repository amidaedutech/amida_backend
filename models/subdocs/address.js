/*
 * Address
 */
var AddressSchema = {
    line1: {type: String},
    line2: String,
    locality: String,
    landmark: String,
    city: {type: String},
    state: {type: String},
    pincode: {type: Number}
};

/* global module */
module.exports = AddressSchema;
