/*
 * Image
 */
var ImageSchema = {
    type: {
        type: String,
        enum: ['thumbnail', 'detail'],
        default: 'detail'
    },
    url: {type: String}
};

/* global module */
module.exports = ImageSchema;
