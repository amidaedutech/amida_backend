/*
 * Phone
 */
var Phone = {
  countryCode: {type: String, default: '+91'},
  number: {type: String}
};

/* global module */
module.exports = Phone;
