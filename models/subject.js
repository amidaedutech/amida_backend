/* global module */
'use strict';
var _ = require('lodash');
var mongoose = require('mongoose');
var baseSchema = require('./base-schema');
var Schema = mongoose.Schema;

var fields = {
    rootCategoryId: {
        type: Schema.ObjectId
    },
    categoryId: {
        type: Schema.ObjectId
    },
    name: { type: String, unique: true },
    code: String,
    chapters: [{
        name: String,
        video: [{ type: String }]
    }]
};

for (var key in baseSchema) {
    fields[key] = baseSchema[key];
}

module.exports = mongoose.model('subjects', new Schema(fields, {
    toObject: { virtuals: true },
    toJSON: { virtuals: true }
}));