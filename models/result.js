/* global module */
'use strict';
var _ = require('lodash');
var mongoose = require('mongoose');
var baseSchema = require('./base-schema');
var Schema = mongoose.Schema;

var Question = require('../models/questions');

var fields = {
    name: String,
    description: String, //If anything want to show to user can add this
    score: String, // how much percentage got
    grade: String, //A,B,C,D
    status: { type: String, default: 'Incomplete' }, // quiz is Completed or Incomplete or Inprogress, UnderChecking ..
    answers: [{
        question: {
            type: Schema.Types.ObjectId,
            ref: require('./questions')
        },
        userAns: String, // option A, B, C, D
        comments: String,
        ans: { type: Boolean, default: false } // if answer is correct: true, if not correct: false;
    }],
    assessmentId: {
        type: Schema.ObjectId
    },
    duration: Number,
    category: String,
    subject: String,
    courseId: {
        type: Schema.ObjectId
    },
    userId: {
        type: Schema.ObjectId
    }
};


for (var key in baseSchema) {
    fields[key] = baseSchema[key];
}

module.exports = mongoose.model('results', new Schema(fields, {
    toObject: { virtuals: true },
    toJSON: { virtuals: true }
}));