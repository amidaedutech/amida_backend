/* global module */
'use strict';
var _ = require('lodash');
var mongoose = require('mongoose');
var baseSchema = require('./base-schema');
var Schema = mongoose.Schema;
var mp = require('mongoose-paginate');
var fields = {
    name: String,
    description: String,
    rootCategory: String,
    category: String,
    subject: String,
    type: {
        type: String // course or test/quiz/essay
    },
    courseId: {
        type: Schema.ObjectId
    },
    duration: Number,
    instructions: String,
    startDate: Date,
    endDate: Date,
    status: String // open, published
};


for (var key in baseSchema) {
    fields[key] = baseSchema[key];
}

module.exports = mongoose.model('assessments', new Schema(fields, {
    toObject: { virtuals: true },
    toJSON: { virtuals: true }
}).plugin(mp));