'use strict';
var _ = require('lodash');
var mongoose = require('mongoose');
var baseSchema = require('./base-schema');
var Schema = mongoose.Schema;

var fields = {
    rootCategoryId: {
        type: Schema.ObjectId,
        ref: require('./root-category')
    },
    name: { type: String, unique: true },
    code: String,
    image: String,
    base64String: String
};

for (var key in baseSchema) {
    fields[key] = baseSchema[key];
}

module.exports = mongoose.model('categories', new Schema(fields, {
    toObject: { virtuals: true },
    toJSON: { virtuals: true }
}));