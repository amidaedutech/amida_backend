'use strict';
var _ = require('lodash');
var mongoose = require('mongoose');
var baseSchema = require('./base-schema');
var Schema = mongoose.Schema;

var fields = {
    userId: { type: Schema.ObjectId },
    courses: [{
        type: Schema.ObjectId,
        ref: require('./course')
    }]
};

for (var key in baseSchema) {
    fields[key] = baseSchema[key];
}

module.exports = mongoose.model('carts', new Schema(fields, {
    toObject: { virtuals: true },
    toJSON: { virtuals: true }
}));