'use strict';
var mongoose = require('mongoose');
var baseSchema = require('./base-schema');
var Schema = mongoose.Schema;
var fields = {
    locations: [String],
    types: [String],
    categories: [String]
};

for (var key in baseSchema) {
    fields[key] = baseSchema[key];
}

module.exports = mongoose.model('jobconfigs', new Schema(fields, {
    toObject: { virtuals: true },
    toJSON: { virtuals: true }
}));