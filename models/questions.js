/* global module */
'use strict';
var _ = require('lodash');
var mongoose = require('mongoose');
var baseSchema = require('./base-schema');
var Schema = mongoose.Schema;

var fields = {
    qType: { type: Boolean, default: true }, // true means optional type, else que and answer.
    question: String,
    description: String,
    options: [{
        key: String,
        option: String
    }],
    userAnswer: String,
    answer: String,
    assessmentId: {
        type: Schema.ObjectId,
        index: true
    },
    number: {
        type: Number
    },
    comments: String
};

for (var key in baseSchema) {
    fields[key] = baseSchema[key];
}

module.exports = mongoose.model('questions', new Schema(fields, {
    toObject: { virtuals: true },
    toJSON: { virtuals: true }
}));