/* global module */
'use strict';

var mongoose = require('mongoose');
var moment = require('moment');
var Schema = mongoose.Schema;

module.exports = {
    created: {
        type: Date,
        default: Date.now
    },
    searchCreated: {
        type: String,
        default: formatDate()
    },
    createdBy: {
        type: Schema.ObjectId,
        ref: 'user',
        required: false
    },
    modified: {
        type: Date
    },
    searchModified: {
        type: String
    },
    modifiedBy: {
        type: Schema.ObjectId,
        ref: 'user',
        required: false
    }
};

function formatDate() {
    var dateVal = new Date();
    var offSet = ((-1 * dateVal.getTimezoneOffset()) / 60) + 4;
    var newDate = moment(dateVal).subtract(offSet, 'hours').format('YYYY-MM-DD h:mm:ss A');
    newDate = newDate.split(' ');
    var date = newDate[0].split('-');
    var time = newDate[1].split(':');
    var sMonth = date[1];
    var sDay = date[2];
    var sYear = date[0];
    var sHour = time[0];
    var sMinute = time[1];
    var sAMPM = newDate[2];
    return sMonth + "-" + sDay + "-" + sYear + " at " + sHour + ":" + sMinute + sAMPM;
}