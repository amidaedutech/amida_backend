/* global module */
'use strict';
var mongoose = require('mongoose');
var baseSchema = require('./base-schema');
var Schema = mongoose.Schema;
var mp = require('mongoose-paginate');
var fields = {
    status: { type: String, default: 'New', enum: ['New', 'Accepted', 'Rejected'] },
    firstName: String,
    lastName: String,
    email: String,
    phone: String,
    resume: String,
    jobfor: {
        type: Schema.ObjectId,
        ref: require('./jobs')
    },
};

for (var key in baseSchema) {
    fields[key] = baseSchema[key];
}

module.exports = mongoose.model('jobapplys', new Schema(fields, {
    toObject: { virtuals: true },
    toJSON: { virtuals: true }
}).plugin(mp));