/* global module */
'use strict';
var mongoose = require('mongoose');
var baseSchema = require('./base-schema');
var mp = require('mongoose-paginate');
var Schema = mongoose.Schema;
var fields = {
    orderId: {
        type: String
    },
    rid: { // razorpay_id
        type: String
    },
    entity: {
        type: String
    },
    quantity: {
        type: Number
    },
    searchQuantity: String,
    amount: Number,
    gst: Number,
    searchAmount: String,
    discount: Number,
    course: [{
        type: Schema.ObjectId,
        ref: require('./course')
    }],
    purchasedBy: {
        type: Schema.ObjectId,
        ref: require('./users')
    },
    payment: {
        type: Schema.ObjectId,
        ref: require('./payment')
    },
    promo: {
        type: Schema.ObjectId,
        ref: require('./promo')
    }
};

for (var key in baseSchema) {
    fields[key] = baseSchema[key];
}
module.exports = mongoose.model('orders', new Schema(fields, {
    toObject: { virtuals: true },
    toJSON: { virtuals: true }
}).index({ orderId: -1 }).plugin(mp));