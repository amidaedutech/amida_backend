'use strict';
var mongoose = require('mongoose');
var baseSchema = require('./base-schema');
var Schema = mongoose.Schema;
var mongoosePaginate = require('mongoose-paginate');
var fields = {
    content: String,
    title: String,
    summary: String,
    image: String,
    rootCategoryName: String,
    categoryName: String,
    subjectName: String,
};

for (var key in baseSchema) {
    fields[key] = baseSchema[key];
}

module.exports = mongoose.model('freemats', new Schema(fields, {
    toObject: { virtuals: true },
    toJSON: { virtuals: true }
}).index({ text: 1 }).plugin(mongoosePaginate));