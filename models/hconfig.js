/* global module */
'use strict';
var mongoose = require('mongoose');
var baseSchema = require('./base-schema');
var Schema = mongoose.Schema;

var fields = {
    title: String,
    type: { type: String, enum: ['homeslider', 'blog'] },
    content: String,
    image: String,
    summary: String,
    description: String
};

for (var key in baseSchema) {
    fields[key] = baseSchema[key];
}

module.exports = mongoose.model('hconfigs', new Schema(fields, {
    toObject: { virtuals: true },
    toJSON: { virtuals: true }
}));