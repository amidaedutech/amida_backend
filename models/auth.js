'use strict';
var _ = require('lodash');
var mongoose = require('mongoose');
var baseSchema = require('./base-schema');
var Schema = mongoose.Schema;

var fields = {
    email: {
        type: String
    },
    token: String,
    linkedInId: String,
    skypeId: String,
    googleId: String,
    fbId: String,
    role: String,
    loginStatus: Boolean
};

for (var key in baseSchema) {
    fields[key] = baseSchema[key];
}

module.exports = mongoose.model('auth', new Schema(fields, {
    toObject: { virtuals: true },
    toJSON: { virtuals: true }
}));