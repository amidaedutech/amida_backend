/* global module */
'use strict';
var mongoose = require('mongoose');
var baseSchema = require('./base-schema');
var Schema = mongoose.Schema;

var fields = {
    heading: String,
    content: String,
    faqBy: { //	Comments, typically from users.
        type: Schema.ObjectId,
        ref: require('./users')
    },
    course: {
        type: Schema.ObjectId,
        ref: require('./course')
    }
};

for (var key in baseSchema) {
    fields[key] = baseSchema[key];
}

module.exports = mongoose.model('faqs', new Schema(fields, {
    toObject: { virtuals: true },
    toJSON: { virtuals: true }
}));