'use strict';
var mongoose = require('mongoose');
var baseSchema = require('./base-schema');
var Schema = mongoose.Schema;
var fields = {
    blogrootcat: {
        type: Schema.ObjectId,
        ref: require('./blog-root-cat')
    },
    name: { type: String, unique: true }
};

for (var key in baseSchema) {
    fields[key] = baseSchema[key];
}

module.exports = mongoose.model('blogcats', new Schema(fields, {
    toObject: { virtuals: true },
    toJSON: { virtuals: true }
}));