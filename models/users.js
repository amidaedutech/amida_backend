'use strict';
var _ = require('lodash');
var mongoose = require('mongoose');
var Address = require('./subdocs/address');
var Phone = require('./subdocs/phone');
var Image = require('./subdocs/image');
var baseSchema = require('./base-schema');
var mongoosePaginate = require('mongoose-paginate');
var Schema = mongoose.Schema;

var fields = {
    status: {
        type: String
            //default: 'Active'
    },
    nameTitle: String, //Mr, Mrs
    username: { //logged-in username
        type: String,
        required: true
    },
    firstName: {
        type: String,
        //  required: true
    },
    middleName: String,
    lastName: String,
    fullName: String,
    dateOfBirth: Date,
    gender: String,
    maritalStatus: String,
    bloodGroup: String,
    language: String,
    description: String, // any description
    email: {
        type: String,
        unique: true
    },
    emailVerify: { type: Boolean, default: false },
    phoneVerify: { type: Boolean, default: false },
    panNumber: {
        type: String,
        uppercase: true,
        required: false,
        //validate: validations.panNumber
    },
    designation: String,
    phone: String,
    alternatePhone: String,
    address: {
        line1: String,
        line2: String,
        locality: String,
        landmark: String,
        city: String,
        state: String,
        pincode: Number,
        country: String
    },
    hashedPassword: {
        type: String,
        required: true
    },
    linkedInId: String,
    skypeId: String,
    googleId: String,
    fbId: String,
    cartId: {
        type: Schema.ObjectId
    },
    role: {
        type: String,
        required: true
    },
    images: [Image],
    image: String,
    base64String: String,
    notifications: Boolean,
    promotions: Boolean
};

for (var key in baseSchema) {
    fields[key] = baseSchema[key];
}

module.exports = mongoose.model('user', new Schema(fields, {
    toObject: { virtuals: true },
    toJSON: { virtuals: true }
}).plugin(mongoosePaginate));