'use strict';
var _ = require('lodash');
var mongoose = require('mongoose');
var baseSchema = require('./base-schema');
var Schema = mongoose.Schema;
var mp = require('mongoose-paginate');
var fields = {
    message: String,
    type: { type: String, default: 'Inquiry', enum: ['Inquiry', 'Contact Us'] }, // A=for particular product, B= non product
    status: { type: String, default: 'Open', enum: ['Open', 'Inprocess', 'Closed'] },
    name: { type: String },
    email: { type: String },
    phone: { type: String, required: true, trim: true },
    location: String,
    rootCategory: String,
    category: String,
    course: String
};

for (var key in baseSchema) {
    fields[key] = baseSchema[key];
}

module.exports = mongoose.model('contactus', new Schema(fields, {
    toObject: { virtuals: true },
    toJSON: { virtuals: true }
}).plugin(mp));