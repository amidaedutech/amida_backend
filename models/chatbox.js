'use strict';
var _ = require('lodash');
var mongoose = require('mongoose');
var baseSchema = require('./base-schema');
var Schema = mongoose.Schema;

var fields = {
    username: String,
    email: String,
    phone: String,
    aread: { type: Boolean, default: true },
    sread: { type: Boolean, default: true },
    chatId: { type: String}, // everytime creat chat id for one session
    status: { type: Boolean, default: false }, // online/offline
    created: Date,
    botname: String,
    text: String,
    createdBy: String
};

for (var key in baseSchema) {
    if (key !== 'createdBy')
        fields[key] = baseSchema[key];
}

module.exports = mongoose.model('chats', new Schema(fields, {
    toObject: { virtuals: true },
    toJSON: { virtuals: true }
}));