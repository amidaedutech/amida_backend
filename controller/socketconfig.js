const mongoose = require('mongoose');
const ObjectId = mongoose.Types.ObjectId;
const _ = require('lodash');
const fs = require('fs');
const uniqid = require('uniqid');
const path = require('path');
const dialogflow = require('dialogflow');

const Content = require('../models/content');
const ChatModel = require('../models/chatbox');
const ChatMsgModel = require('../models/chatmsgs');
const log = require('../utils/winston');
module.exports = {
    serverSocket: serverSocket
}

function serverSocket(server) {
    const io = require('socket.io').listen(server);
    var files = { downloaded: 0 };
    io.sockets.on('connection', function(socket) {
        console.log("socket connected");
        socketObj = socket;
        // broadcast a user's message to other users
        var uniqueName = '';
        var videoPath = path.join('../' + '/SkillAdda_videos/');
        var tempPath = path.join('../' + '/temp_videos/');
        var formData = {};
        socket.on('start:upload:video', function(data) { //data contains the variables that we passed through in the html file

            formData = data.formData;
            var fileName = data['fileName'];
            var ext = fileName.substring(fileName.lastIndexOf('.') + 1);
            uniqueName = uniqid() + '.' + ext;
            var name = data['fileName'];
            files[name] = { //Create a new Entry in The Files Variable
                fileSize: data['fileSize'],
                data: "",
                downloaded: 0
            }
            var place = 0;

            try {
                if (!fs.existsSync(videoPath)) {
                    fs.mkdirSync(videoPath);
                }
                if (data.coursePath) {
                    videoPath = path.join('../SkillAdda_videos/' + data.coursePath + '/');
                    if (!fs.existsSync(videoPath)) {
                        fs.mkdirSync(videoPath);
                    }
                }
                if (!fs.existsSync(tempPath)) {
                    fs.mkdirSync(tempPath);
                }
                var stats = fs.statSync(tempPath + uniqueName);
                if (stats.isFile()) {
                    files[name]['downloaded'] = stats.size;
                    place = stats.size / 2097152;
                }
            } catch (er) {
                //console.log('err stat', er);
            } //It's a New File
            fs.open(tempPath + uniqueName, "a", 0755, function(err, fd) {
                if (err) {
                    console.log(err);
                } else {
                    files[name]['handler'] = fd; //We store the file handler so we can write to it later
                    socket.emit('upload:moredata', { 'place': place, percent: 0 });
                }
            });
        });
        socket.on('upload:video', function(data) {
            // console.log('data v', data);
            var name = data['fileName'];
            files[name]['downloaded'] += data['data'].length;
            files[name]['data'] += data['data'];
            if (files[name]['downloaded'] == files[name]['fileSize']) //If File is Fully Uploaded
            {
                fs.write(files[name]['handler'], files[name]['data'], null, 'Binary', function(err, Writen) {
                    var filePath = videoPath + uniqueName;
                    var tempStrem = fs.createReadStream(tempPath + uniqueName);
                    var outStrem = fs.createWriteStream(filePath);
                    tempStrem.pipe(outStrem);
                    tempStrem.on("end", function() {
                        fs.unlinkSync(tempPath + uniqueName);
                        if (((data || {}).formData || {}).existPath) {
                            if (fs.existsSync(data.formData.existPath)) {
                                fs.unlinkSync(data.formData.existPath);
                            }
                        }
                        saveContent(formData, filePath).then(function(contentDoc) {
                            socket.emit('upload:done', contentDoc);
                        }, function(err) {
                            socket.emit('upload:done', { error: err });
                        });
                    });
                });
            } else if (files[name]['data'].length > 10485760) { //If the Data Buffer reaches 10MB
                fs.write(files[name]['handler'], files[name]['data'], null, 'Binary', function(err, Writen) {
                    files[name]['data'] = ""; //Reset The Buffer
                    var place = files[name]['downloaded'] / 2097152;
                    var percent = (files[name]['downloaded'] / files[name]['fileSize']) * 100;
                    socket.emit('upload:moredata', { 'place': place, 'percent': percent });
                });
            } else {
                var place = files[name]['downloaded'] / 2097152;
                var percent = (files[name]['downloaded'] / files[name]['fileSize']) * 100;
                socket.emit('upload:moredata', { 'place': place, 'percent': percent });
            }
        });

        socket.on('update:content', function(data) {
            saveContent(data.formData, '').then(function(contentDoc) {
                socket.emit('upload:done', contentDoc);
            }, function(err) {
                socket.emit('upload:done', { error: err });
            });
        });
        socket.on('user:msg:req', async(data) => {
            try {
                let chatDoc = await ChatModel.findOne({ chatId: data.chatId });
                if (!(chatDoc || {}).chatId) {
                    data.status = true;
                    data.modified = new Date();
                    const chatModel = new ChatModel(data);
                    await chatModel.save();
                    data.status = 'unread';
                    let chatMsgModel = new ChatMsgModel(data);
                    let chtMdoc = await chatMsgModel.save();
                    let findChat = await ChatMsgModel.findById(chtMdoc._id);
                    if ((data || {}).role === 'admin') {
                        // socket.emit('admin:msg:res', findChat);
                        socket.broadcast.emit('user:msg:res:' + data.chatId, findChat);
                    } else {
                        // socket.emit('user:msg:res:' + data.chatId, findChat);
                        socket.broadcast.emit('admin:msg:res', findChat);
                    }
                } else {
                    chatDoc.status = true;
                    chatDoc.modified = new Date();
                    await chatDoc.save();
                    data.chatId = chatDoc.chatId;
                    data.status = 'unread';
                    let chatMsgModel = new ChatMsgModel(data);
                    let chtMdoc = await chatMsgModel.save();
                    let findChat = await ChatMsgModel.findById(chtMdoc._id);
                    if ((data || {}).role === 'admin') {
                        socket.broadcast.emit('user:msg:res:' + chatDoc.chatId, findChat);
                    } else {
                        socket.broadcast.emit('admin:msg:res', findChat);
                    }
                }
            } catch (err) {
                log.info({ message: 'Socket1 Error', label: 'ERROR', msg: err.stack });
                socket.emit('user:msg:res', { error: 'Unable to find user/' + err.toString() });
            }
        });

        socket.on('user:msg:offline', async(data) => {
            try {
                let chatDoc = await ChatModel.findOne({ chatId: data.chatId });
                chatDoc.status = false;
                let saveDoc = await chatDoc.save();
                socket.emit('user:msg:offlinelist', { chatId: data.chatId, status: false });
                socket.broadcast.emit('admin:msg:ucontact', {}); // after logout refresh contacts
            } catch (err) {
                //socket.emit('user:msg:res', { error: 'Unable to find user/' + err.toString() });
            }
        });
        socket.on('user:msg:loaduser', async(data) => {
            try {
                let chatDoc = await ChatModel.findOne({ chatId: data.chatId });
                chatDoc.status = true;
                await chatDoc.save();
                socket.broadcast.emit('admin:msg:ucontact', {}); // after logout refresh contacts
            } catch (err) {
                log.info({ message: 'Socket 2 Error', label: 'ERROR', msg: err.stack });
            }
        });
        socket.on('SkillAdda:logout', async(data) => {
            try {
                socket.broadcast.emit('SkillAdda:client:logout', data); // after logout refresh contacts
            } catch (err) {
                log.info({ message: 'Socket 3 Error', label: 'ERROR', msg: err.stack });
            }
        })
        socket.on('disconnect', function(data) {
            console.log('client disconnected');
        });
    });
};


function saveContent(reqBody, uniqueName) {
    var filePathIs = uniqueName;
    return new Promise(function(resolve, reject) {
        Content.find({ courseId: ObjectId(reqBody.courseId) }, function(err, contents) {
            //contentId is main content id
            //_id is part id
            var contentData = {};
            var contentArray = _.filter(contents, function(o) { return o._id.toString() === reqBody.contentId.toString() });
            if (contentArray.length > 0) {
                contentData = contentArray[0];
            }
            if (err) {
                reject("There was a problem while save to Content.");
            } else if (!contentData._id) {
                //first content and first video
                //new video also
                var cont = new Content();
                cont.title = reqBody.title;
                cont.seq = reqBody.seq;
                cont.videoUrl = [];
                if (filePathIs) {
                    cont.videoUrl.push({
                        title: reqBody.contentTitle,
                        vseq: reqBody.vseq,
                        path: filePathIs,
                        vFlag: reqBody.vFlag
                            // duration: reqBody.duration
                    });
                }
                cont.courseId = ObjectId(reqBody.courseId)
                cont.save(function(err, doc) {
                    if (err) reject("There was a problem while save to Content.");
                    resolve(doc);
                });
            } else {
                //from second video
                if (!reqBody._id) {
                    //new chapter for existing content 
                    contentData.title = reqBody.title;
                    if (reqBody.seq) {
                        contentData.seq = reqBody.seq;
                    }
                    contentData.videoUrl.push({
                        title: reqBody.contentTitle,
                        vseq: reqBody.vseq,
                        vFlag: reqBody.vFlag,
                        path: filePathIs
                    });
                } else if (reqBody._id) {
                    contentData.title = reqBody.title;
                    if (reqBody.seq) {
                        contentData.seq = reqBody.seq;
                    }
                    //existing content chapter edit
                    var videoData = contentData.videoUrl;
                    var index = _.findIndex(contentData.videoUrl, function(o) { return o._id.toString() === reqBody._id.toString() });
                    // let duration = videoData[index].duration;
                    videoData[index].title = reqBody.contentTitle;
                    videoData[index].vseq = reqBody.vseq;
                    videoData[index].vFlag = reqBody.vFlag
                        // videoData[index].duration = reqBody.duration;
                    if (filePathIs) {
                        videoData[index].path = filePathIs;
                    }
                    contentData.videoUrl = videoData;
                    // contentData.duration = Number(contentData.duration) + Number(reqBody.duration) - duration;
                }
                contentData.save(function(err, doc) {
                    if (err) {
                        reject("There was a problem while save to Content.");
                    }
                    resolve(doc);
                });
            }
        });
    });
}