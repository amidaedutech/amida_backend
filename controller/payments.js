var payumoney = require('payumoney-node');
const Cart = require('../models/cart');
const Order = require('../models/order');
const PAYMENT = require('../bin/server-config').PAYMENT;
const Payment = require('../models/payment');
const NotiModel = require('../models/notification');
const UserModel = require('../models/users');
const CourseModel = require('../models/course');
var log = require('../utils/winston');
const crypto = require('crypto');
const sha512 = require('js-sha512');
const Razorpay = require('razorpay');
module.exports = {
    initPayment: initPayment,
    // payment: rpayment,
    paymentCallback: paymentCallback,
    paymentRefundStatus: paymentRefundStatus,
    paymentRefund: paymentRefund,
    getPaymentStatus: getPaymentStatus,
    orderManually: orderManually,
    getHash: getHash
};

var razorPayInstance = new Razorpay({
    key_id: PAYMENT.KEY_ID,
    key_secret: PAYMENT.KEY_SECRET,
  });
  function rpayment(req, res, next){

  }
  async function initPayment(req, res, next){
  try {
        let carts = await Cart.findOne({ userId: req.body.userId }).populate({ path: 'courses', model: 'courses' }).exec();
        console.log('carts', carts);
        if(!carts) return res.status(404).send("You cart is empty, please add some product and try.");
        let price = 0;
        let priceWithGst = 0;
        let courseList = [];
        if ((carts || {}).courses && carts.courses.length > 0) {
            carts.courses.forEach(element => {
                price += element.price;
                courseList.push(element._id);
            });
            priceWithGst = price * 0.18;
            let priceAfterGst = priceWithGst + price;
            let receiptId = getOrderId();
            let orderDetail = await generateOrderId({amount: parseInt(priceAfterGst), receiptId: receiptId});
            orderDetail.r_key_id = PAYMENT.KEY_ID;
            orderDetail.courseList = courseList;
            orderDetail.quantity = carts.courses.length;
            orderDetail.searchQuantity = carts.courses.length + '';
            return res.status(200).send(orderDetail);
        }
      } catch (err) {
        log.info({ message: 'Init Payment payment Error', label: 'ERROR', msg: err.stack || '' });
          if(err.statusCode) {
            return res.status(err.statusCode).send('Payment Gateway Error : ' + err.error.description);
          } else {
            return res.status(500).send('Internal Server Error : ' + err.toString());
          }
      }
  }

  async function paymentCallback(req, res) {
    try {
        let orderDetail = req.body;
        log.info({ message: 'Payment paymentCallback Response', label: 'ERROR', msg: JSON.stringify(req.body) });
        let validatePayment = checkHash(req);
        console.log('validatePayment', validatePayment)
        if (validatePayment) {
           
            let paymentDoc = {};
            if(((req.body ||{}).error ||{}).code){
                paymentDoc.txnid = orderDetail.receipt;;
                paymentDoc.status = 'failure';
                paymentDoc.roid = orderDetail.error.metadata.order_id;
                paymentDoc.mihpayid = orderDetail.error.metadata.payment_id;
                paymentDoc.error = orderDetail.error.code;
                paymentDoc.error_Message = orderDetail.error.description;
                paymentDoc.error_reason = orderDetail.error.reason;
                paymentDoc.error_source = orderDetail.error.source;
                paymentDoc.error_step = orderDetail.error.step;
            } else {
                // need to add more fields from payment.js models
                paymentDoc.txnid = orderDetail.receipt;
                paymentDoc.status = 'success';
                paymentDoc.roid = orderDetail.razorpay_order_id;
                paymentDoc.mihpayid = orderDetail.razorpay_payment_id;
                paymentDoc.signature = orderDetail.razorpay_signature;
                paymentDoc.currency = orderDetail.currency;
                paymentDoc.createdBy = orderDetail.userId;
            }
            let payModel = new Payment(paymentDoc);
            let paymentDocD = await payModel.save();
            let orderDoc = await Order.create({
                payment: paymentDocD._id,
                orderId: orderDetail.receipt,
                entity: orderDetail.entity,
                rid: orderDetail.id,
                quantity: orderDetail.quantity,
                searchQuantity: orderDetail.searchQuantity,
                amount: orderDetail.amount,
                searchAmount: orderDetail.amount + '',
                course: orderDetail.courseList,
                purchasedBy: orderDetail.userId,
                createdBy: orderDetail.userId
            })
            let userArray = await UserModel.find({ role: 'admin' });
            for (let u of userArray) {
                let data = { type: 'Purchase' };
                if (req.userId) {
                    data.createdBy = req.userId;
                }
                data.to = u._id;
                data.msg = 'Payment has been done for Order No. ' + orderDetail.receipt
                await NotiModel.create(data);
            }
            return res.status(200).send({'status': 'success', 'order':'success', txnid: orderDetail.receipt})
        } else {
            return res.status(403).send({'status': 'failure', 'order':'failure', txnid: orderDetail.receipt})
        }
    } catch (err) {
        log.info({ message: 'Payment paymentCallback2 Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send({'status': 'failure', 'order':'failure', txnid: orderDetail.receipt});
    }
}

async function orderManually(req, res) {
    try {
        let courses = [];
        if (req.body.courseName) {
            courses = await CourseModel.find({ _id: req.body.courseName }).lean();
            courses = courses;
        } else if (req.body.subjectName) {
            courses = await CourseModel.find({ subjectName: req.body.subjectName }).lean();
        } else if (req.body.categoryName) {
            courses = await CourseModel.find({ categoryName: req.body.categoryName }).lean();
        } else if (req.body.rootCategoryName) {
            courses = await CourseModel.find({ rootCategoryName: req.body.rootCategoryName }).lean();
        }
        let txnid = getOrderId();
        let courseList = [];
        let price = 0;
        let priceWithGst = 0;
        let gst = 0;
        for (let c of courses) {
            let alreadyAss = await Order.find({ purchasedBy: req.body.fullName, course: c._id }).populate({ path: 'payment', model: 'payments' }).lean();
            if (!alreadyAss || (alreadyAss || {}).length === 0) {
                price += c.price;
                courseList.push(c._id);
            } else {
                let flag = false;
                for (let orderDetail of alreadyAss) {
                    if (((orderDetail || {}).payment || {}).status === 'success') {
                        flag = true;
                    }
                }
                if (!flag) {
                    price += c.price;
                    courseList.push(c._id);

                }
            }
        }
        priceWithGst = price * 0.18;
        gst = priceWithGst + price;
        if (courseList.length === 0) return res.status(500).send('Course Already Purchased by this student');
        let payModel = new Payment({
            txnid: txnid,
            status: 'success',
            mode: 'Manual',
            unmappedstatus: 'captured',
            amount: gst,
        });
        let paymentDoc = await payModel.save();
        let orderModel = new Order({
            orderId: txnid,
            quantity: courseList.length,
            searchQuantity: courseList.length + '',
            amount: gst,
            gst: priceWithGst,
            searchAmount: gst + '',
            course: courseList,
            purchasedBy: req.body.fullName,
            payment: paymentDoc._id
        })
        let orderCreate = await orderModel.save()
        return res.status(200).send(orderCreate);
    } catch (err) {
        log.info({ message: 'Payment orderManually Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error : ' + err.toString());
    }
}

function getPaymentStatus(req, res, next) {
    let txnid = req.params.txnid;
    payumoney.paymentResponse(txnid, function(err, response) {
        if (err) {
            log.info({ message: 'Payment getPaymentStatus Error', label: 'ERROR', msg: err.stack });
            return res.status(500).send('Internal Server Error : ' + err.toString());
        } else {
            log.info({ message: 'Payment getPaymentStatus Error', label: 'ERROR', msg: response });
            return res.status(200).send(response);
        }
    });
}

function checkHash(req) {
    let body=req.body.razorpay_order_id + "|" + req.body.razorpay_payment_id;
    var expectedSignature = crypto.createHmac('sha256', PAYMENT.KEY_SECRET).update(body.toString()).digest('hex');
    console.log("sig received " ,req.body.razorpay_signature);
    console.log("sig generated " ,expectedSignature);
    var response = false;
    if(expectedSignature === req.body.razorpay_signature)
        response=true;
    return response;
}

function getHash(req, res, next) {
    let hash = '';
    let key = 'sVx6BqFf';
    let txnid = 'TXN15854889744521';
    let amount = '2.00';
    let productinfo = 'SkillAdda Course';
    let fname = 'Siva';
    let email = 'dsmk06@gmail.com';
    let status = 'failure';
    let salt = '2EOEfj4Yc0';
    var keyString = salt + '|' + status + '|||||||||||' + email + '|' + fname + '|' + productinfo + '|' + amount + '|' + txnid + '|' + key;
    var cryp = crypto.createHash('sha512');
    cryp.update(keyString);
    hash = cryp.digest('hex');
    let hash1 = sha512(keyString);
    return res.status(200).send({ hash: hash });

}

async function paymentRefund(req, res, next) {
    try {
        let paymentDoc = await refundCallback(req.body);
        if (paymentDoc.message !== 'Refund Initiated') {
            log.info({ message: 'Payment paymentRefund1 Error', label: 'ERROR', msg: JSON.stringify(paymentDoc) });
            return res.status(500).send('Internal Server Error : ' + err.toString());
        }
        let fetchOrder = await Order.findById(req.body._id);
        let updatePayment = await Payment.findById(fetchOrder.payment);
        updatePayment.status = paymentDoc.message;
        await updatePayment.save();
        return res.status(200).send(paymentDoc);
    } catch (err) {
        log.info({ message: 'Payment paymentRefund Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error : ' + err.toString());
    }

}

function refundCallback(data) {
    return new Promise((resolve, reject) => {
        payumoney.refundPayment(data.paymentId, data.amount, function(err, response) {
            if (err) {
                reject(err)
            } else {
                resolve(response);
            }
        });
    })
}

function paymentRefundStatus(req, res, next) {
    payumoney.refundStatus("paymentId", function(err, response) {
        if (err) {
            log.info({ message: 'Payment paymentRefundStatus Error', label: 'ERROR', msg: err.stack });
            return res.status(500).send('Internal Server Error : ' + err.toString());
        } else {
            console.log(response);
        }
    });
}

function getOrderId() {
    let now = Date.now().toString(); // '1492341545873'
    // pad with extra random digit
    now += now + Math.floor(Math.random() * 10);
    // format
    return 'TXN' + [now.slice(0, 4), now.slice(4, 10), now.slice(10, 14)].join('');
}

function generateOrderId(order){
    console.log('order==', order);
    return new Promise((resolve, reject) => {
        var options = {
            amount: order.amount,  // amount in the smallest currency unit
            currency: "INR",
            receipt: order.receiptId
          };
          razorPayInstance.orders.create(options, function(err, order) {
            if(err){
                console.log('error:', err);
                reject(err);
            } else {
                console.log('order:', order);
                resolve(order);
            }
          });
    });
}
/*payu res: { mihpayid: '140093',
  mode: 'CC',
  status: 'success',
  unmappedstatus: 'captured',
  key: 'JUjvVuzM',
  txnid: 'TX10012354',
  amount: '10.0',
  addedon: '2018-09-02 12:09:44',
  productinfo: 'iPhone1',
  firstname: 'Kinkar',
  lastname: '',
  address1: '',
  address2: '',
  city: '',
  state: '',
  country: '',
  zipcode: '',
  email: 'kinkarsarkar53@gmail.com',
  phone: '8143326787',
  udf1: '',
  udf2: '',
  udf3: '',
  udf4: '',
  udf5: '',
  udf6: '',
  udf7: '',
  udf8: '',
  udf9: '',
  udf10: '',
  hash: '3f9a43ca4462fe9869a2bde0fb703cda81238c45c07c3a6c9c676f1dcdc5d59c144fd6c27296ce5e19db7367e929a494449172d43f494f7b489aadf2402397d3',
  field1: '233895',
  field2: '374163',
  field3: '20180902',
  field4: 'MC',
  field5: '543280291882',
  field6: '00',
  field7: '0',
  field8: '3DS',
  field9: ' Verification of Secure Hash Failed: E700 -- Approved -- Transaction Successful -- Unable to be determined--E000',
  PG_TYPE: 'AXISPG',
  encryptedPaymentId: '3F5D91D931D3D0485F706EAAA5CBF98D',
  bank_ref_num: '233895',
  bankcode: 'VISA',
  error: 'E000',
  error_Message: 'No Error',
  name_on_card: 'Test',
  cardnum: '401200XXXXXX1112',
  cardhash: 'This field is no longer supported in postback params.',
  amount_split: '{"PAYU":"10.0"}',
  payuMoneyId: '295339',
  discount: '0.00',
  net_amount_debit: '10' }
  */