const async = require('async');
const Subject = require('../models/subject');
const Courses = require('../models/course');
const User = require('../models/users');
const Assessment = require('../models/assessment');
const log = require('../utils/winston');
module.exports = {
    homePageReport: homePageReport,
    fetchById: fetchById,
    fetchByOne: fetchByOne
}

function homePageReport(req, res, next) {
    async.parallel({
        courses: function(callback) {
            Courses.count({}).exec(function(err, courses) {
                if (err) {
                    callback({ status: 500, message: 'There was a problem finding the courses' }, null);
                } else {
                    callback(null, courses);
                }
            });
        },
        subjects: function(callback) {
            Subject.count({}).exec(function(err, subjects) {
                if (err) {
                    callback({ status: 500, message: 'There was a problem finding the subjects' }, null);
                } else {
                    callback(null, subjects);
                }
            });
        },
        users: function(callback) {
            User.count({}).exec(function(err, users) {
                if (err) {
                    callback({ status: 500, message: 'There was a problem finding the users' }, null);
                } else {
                    callback(null, users);
                }
            });
        },
        assessment: function(callback) {
            Assessment.count({}).exec(function(err, assessment) {
                if (err) {
                    callback({ status: 500, message: 'There was a problem finding the assessment' }, null);
                } else {
                    callback(null, assessment);
                }
            });
        }
    }, function(err, results) {
        // results now equals to: results.one: 'abc\n', results.two: 'xyz\n'
        res.status(200).send(results);
    });
}

function fetchById(req, res, next) {
    Subject.findById(req.params.id, function(err, subject) {
        if (err) {
            log.info({ message: 'Report fetchById Error', label: 'ERROR', msg: err.stack });
            return res.status(500).send('There was a problem finding the subject: ' + err.toString());
        } else if (!subject) return res.status(401).send("There was a problem finding the subject.");
        res.status(200).send(subject);
    });
}

function fetchByOne(req, res, next) {
    Subject.find(req.body, function(err, subjects) {
        if (err) {
            log.info({ message: 'Report fetchByOne Error', label: 'ERROR', msg: err.stack });
            return res.status(500).send('There was a problem finding the subject: ' + err.toString());
        } else if (!subjects) return res.status(401).send("There was a problem finding the subjects.");
        res.status(200).send(subjects);
    });
}