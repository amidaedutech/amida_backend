const async = require('async');
const mongoose = require('mongoose');
const ObjectId = mongoose.Types.ObjectId;
const Faq = require('../models/faq');
const Review = require('../models/review');
const Courses = require('../models/course');
const log = require('../utils/winston');
module.exports = {
    fetch: fetch,
    fetchById: fetchById,
    fetchByOne: fetchByOne,
    createFaq: createFaq
}


function fetch(req, res, next) {
    Faq.find({}, function(err, faqs) {
        if (err) {
            log.info({ message: 'Faq fetch Error', label: 'ERROR', msg: err.stack });
            return res.status(500).send('Internal Server Error : ' + err.toString());
        } else if (!faqs) return res.status(401).send("There was a problem finding the faq list.");
        res.status(200).send(faqs);
    });
}

function fetchById(req, res, next) {
    Faq.findById(req.params.id, function(err, faq) {
        if (err) {
            log.info({ message: 'Faq fetchById Error', label: 'ERROR', msg: err.stack });
            return res.status(500).send('Internal Server Error : ' + err.toString());
        } else if (!faq) return res.status(401).send("There was a problem finding the faq.");
        res.status(200).send(faq);
    });
}

function fetchByOne(req, res, next) {
    Faq
        .find(req.body)
        .populate({
            path: 'course',
            model: 'courses'
        })
        .populate({
            path: 'faqBy',
            model: 'user'
        })
        .exec(function(err, faqlist) {
            if (err) {
                log.info({ message: 'Faq fetchByOne Error', label: 'ERROR', msg: err.stack });
                return res.status(500).send('Internal Server Error : ' + err.toString());
            }
            res.status(200).send(faqlist);
        });
}

function createFaq(req, res, next) {
    Faq.create(req.body,
        function(err, faq) {
            if (err) {
                log.info({ message: 'Faq createFaq Error', label: 'ERROR', msg: err.stack });
                return res.status(500).send('Internal Server Error : ' + err.toString());
            }
            res.status(200).send(faq);
        });
}