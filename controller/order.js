const fs = require('fs');
const path = require('path');
var request = require('request').defaults({ encoding: null });
const mongoose = require('mongoose');
const ObjectId = mongoose.Types.ObjectId;
const Order = require('../models/order');
const Review = require('../models/review');
const Courses = require('../models/course');
const Payment = require('../models/payment');
const utilso = require('../utils/fileoperation');
const log = require('../utils/winston');
module.exports = {
    save: save,
    fetch: fetch,
    fetchById: fetchById,
    fetchByTxnId: fetchByTxnId,
    fetchByOne: fetchByOne,
    createReview: createReview,
    getReviews: getReviews,
    fetchAllReviews: fetchAllReviews,
    getPurchasedCourse: getPurchasedCourse,
    getRelatedCourse: getRelatedCourse,
    deleteReview: deleteReview
}

function save(req, res, next) {
    Payment.create(req.body.payment, function(err, paymentDoc) {
        if (err) {
            log.info({ message: 'Order save Error', label: 'ERROR', msg: err.stack });
            return res.status(500).send('Internal Server Error : ' + err.toString());
        }
        req.body.orders.payment = paymentDoc._id;
        Order.create(req.body.orders, function(err, doc) {
            if (err) {
                log.info({ message: 'Order create Error', label: 'ERROR', msg: err.stack });
                return res.status(500).send('Internal Server Error : ' + err.toString());
            }
            res.status(200).send({ status: 200, statusText: 'Order Placed', message: 'Your Order Placed Successfully' });
        });
    });
}

async function fetch(req, res, next) {
    try {
        let courseQuery = {};
        if (((req.body || {}).filter || {}).filterText) {
            var re = new RegExp(req.body.filter.filterText, 'i');
            courseQuery.orderId = { $regex: re };
        }

        if (((req.body || {}).filter || {}).rootCategoryName) {
            courseQuery['course.rootCategoryName'] = req.body.filter.rootCategoryName
        }
        if (((req.body || {}).filter || {}).categoryName) {
            courseQuery['course.categoryName'] = req.body.filter.categoryName
        }
        if (((req.body || {}).filter || {}).subjectName) {
            courseQuery['course.subjectName'] = req.body.filter.subjectName
        }
        if (((req.body || {}).filter || {}).courseName) {
            courseQuery['course._id'] = ObjectId(req.body.filter.courseName);
        }
        // let orders = await Order.find(filterObj).skip(req.body.skip).limit(req.body.limit).sort({ created: -1 })
        //     .populate({ path: 'course', model: 'courses', match: courseQuery }).populate({ path: 'payment', model: 'payments' }).populate({ path: 'purchasedBy', model: 'user' });

        let ordercount = await Order.aggregate([
            { $lookup: { from: "courses", localField: "course", foreignField: "_id", as: "course" } },
            { $lookup: { from: "payments", localField: "payment", foreignField: "_id", as: "payment" } },
            { $lookup: { from: "users", localField: "purchasedBy", foreignField: "_id", as: "purchasedBy" } },
            { $match: courseQuery }
        ]);
        let orders = await Order.aggregate([
            { $lookup: { from: "courses", localField: "course", foreignField: "_id", as: "course" } },
            { $lookup: { from: "payments", localField: "payment", foreignField: "_id", as: "payment" } },
            { $lookup: { from: "users", localField: "purchasedBy", foreignField: "_id", as: "purchasedBy" } },
            { $match: courseQuery },
            { $sort: { created: -1 } },
            { $limit: req.body.skip + req.body.limit },
            { $skip: req.body.skip }

        ]);
        return res.send({
            limit: req.body.limit,
            offset: req.body.offset,
            page: req.body.offset,
            skip: req.body.skip,
            total: ordercount.length,
            docs: orders
        });
    } catch (err) {
        log.info({ message: 'Order fetch Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error : ' + err.toString());
    }
}

function fetchById(req, res, next) {
    Order.findById(req.params.id, function(err, order) {
        if (err) {
            log.info({ message: 'Order fetchById Error', label: 'ERROR', msg: err.stack });
            return res.status(500).send('Internal Server Error : ' + err.toString());
        } else if (!order) return res.status(401).send("There was a problem finding the order.");
        res.status(200).send(order);
    });
}

function fetchByTxnId(req, res, next) {
    Order
        .findOne({ orderId: req.params.id })
        .populate({
            path: 'course',
            model: 'courses'
        })
        .populate({
            path: 'payment',
            model: 'payments'
        })
        .exec(function(err, orderDoc) {
            if (err) {
                log.info({ message: 'Order fetchByTxnId Error', label: 'ERROR', msg: err.stack });
                return res.status(500).send('Internal Server Error : ' + err.toString());
            }
            res.status(200).send(orderDoc);
        });
}

async function fetchByOne(req, res, next) {
    try {
        let orderlist = await Order.find({ purchasedBy: req.body.purchasedBy }).populate({
                path: 'course',
                model: 'courses'
            })
            .populate({
                path: 'payment',
                model: 'payments'
            }).sort([
                ['created', -1]
            ]);
        return res.status(200).send(orderlist);
    } catch (err) {
        log.info({ message: 'Order fetchByOne Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error : ' + err.toString());
    }

}

function createReview(req, res, next) {
    Review.find({ course: req.body.course, reviwedBy: req.body.reviwedBy }, function(err, doc) {
        if (err) {
            log.info({ message: 'Order createReview Error', label: 'ERROR', msg: err.stack });
            return res.status(500).send('Internal Server Error : ' + err.toString());
        } else if (doc && doc.length > 0) return res.status(302).send("Already reviewed by you.");
        else {
            Review.create(req.body,
                function(err, review) {
                    if (err) {
                        log.info({ message: 'Order createReview1 Error', label: 'ERROR', msg: err.stack });
                        return res.status(500).send('Internal Server Error : ' + err.toString());
                    }
                    Review.find({ course: req.body.course }, function(err, reviewList) {
                        if (err) {
                            log.info({ message: 'Order createReview2 Error', label: 'ERROR', msg: err.stack });
                            return res.status(500).send('Internal Server Error : ' + err.toString());
                        }
                        Courses.update({ _id: req.body.course }, { $set: getReviewAvg(reviewList) }, function(err, doc) {
                            if (err) {
                                log.info({ message: 'Order createReview2 Error', label: 'ERROR', msg: err.stack });
                                return res.status(500).send('Internal Server Error : ' + err.toString());
                            }
                            res.status(200).send(review);
                        });
                    });
                });
        }
    });
}

function getReviews(req, res, next) {
    Review.find({ course: req.params.id }).populate({ path: 'reviwedBy', model: 'user' }).exec(function(err, reviews) {
        if (err) {
            log.info({ message: 'Order getReviews Error', label: 'ERROR', msg: err.stack });
            return res.status(500).send('Internal Server Error : ' + err.toString());
        }
        res.status(200).send(reviews);
    });
}

async function getPurchasedCourse(req, res, next) {
    try {
        let orders = await Order.findOne({ orderId: req.body.orderId }).populate({ path: 'payment', model: 'payments' })
        if (!orders) return res.status(404).send("No orders found.");
        let course = await Courses.findById(req.body.id);
        if (!course) return res.status(404).send("No course found.");
        if (((orders || {}).payment || {}).status === 'success') {
            res.status(200).send(course);
        } else {
            return res.status(404).send("payment was not successfully done for this course.");
        }
    } catch (err) {
        log.info({ message: 'Order getPurchasedCourse Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error : ' + err.toString());
    }
}

async function getRelatedCourse(req, res, next) {
    try {
        let course = await Courses.findById(req.params.id);
        if (!course) return res.status(404).send("No courses found.");
        let category = await Courses.find({ categoryName: course.categoryName });
        return res.status(200).send(category);
    } catch (err) {
        log.info({ message: 'Order getRelatedCourse Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error : ' + err.toString());
    }
}

async function fetchAllReviews(req, res) {
    try {
        let reviewQuery = {};
        if (((req.body || {}).filter || {}).filterText) {
            var re = new RegExp(req.body.filter.filterText, 'i');
            reviewQuery.review = { $regex: re };
        }

        if (((req.body || {}).filter || {}).username) {
            reviewQuery['reviwedBy.username'] = req.body.filter.username
        }
        let reviewcount = await Review.aggregate([
            { $lookup: { from: "users", localField: "reviwedBy", foreignField: "_id", as: "reviwedBy" } },
            { $match: reviewQuery }
        ]);
        let reviews = await Review.aggregate([
            { $lookup: { from: "users", localField: "reviwedBy", foreignField: "_id", as: "reviwedBy" } },
            { $match: reviewQuery },
            { $sort: { created: -1 } },
            { $limit: req.body.skip + req.body.limit },
            { $skip: req.body.skip }

        ]);
        return res.send({
            limit: req.body.limit,
            offset: req.body.offset,
            page: req.body.offset,
            skip: req.body.skip,
            total: reviewcount.length,
            docs: reviews
        });
    } catch (err) {
        log.info({ message: 'Order fetchAllReviews Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error : ' + err.toString());
    }
}

async function deleteReview(req, res) {
    try {
        let review = await Review.remove({ _id: req.params.id });
        return res.status(200).send(review);
    } catch (err) {
        log.info({ message: 'Order deleteReview Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error : ' + err.toString());
    }
}

function getReviewAvg(reviewList) {
    var rating1 = 0;
    var rating2 = 0;
    var rating3 = 0;
    var rating4 = 0;
    var rating5 = 0;
    var avgRate = 1;
    reviewList.forEach(function(item) {
        if (item.rating === 1) {
            rating1++;
        } else if (item.rating === 2) {
            rating2++;
        } else if (item.rating === 3) {
            rating3++;
        } else if (item.rating === 4) {
            rating4++;
        } else if (item.rating === 5) {
            rating5++;
        }
    });

    avgRate = (5 * rating5 + 4 * rating4 + 3 * rating3 + 2 * rating2 + 1 * rating1) / reviewList.length;
    return { aggregateRating: avgRate, totalRating: reviewList.length };

}