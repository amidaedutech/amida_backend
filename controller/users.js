const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = mongoose.Types.ObjectId;
var jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens
var bcrypt = require('bcryptjs');
var config = require('../bin/server-config'); // get config file
const formidable = require('formidable');
const fs = require('fs');
const path = require('path');
const uniqid = require('uniqid');
const Auth = require('../models/auth');
const User = require('../models/users');
const Contactus = require('../models/contactus');
const mail = require('../utils/mail');
const utilenc = require('../utils/utils');
const fileopr = require('../utils/fileoperation');
const NotiModel = require('../models/notification');
var socket = require('./socketconfig');
const log = require('../utils/winston');
// var socket = require('socket.io-client')(process.env.NODE_HOST + ':' + process.env.PORT);
// socket.on('connect', function() {
//     console.log('connect clicent==')
// });
// socket.on('disconnect', function() {});
module.exports = {
    fetch: fetch,
    fetchById: fetchById,
    deleteById: deleteById,
    register: register,
    update: update,
    save: save,
    login: login,
    socialLogin: socialLogin,
    logout: logout,
    verifyToken: verifyToken,
    refreshToken: refreshToken,
    updatePassword: updatePassword,
    uploadImage: uploadImage,
    forgetPassword: forgetPassword,
    resetPwd: resetPwd,
    contactus: contactus,
    fetchInq: fetchInq,
    fetchByIdInq: fetchByIdInq,
    deleteByIdInq: deleteByIdInq,
    updateInq: updateInq,
    isActive: isActive,
    notiByTo: notiByTo,
    notiByToCount: notiByToCount,
    readNoti: readNoti,
    sendMailForVerify: sendMailForVerify,
    mailVerify: mailVerify,
    fetchByOne: fetchByOne,
    fetchForAutoCompl: fetchForAutoCompl
}

async function fetch(req, res) {
    try {
        let userDocs = await getUsers(req);
        return res.status(200).send(userDocs);
    } catch (err) {
        log.info({ message: 'Users fetch Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error: ' + err.toString());
    }

}
async function fetchForAutoCompl(req, res) {
    try {
        // here need to add service or bisiness
        let query = {};
        if (req.body.fullName) {
            const re = new RegExp(req.body.fullName, 'gi'); // gi also check once
            query = { fullName: { $regex: re } };
        }
        query.role = 'student';
        const userList = await User.find(query).sort({ fullName: 1 }).limit(15);
        return res.status(200).send(userList);
    } catch (err) {
        log.info({ message: 'Users fetchForAutoCompl Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error: ' + err.toString());
    }
}
async function getUsers(req) {
    return new Promise((resolve, reject) => {
        let query = {};
        if (req.body.searchString) {
            query.$or = [];
            const re = new RegExp(req.body.searchString, 'gi');
            query.$or.push({ username: { $regex: re } });
            query.$or.push({ email: { $regex: re } });
        }
        if (req.body.role) {
            query.role = req.body.role;
        }
        User.paginate(query, { page: parseInt(req.body.skip), limit: parseInt(req.body.limit), sort: req.body.sort },
            async(err, userDocs) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(userDocs);
                }
            });
    });
}

function fetchById(req, res, next) {
    User.findById(req.params.id, { hashedPassword: 0 }, function(err, user) {
        if (err) {
            log.info({ message: 'Users fetchById Error', label: 'ERROR', msg: err.stack });
            return res.status(500).send('Internal Server Error: ' + err.toString());
        }
        if (!user) return res.status(404).send("No user found.");
        res.status(200).send(user);
    });
}

function fetchByOne(req, res, next) {
    User.findOne(req.body, { hashedPassword: 0 }, function(err, user) {
        if (err) {
            log.info({ message: 'Users fetchByOne Error', label: 'ERROR', msg: err.stack });
            return res.status(500).send('Internal Server Error: ' + err.toString());
        }
        if (!user) return res.status(404).send("No user found.");
        res.status(200).send(user);
    });
}

function deleteById(req, res, next) {
    User.findByIdAndRemove(req.params.id, function(err, user) {
        if (err) {
            log.info({ message: 'Users deleteById Error', label: 'ERROR', msg: err.stack });
            return res.status(500).send('Internal Server Error: ' + err.toString());
        }
        res.status(200).send("User: " + user.name + " was deleted.");
    });
}

function register(req, res, next) {
    var hashedPassword = bcrypt.hashSync(req.body.hashedPassword, 8);
    req.body.hashedPassword = hashedPassword;
    req.body.status = 'Active';
    User.create(req.body, async(err, user) => {
        if (err && err.code === 11000) {
            return res.status(500).send("Your Already registed, please login..");
        } else if (err) {
            log.info({ message: 'Users register Error', label: 'ERROR', msg: err.stack });
            return res.status(500).send('Internal Server Error: register' + err.toString());
        }
        var token = jwt.sign({ id: user._id }, config.secret, {
            expiresIn: 86400 // expires in 24 hours
        });
        Auth.create({ email: req.body.email, token: token, loginStatus: false }, function(error, newdoc) {});
        res.status(200).send({ auth: true, token: token });
    });
}

async function update(req, res, next) {
    try {
        let user = await User.update({ _id: req.params.id }, { $set: req.body }, { upsert: true })
        res.status(200).send(user);
    } catch (err) {
        log.info({ message: 'Users update Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error' + err.toString());
    }
}

function save(req, res, next) {
    User.findOne({ _id: req.body._id }, function(err, user) {
        // console.log("user", user);
        user.status = req.body.status;
        user.role = req.body.role;
        user.save(function(err, userDoc) {
            if (err) {
                log.info({ message: 'Users save Error', label: 'ERROR', msg: err.stack });
                return res.status(500).send('Internal Server Error' + err.toString());
            }
            res.status(200).send(userDoc);
        });
    });
}

async function login(req, res, next) {
    try {
        let user = await User.findOne({ email: req.body.email });
        if (!user) return res.status(404).send('No user found.');
        //check user is active or not
        if (user.status !== 'Active')
            return res.status(401).send('You are not an active member');

        // check if the password is valid
        var passwordIsValid = bcrypt.compareSync(req.body.hashedPassword, user.hashedPassword);
        if (!passwordIsValid) return res.status(401).send('Invalid Username and Password');
        var token = jwt.sign({ id: user._id }, config.secret, {
            expiresIn: 86400 // expires in 24 hours
        });
        let authdoc = await Auth.findOne({ email: req.body.email });
        if (!authdoc) {
            await Auth.create({ email: req.body.email, token: token, loginStatus: true });
            res.status(200).send({ user: user, auth: true, accessToken: token, expiresIn: 86400, isLoggedIn: true });
        } else {
            await Auth.update({ email: req.body.email }, { $set: { token: token, loginStatus: true } });
            res.status(200).send({ user: user, auth: true, accessToken: token, expiresIn: 86400, isLoggedIn: authdoc.loginStatus });
        }
    } catch (err) {
        log.info({ message: 'Users login Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error' + err.toString());
    }

}

function logout(req, res, next) {
    Auth.update({ email: req.body.email }, { $set: { loginStatus: false, token: null } }, function(err, docRes) {
        return res.status(200).send({ auth: false, token: null });
    });
}

async function isActive(req, res) {
    try {
        let auth = await Auth.findOne({ token: req.body.token });
        if (!auth) return res.status(200).send({ isActive: false });
        return res.status(200).send({ isActive: true });
    } catch (err) {
        log.info({ message: 'Users isActive Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error' + err.toString());
    }
}

function verifyToken(req, res, next) {
    User.findById(req.userId, { hashedPassword: 0 }, function(err, user) {
        if (err) {
            log.info({ message: 'Users verifyToken Error', label: 'ERROR', msg: err.stack });
            return res.status(500).send('Internal Server Error' + err.toString());
        }
        if (!user) return res.status(404).send("No user found.");
        res.status(200).send(user);
    });
}

async function refreshToken(req, res, next) {
    var token = jwt.sign({ id: req.body._id }, config.secret, {
        expiresIn: 86400 // expires in 24 hours
    });
    await Auth.update({ email: req.body.email }, { $set: { loginStatus: true, token: token } }, function(err, docRes) {});
    res.status(200).send({ auth: true, accessToken: token });
}

function updatePassword(req, res, next) {
    try {
        User.findOne({ email: req.body.email }, function(err, user) {
            if (err) {
                log.info({ message: 'Users updatePassword Error', label: 'ERROR', msg: err.stack });
                return res.status(500).send('Internal Server Error' + err.toString());
            }
            if (!user) return res.status(404).send('No user found.');

            // check if the password is valid
            if (!req.body.socialFlag) {
                var passwordIsValid = bcrypt.compareSync(req.body.oldPassword, user.hashedPassword);
                if (!passwordIsValid) return res.status(401).send('Incorrect old password');
            }
            var hashedPassword = bcrypt.hashSync(req.body.hashedPassword, 8);
            User.findByIdAndUpdate(user._id, { hashedPassword: hashedPassword }, { new: true }, function(err, user) {
                if (err) {
                    log.info({ message: 'Users findByIdAndUpdate Error', label: 'ERROR', msg: err.stack });
                    return res.status(500).send('Internal Server Error' + err.toString());
                }
                res.status(200).send(user);
            });
        });
    } catch (err) {
        log.info({ message: 'Users updatePassword1 Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error' + err.toString());
    }
}

function uploadImage(req, res, next) {
    try {
        var filePath = '';
        var reqBody = {};
        var form = new formidable.IncomingForm();
        form.parse(req);
        form.on('field', function(name, value) {
            reqBody[name] = value;
            //console.log("name value", name, value);
        });

        form.on('fileBegin', function(name, file) {
            filePath = path.join('../SkillAdda_images/');
            if (!fs.existsSync(filePath)) {
                fs.mkdirSync(filePath);
            }
            filePath = path.join('../SkillAdda_images/users/');
            if (!fs.existsSync(filePath)) {
                fs.mkdirSync(filePath);
            }
            var fileName = file.name;
            var ext = fileName.substring(fileName.lastIndexOf('.') + 1);
            var uniqueName = uniqid() + '.' + ext;
            file.path = filePath + uniqueName;
            file.path = file.path.replace(/ /g, '_')
            filePath = file.path;
        });

        form.on('file', function(name, file) {
            console.log('Uploaded ' + file.name);
        });

        form.on('error', function(err) {
            if (err) return res.status(500).send("The max File Size exceeded, please reduce the size of file the default size is 200 mb.");
        });

        form.on('aborted', function() {
            return res.status(500).send("The request aborted, please try again.");
        });
        form.on('end', function() {
            User.findOne({ _id: ObjectId(reqBody.userId) }, function(err, userDoc) {
                if (userDoc) {
                    if (userDoc.image) {
                        if (fs.existsSync(userDoc.image)) {
                            fs.unlinkSync(userDoc.image);
                        }

                    }
                    userDoc.image = filePath;
                    userDoc.save(function(err, savedUser) {
                        if (err) {
                            log.info({ message: 'Users uploadImage Error', label: 'ERROR', msg: err.stack });
                            return res.status(500).send('Internal Server Error' + err.toString());
                        }
                        if (savedUser && savedUser.image) {
                            utilenc.encode_base64(savedUser.image).then((str) => {
                                savedUser.base64String = ("data:Image/*;base64," + str);
                                res.status(200).send(savedUser);
                            }, err => {
                                log.info({ message: 'Users uploadImage1 Error', label: 'ERROR', msg: err.stack });
                                return res.status(500).send('Internal Server Error' + err.toString());
                            });
                        } else {
                            res.status(200).send(savedUser);
                        }
                    });
                } else {
                    return res.status(500).send("There was a problem while saving course details.");
                }
            });
        });
    } catch (err) {
        log.info({ message: 'Users uploadImage3 Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error' + err.toString());
    }
}

async function forgetPassword(req, res) {
    try {
        let user = await User.findOne({ email: req.body.email });
        if (!user) return res.status(404).send('Entered Email Id is not registered.');
        let otp = Math.floor(100000 + Math.random() * 900000);
        var hashedPassword = bcrypt.hashSync(otp + '', 8);
        if (user.phone) {
            let sendSms = await utilenc.sms({ phone: user.phone, msg: `Your new password is ${otp} for SkillAddaEdu Login. Please Reset your password after login.` });
            // user.hashedPassword = hashedPassword
            //     //console.log("user", user);
            // await user.save();
            // return res.status(200).send({ msg: 'New password has been sent to your registered mobile number, please login and reset your password.' });
        }
        if (user.email) {
            let data = {};
            data.emailAddress = user.email;
            data.username = user.firstName ? user.firstName : user.email;
            data.subject = 'SkillAdda | Password for your account';
            data.hashedPassword = otp;
            let mailSend = await mail.sendingMail('forgotpwd', data);
        }
        user.hashedPassword = hashedPassword;
        await user.save();
        return res.status(200).send({ msg: 'New password has been sent to your registered Email Id or Mobile, please login and reset your password.' });
    } catch (err) {
        log.info({ message: 'Users forgetPassword Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error' + err.toString());
    }
}

function resetPwd(req, res, next) {
    var hashedPassword = bcrypt.hashSync(req.body.hashedPassword, 8);
    User.update({ email: req.body.email }, { $set: { hashedPassword: hashedPassword } }, function(err, userDoc) {
        if (err) {
            log.info({ message: 'Users resetPwd Error', label: 'ERROR', msg: err.stack });
            return res.status(500).send('Internal Server Error' + err.toString());
        }
        if (!userDoc) return res.status(404).send('Your not valid user!');
        res.status(200).send(userDoc);
    })
}

async function contactus(req, res, next) {
    try {
        let contact = new Contactus(req.body);
        let doc = await contact.save()
        let userArray = await User.find({ role: 'admin' });
        for (let u of userArray) {
            let data = { type: 'Inquiry' };
            if (req.userId) {
                data.createdBy = req.userId;
            }
            data.to = u._id;
            data.msg = 'Inquiry has made by ' + req.body.email + ' - ' + req.body.message;
            await NotiModel.create(data);
        }
        res.status(200).send(doc);
    } catch (err) {
        log.info({ message: 'Users contactus Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error' + err.toString());
    }
}

async function socialLogin(req, res, next) {
    try {
        let user = await User.findOne({ email: req.body.email })
        if (!user) {
            req.body.hashedPassword = bcrypt.hashSync('testpwd123', 8);
            req.body.username = req.body.email;
            req.body.role = 'student';
            req.body.status = 'Active';
            req.body.emailVerify = true;
            let userDoc = await User.create(req.body);
            let token = jwt.sign({ id: userDoc._id }, config.secret, {
                expiresIn: 86400 // expires in 24 hours
            });
            await Auth.create({ email: req.body.email, token: token, loginStatus: true });
            return res.status(200).send({ user: userDoc, auth: true, accessToken: token, expiresIn: 86400 });
        } else {
            // if user is found and password is valid
            // create a token
            let token = jwt.sign({ id: user._id }, config.secret, {
                expiresIn: 86400 // expires in 24 hours
            });
            let passwordIsValid = false;
            if (req.body.googleId === user.googleId) {
                passwordIsValid = true;
            } else if (req.body.fbId === user.fbId) {
                passwordIsValid = true;
            }
            if (!passwordIsValid) return res.status(401).send('Invalid Username and Password');
            const authdoc = await Auth.findOne({ email: req.body.email });
            await Auth.update({ email: req.body.email }, { $set: { token: token, loginStatus: true } }, function(err, docRes) {});
            return res.status(200).send({ user: user, auth: true, accessToken: token, expiresIn: 86400, isLoggedIn: authdoc.loginStatus });
        }
    } catch (err) {
        log.info({ message: 'Users socialLogin Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error' + err.toString());
    }
}

function fetchInq(req, res) {
    const matchFields = {};
    if (req.body.searchString) {
        matchFields.name = { $regex: new RegExp(req.body.searchString, 'i') };
    }
    if (req.body.sp) {
        matchFields.sp = req.body.sp;
    }
    Contactus.paginate(matchFields, { page: parseInt(req.body.skip), limit: parseInt(req.body.limit), sort: { created: -1 } }, (err, inqDocs) => {
        if (err) {
            log.info({ message: 'Users fetchInq Error', label: 'ERROR', msg: err.stack });
            return res.status(500).send('Internal Server Error' + err.toString());
        } else {
            return res.status(200).send(inqDocs);
        }
    });
}
async function fetchByIdInq(req, res) {

}
async function deleteByIdInq(req, res) {

}
async function updateInq(req, res) {
    try {
        // if (((res.locals || {}).jwtPayload || {}).id) {
        //     req.body.modifiedBy = res.locals.jwtPayload.id;
        // }
        await Contactus.updateOne({ _id: req.body._id }, req.body);
        return res.status(200).end();
    } catch (err) {
        log.info({ message: 'Users updateInq Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error' + err.toString());
    }
}

async function notiByTo(req, res) {
    try {
        let notiDocs = await NotiModel.find({ to: req.body.to, status: 'Unread' });
        return res.status(200).send(notiDocs);
    } catch (err) {
        log.info({ message: 'Users notiByTo Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error' + err.toString());
    }
}

async function notiByToCount(req, res) {
    try {
        let count = await NotiModel.count({ to: req.body.to, status: 'Unread' });
        return res.send({ count: count });
    } catch (err) {
        log.info({ message: 'Users notiByToCount Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error' + err.toString());
    }
}

async function readNoti(req, res) {
    try {
        let doc = await NotiModel.update({ _id: req.body._id }, { $set: { status: 'Read' } });
        return res.send(doc);
    } catch (err) {
        log.info({ message: 'Users readNoti Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error' + err.toString());
    }
}
async function sendMailForVerify(req, res) {
    try {
        let userFind = await User.findOne(req.body);
        let data = {};
        data.emailAddress = userFind.email;
        data.username = userFind.firstName || userFind.email;
        data.subject = 'E-Mail ID Verification from SkillAdda';
        data.url = process.env.CLIENT_HOST + '/#/home/emailverify?id=' + userFind._id;
        let mailSend = await mail.sendingMail('mailverify', data);
        return res.send(mailSend);
    } catch (err) {
        log.info({ message: 'Users sendMailForVerify Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error' + err.toString());
    }
}

async function mailVerify(req, res) {
    try {
        let userFind = await User.findById(req.body._id);
        userFind.emailVerify = true;
        await userFind.save();
        return res.send({ msg: 'Your mail ID has been successfully verified.' });
    } catch (err) {
        log.info({ message: 'Users mailVerify Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error' + err.toString());
    }
}