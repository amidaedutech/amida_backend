const mongoose = require('mongoose');
const Order = require('../models/order');
const log = require('../utils/winston');
module.exports = {
    fetchByOne: fetchByOne
};

function fetchByOne(req, res, next) {
    Order
        .find({ purchasedBy: req.body.purchasedBy })
        .populate({
            path: 'course',
            model: 'courses'
        })
        .populate({
            path: 'payment',
            model: 'payments'
        }).sort([
            ['created', -1]
        ])
        .exec(function(err, orderlist) {
            if (err) return res.status(500).send("Internal server error while fetching the order list.");
            var courseList = [];
            let totalPrice = 0;
            orderlist.forEach(element => {
                totalPrice += element.amount;
                courseList = courseList.concat(element.course);
            });
            res.status(200).send({ courses: courseList, ordersLength: orderlist.length, totalPrice: totalPrice });
        });
}