var fs = require("fs"),
    url = require("url"),
    path = require("path");
const fse = require('fs-extra');
const log = require('../utils/winston');
module.exports = {
    download: download,
    upload: upload
};

function download(req, res, next) {
    // http://localhost:8888/api/video/S and T In Agri 17072017 0249p.mp4
    var fileName = req.query.filename;
    // if (req.params.filename) {
    //     fileName = "F://SkillAdda/SkillAdda_v/Vedio of Science and Technology in Agriculture Part -1/" + req.params.filename;
    // }
    var file = path.resolve(fileName);
    fs.stat(file, function(err, stats) {
        if (err) {
            if (err.code === 'ENOENT') {
                // 404 Error if file not found
                return res.sendStatus(404);
            }
            res.end(err);
        }
        var range = req.headers.range;
        if (!range) {
            return res.sendStatus(416);
        }
        var positions = range.replace(/bytes=/, "").split("-");
        var start = parseInt(positions[0], 10);
        var total = stats.size;
        var end = positions[1] ? parseInt(positions[1], 10) : total - 1;
        var chunksize = (end - start) + 1;

        res.writeHead(206, {
            "Content-Range": "bytes " + start + "-" + end + "/" + total,
            "Accept-Ranges": "bytes",
            "Content-Length": chunksize,
            "Content-Type": "video/mp4"
        });

        var stream = fs.createReadStream(file, { start: start, end: end })
            .on("open", function() {
                stream.pipe(res);
            }).on("error", function(err) {
                res.end(err);
            });
    });
}

function upload(req, res, next) {
    // let data = req.body;
    // console.log('req.body', req.body);
    // var videoPath = path.join('../' + '/SkillAdda_videos/'); // Register the upload path
    // if (!fs.existsSync(videoPath)) {
    //     fs.mkdirSync(videoPath);
    // }
    // if (data.coursePath) {
    //     videoPath = path.join('../SkillAdda_videos/' + data.coursePath + '/');
    //     if (!fs.existsSync(videoPath)) {
    //         fs.mkdirSync(videoPath);
    //     }
    // }
    // fs.ensureDir(videoPath); // Make sure that he upload path exits
    // req.pipe(req.busboy); // Pipe it trough busboy

    // req.busboy.on('file', (fieldname, file, filename) => {
    //     console.log(`Upload of '${filename}' started`);
    //     var filePath = videoPath + filename;
    //     // Create a write stream of the new file
    //     const fstream = fs.createWriteStream(path.join(videoPath, filename));
    //     // Pipe it trough
    //     file.pipe(fstream);

    //     // On finish of the upload
    //     fstream.on('close', () => {
    //         console.log(`Upload of '${filename}' finished`);
    //         res.send({});
    //     });
    // });
}