const _ = require('lodash');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = mongoose.Types.ObjectId;
const async = require('async');
const formidable = require('formidable');
const fs = require('fs');
const path = require('path');
const uniqid = require('uniqid');
var request = require('request').defaults({ encoding: null });
const Courses = require('../models/course');
const Content = require('../models/content');
var rimraf = require("rimraf");
const fileopr = require('../utils/fileoperation');
const log = require('../utils/winston');
module.exports = {
    fetch: fetch,
    getCourses: getCourses,
    getLatestCourse: getLatestCourse,
    fetchById: fetchById,
    fetchByOne: fetchByOne,
    deleteById: deleteById,
    update: update,
    getAll: getAll,
    fetchByType: fetchByType,
    getFilter: getFilter,
    remove: remove,
    saveCourse: saveCourse,
    uploadCourseImage: uploadCourseImage,
    docsUpload: docsUpload
}

function fetch(req, res, next) {
    // console.log('course: ', req.body);
    var result = {};
    result.docs = [];
    var filterObj = {};
    if (req.body.filterQuery) {
        var re = new RegExp(req.body.filterQuery, 'i');
        var matchFields = [];
        matchFields.push({ 'subjectCode': { $regex: re } }, { 'courseType': { $regex: re } }, { 'subjectName': { $regex: re } }, { 'headline': { $regex: re } }, { 'name': { $regex: re } }, { 'rootCategoryName': { $regex: re } }, { 'categoryName': { $regex: re } });
        filterObj.$or = matchFields;
    }

    Courses.paginate(filterObj, { page: _.toInteger(req.body.skip), limit: _.toInteger(req.body.limit) }, function(err, result) {
        if (err) {
            log.info({ message: 'Course fetch Error', label: 'ERROR', msg: err.stack });
            return res.status(500).send('Internal Server Error : ' + err.toString());
        } else {
            res.status(200).send(result);
        }
    });
}

function getLatestCourse(req, res) {
    Courses.find({ categoryName: req.body.name }).sort({ "created": -1 }).limit(15).exec(function(err, courses) {
        if (err) {
            log.info({ message: 'Course getLatestCourse Error', label: 'ERROR', msg: err.stack });
            return res.status(500).send('Internal Server Error : ' + err.toString());
        } else {
            res.status(200).send(courses);
        }
    });
}

function getCourses(req, res, next) {
    async.parallel({
        latest: function(callback) {
            Courses.find({}).sort({ "created": -1 }).limit(15).exec(function(err, courses) {
                if (err) {
                    callback({ status: 500, message: 'There was a problem finding the courses' }, null);
                } else {
                    res.status(200).send(courses);
                }
            });
        },
        popular: function(callback) {
            Courses.find({}).sort({ "aggregateRating": -1 }).limit(15).exec(function(err, courses) {
                if (err) {
                    callback({ status: 500, message: 'There was a problem finding the courses' }, null);
                } else {
                    res.status(200).send(courses);
                }
            });
        },
        recommended: function(callback) {
            Courses.find({}).limit(15).exec(function(err, courses) {
                if (err) {
                    callback({ status: 500, message: 'There was a problem finding the courses' }, null);
                } else {
                    res.status(200).send(courses);
                }
            });
        },
        discounted: function(callback) {
            Courses.find({}).limit(15).exec(function(err, courses) {
                if (err) {
                    callback({ status: 500, message: 'There was a problem finding the courses' }, null);
                } else {
                    res.status(200).send(courses);
                }
            });
        },
    }, function(err, results) {
        if (err) {
            log.info({ message: 'Course getCourses Error', label: 'ERROR', msg: err.stack });
            return res.status(500).send('Internal Server Error : ' + err.toString());
        }
        res.status(200).send(results);
    });
}

function fetchById(req, res, next) {
    Courses.findById(req.params.id, function(err, course) {
        if (err) {
            log.info({ message: 'Course fetchById Error', label: 'ERROR', msg: err.stack });
            return res.status(500).send('Internal Server Error : ' + err.toString());
        }
        if (!course) {
            return res.status(404).send("No course found.");
        } else {
            res.status(200).send(course);
        }
    });
}

function fetchByOne(req, res, next) {
    Courses.find(req.body, function(err, courses) {
        if (err) {
            log.info({ message: 'Course fetchByOne Error', label: 'ERROR', msg: err.stack });
            return res.status(500).send('Internal Server Error : ' + err.toString());
        } else if (!courses) return res.status(500).send("Courses Not found..");
        res.status(200).send(courses);
    });
}

function fetchByType(req, res, next) {
    if (req.params.type === 'latest') {
        Courses.find({}).sort({ "created": -1 }).limit(15).exec(function(err, courses) {
            if (err) {
                log.info({ message: 'Course fetchByType Error', label: 'ERROR', msg: err.stack });
                return res.status(500).send('Internal Server Error : ' + err.toString());
            }
            res.status(200).send(courses);
        });
    }

}

async function deleteById(req, res, next) {
    try {
        let cdoc = await Courses.findById(req.body.id);

        if (cdoc.image && fs.existsSync(cdoc.image)) {
            fs.unlinkSync(cdoc.image);
        }
        if (cdoc.htmlpath && fs.existsSync(cdoc.htmlpath)) {
            rimraf.sync(cdoc.htmlpath);
        }
        let vpath = path.join('../' + '/SkillAdda_videos/' + cdoc.name);
        if (fs.existsSync(vpath)) {
            var deleteFolderRecursive = function(path) {
                if (fs.existsSync(path)) {
                    fs.readdirSync(path).forEach(function(file, index) {
                        var curPath = path + "/" + file;
                        if (fs.lstatSync(curPath).isDirectory()) { // recurse
                            deleteFolderRecursive(curPath);
                        } else { // delete file
                            fs.unlinkSync(curPath);
                        }
                    });
                    fs.rmdirSync(path);
                }
            };
            deleteFolderRecursive(vpath);
        }
        await Content.deleteMany({ courseId: cdoc._id });
        let course = await Courses.deleteOne({ _id: ObjectId(req.body.id) });
        res.status(200).send(course);
    } catch (err) {
        log.info({ message: 'Course deleteById Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error : ' + err.toString());
    }
}

function update(req, res, next) {
    Courses.findByIdAndUpdate(req.params.id, req.body, { new: true }, function(err, course) {
        if (err) {
            log.info({ message: 'Course update Error', label: 'ERROR', msg: err.stack });
            return res.status(500).send('Internal Server Error : ' + err.toString());
        }
        res.status(200).send(course);
    });
}

function getAll(req, res, next) {
    Courses.find({}).populate({ path: 'rootcategory', model: 'root-categories' })
        .populate('category')
        .populate('subject')
        .exec(function(err, docs) {
            if (err) {
                log.info({ message: 'Course getAll Error', label: 'ERROR', msg: err.stack });
                return res.status(500).send('Internal Server Error : ' + err.toString());
            } else if (!docs) return res.status(404).send("There was a problem finding the categories.");
            res.status(200).send(docs);
        });
}

function getFilter(req, res, next) {
    var filterObj = {};
    if (req.body.rootCategoryName)
        filterObj.rootCategoryName = req.body.rootCategoryName;
    if (req.body.categoryName)
        filterObj.categoryName = req.body.categoryName;
    if (req.body.filterValue) {
        var re = new RegExp(req.body.filterValue, 'i');
        var matchFields = [];
        matchFields.push({ 'subjectCode': { $regex: re } }, { 'courseType': { $regex: re } }, { 'subjectName': { $regex: re } }, { 'headline': { $regex: re } }, { 'name': { $regex: re } }, { 'rootCategoryName': { $regex: re } }, { 'categoryName': { $regex: re } });
        filterObj.$or = matchFields;
    }
    if (req.body.maxPrice) {
        filterObj.price = { $range: [req.body.minPrice, req.body.maxPrice] };
    }
    if (req.body.price === 0) {
        filterObj.price = 0;
    }
    Courses.paginate(filterObj, { page: _.toInteger(req.body.skip), limit: _.toInteger(req.body.limit) }, function(err, result) {
        if (err) {
            log.info({ message: 'Course getFilter Error', label: 'ERROR', msg: err.stack });
            return res.status(500).send('Internal Server Error : ' + err.toString());
        }
        if (result.docs) {
            res.status(200).send(result);
        } else {
            return res.status(500).send("There was a problem finding the courses.");
        }
    });
}

function remove(req, res, next) {
    Courses.findById(ObjectId(req.body.id), function(err, course) {
        if (err) {
            log.info({ message: 'Course remove Error', label: 'ERROR', msg: err.stack });
            return res.status(500).send('Internal Server Error : ' + err.toString());
        }
        course.remove(function(err, obj) {
            res.status(200).send(obj);
        }); //Removes the document
    })
}

function saveCourse(req, res, next) {
    if (req.body._id) {
        Courses.findOne({ _id: req.body._id }, function(err, course) {
            if (course) {
                Object.keys(req.body).forEach(function(key) {
                    course[key] = req.body[key];
                });
                course.save(function(err, savedCourse) {
                    if (err) {
                        log.info({ message: 'Course saveCourse Error', label: 'ERROR', msg: err.stack });
                        return res.status(500).send('Internal Server Error : ' + err.toString());
                    }
                    res.status(200).send(savedCourse);
                });
            } else {
                log.info({ message: 'Course saveCourse 1 Error', label: 'ERROR', msg: err.stack });
                return res.status(500).send('Internal Server Error : ' + err.toString());
            }
        });
    } else {
        new Courses(req.body).save(function(err, doc) {
            if (err) {
                log.info({ message: 'Course saveCourse 2 Error', label: 'ERROR', msg: err.stack });
                return res.status(500).send('Internal Server Error : ' + err.toString());
            }
            res.status(200).send(doc);
        });
    }

}

function uploadCourseImage(req, res, next) {
    Courses.findOne({ _id: ObjectId(req.body.courseId) }, function(err, course) {
        if (err) {
            return res.status(500).send('Internal Server Error : ' + err.toString());
        }
        course.image = req.body.image;
        course.save(function(err, savedCourse) {
            if (err) {
                log.info({ message: 'Course uploadCourseImage Error', label: 'ERROR', msg: err.stack });
                return res.status(500).send('Internal Server Error : ' + err.toString());
            }
            return res.status(200).send(savedCourse);

        });
    });


    // let imagePath = '';
    // var reqBody = {};
    // var form = new formidable.IncomingForm();
    // form.parse(req);
    // form.on('field', function(name, value) {
    //     reqBody[name] = value;
    // });

    // form.on('fileBegin', function(name, file) {
    //     imagePath = path.join('../SkillAdda_images/');
    //     if (!fs.existsSync(imagePath)) {
    //         fs.mkdirSync(imagePath);
    //     }
    //     imagePath = path.join('../SkillAdda_images/course/');
    //     if (!fs.existsSync(imagePath)) {
    //         fs.mkdirSync(imagePath);
    //     }
    //     form.uploadDir = imagePath;
    //     var fileName = file.name;
    //     var ext = fileName.substring(fileName.lastIndexOf('.') + 1);
    //     var uniqueName = uniqid() + '.' + ext;
    //     file.path = imagePath + uniqueName;
    //     file.path = file.path.replace(/ /g, '_')
    //     imagePath = file.path;
    // });

    // form.on('file', function(name, file) {});

    // form.on('error', function(err) {
    //     if (err) return res.status(500).send("The max File Size exceeded, please reduce the size of file the default size is 200 mb.");
    // });

    // form.on('aborted', function() {
    //     return res.status(500).send("The request aborted, please try again.");
    // });
    // form.on('end', function() {
    //     Courses.findOne({ _id: ObjectId(reqBody.courseId) }, function(err, course) {
    //         if (course) {
    //             if (course.image) {
    //                 // fs.unlinkSync(course.image);
    //             }
    //             course.image = imagePath;
    //             course.save(function(err, savedCourse) {
    //                 if (err) {
    //                     log.info({ message: 'Course uploadCourseImage Error', label: 'ERROR', msg: err.stack });
    //                     return res.status(500).send('Internal Server Error : ' + err.toString());
    //                 } else {
    //                     res.status(200).send(savedCourse);
    //                 }
    //             });
    //         } else {
    //             log.info({ message: 'Course uploadCourseImage 1 Error', label: 'ERROR', msg: err.stack });
    //             return res.status(500).send('Internal Server Error : ' + err.toString());
    //         }
    //     });
    // });
}
async function docsUpload(req, res, next) {
    try {
        let docsup = await fileopr.uploadDocs(req);
        if (!docsup) return res.status(500).send('Unable to upload doc');
        if ((docsup || {})._id) {
            let courseDoc = await Courses.findById(docsup._id);
            if (((courseDoc || {}).docs || {}).length > 0) {
                let docArray = [];
                for (let doc of docsup.docs) {
                    if (courseDoc.docs.indexOf(doc) == -1) {
                        docArray.push(doc);
                    }
                }
                docArray = docArray.concat(courseDoc.docs);
                courseDoc.docs = docArray;
            } else {
                courseDoc.docs = docsup.docs;
            }
            await courseDoc.save();
        }
        res.status(200).send(docsup);
    } catch (err) {
        log.info({ message: 'Course docsUpload Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error : ' + err.toString());
    }
}

function encode_base64(filename, callback) {
    if (filename.indexOf("http") > -1) {
        request.get(filename, function(error, response, body) {
            if (!error && response.statusCode == 200) {
                data = new Buffer(body).toString('base64');
                callback({}, data);
            } else {
                callback({}, '');
            }
        });
    } else {
        fs.readFile(path.join(filename), function(error, data) {
            if (error) {
                callback({}, '');
            } else {
                var buf = Buffer.from(data);
                var base64 = buf.toString('base64');
                //console.log('Base64 of ddr.jpg :' + base64);
                callback({}, base64);
            }
        });
    }
}