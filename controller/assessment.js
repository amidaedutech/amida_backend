'use strict';
const mongoose = require('mongoose');
const ObjectId = mongoose.Types.ObjectId;
const Result = require('../models/result');
const Question = require('../models/questions');
const Assessment = require('../models/assessment');
const NotiModel = require('../models/notification');
const Order = require('../models/order');
const UserModel = require('../models/users');
const log = require('../utils/winston');
module.exports = {
    saveAnswer: saveAnswer,
    assessmentById: assessmentById,
    result: assessmentResult,
    assessmentByUserId: assessmentByUserId,
    fetchAll: fetchAll,
    getAssessementById: getAssessementById,
    getAssessementsByUser: getAssessementsByUser,
    saveAssessment: saveAssessmentDetails,
    saveQuestions: saveQuestions,
    removeAssessment: removeAssessment,
    removeQuestion: removeQuestion,
    getQuestionByAssessment: getQuestionByAssessment,
    getQuestionById: getQuestionById,
    addAssessmemntToStudents: addAssessmemntToStudents,
    completeAssessment: completeAssessment,
    list: list,
    publishAss: publishAss,
    fechList: fechList,
    assignQuiz: assignQuiz,
    assUnderChecking: assUnderChecking
};

function list(req, res) {
    var result = {};
    result.docs = [];
    var filterObj = {};
    if (req.body.filterQuery) {
        var re = new RegExp(req.body.filterQuery, 'i');
        var matchFields = [];
        matchFields.push({ 'name': { $regex: re } }, { 'rootCategory': { $regex: re } }, { 'category': { $regex: re } }, { 'subject': { $regex: re } });
        filterObj.$or = matchFields;
    }
    Assessment.paginate(filterObj, { page: parseInt(req.body.skip), limit: (req.body.limit) }, function(err, result) {
        if (err) {
            log.info({ message: 'Assessment list Error', label: 'ERROR', msg: err.stack });
            return res.status(500).send('Internal Server Error' + err.toString());
        } else {
            return res.status(200).send(result);
        }
    });
}

async function fechList(req, res) {
    try {
        let query = {};
        for (let key in req.body) {
            if (req.body[key])
                query[key] = req.body[key];
        }
        let assesmts = await Assessment.find(query);
        return res.status(200).send(assesmts);
    } catch (err) {
        log.info({ message: 'Assessment fechList Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error' + err.toString());
    }
}
async function saveAnswer(req, res, next) {
    try {
        var assessmentResultDoc = await Result.findById(req.body.resultId);
        if (req.body.duration) {
            assessmentResultDoc.duration = req.body.duration;
        }
        if (req.body.status) {
            assessmentResultDoc.status = req.body.status;
        } else {
            assessmentResultDoc.status = 'In Progress';
        }
        var checkAns = await checkAnswer(req.body); // if answer is correct it will come true else false
        console.log('checkAns', checkAns)
        if (checkAns.flag) return res.status(500).send(checkAns.message);
        var answerList = [];
        var isQuestionExist = false;
        console.log('assessmentResultDoc', assessmentResultDoc)
        for (let ans of assessmentResultDoc.answers) {
            if (req.body._id === ans.question.toString()) {
                isQuestionExist = true;
                if (req.body.qType) {
                    ans.ans = checkAns.isCorrect;
                } else {
                    ans.ans = req.body.ans;
                }

                if (req.body.comments) {
                    ans.comments = req.body.comments;
                }
                if (req.body.userAns) {
                    ans.userAns = req.body.userAns
                }
                answerList.push(ans);
            } else {
                answerList.push(ans);
            }
        }
        if (!isQuestionExist) {
            answerList.push({ ans: req.body.ans, question: req.body._id });
        }
        assessmentResultDoc.answers = answerList;
        var saveAssessments = await assessmentResultDoc.save();
        res.status(200).send(saveAssessments);

    } catch (err) {
        console.log('err', err);
        return res.status(500).send('Internal Server Error' + err.toString());
    }

}

async function assessmentById(req, res, next) {
    var result = await findAssessmentByuser(req.body);
    if (result.flag) return res.status(500).send(result.message);
    res.status(200).send(result.doc);
}

function findAssessmentByuser(data) {
    return new Promise(function(resolve, reject) {
        Result.findOne({ _id: data._id }).populate({ path: 'answers.question', model: Question }).exec(function(err, doc) {
            if (err) reject({ flag: true, message: 'Assessment not found' });
            resolve({ flag: false, doc: doc });
        });
    });
}

async function assessmentResult(req, res, next) {
    var assessment = await findAssessmentResult(req.body);
    if (assessment.flag) return res.status(500).send(assessment.message);
    res.status(200).send(assessment.doc);
}

function assessmentByUserId(req, res, next) {
    Result.find(req.body).populate({ path: 'answers.question', model: Question }).exec(function(err, docs) {
        if (err) {
            log.info({ message: 'Assessment assessmentByUserId Error', label: 'ERROR', msg: err.stack });
            return res.status(500).send('Internal Server Error' + err.toString());
        }
        res.status(200).send(docs);
    });
}

async function saveAssessment(data) {
    try {
        let doc = await data.save();
        return { flag: false, doc: doc };
    } catch (err) {
        console.log('err=-=-', err);
        return { flag: true, message: 'Unable to save assessment' };
    }
    // return new Promise(function(resolve, reject) {
    //     data.save(function(err, doc) {
    //         if (err) reject({ flag: true, message: 'Unable to save assessment' });
    //         resolve({ flag: false, doc: doc });
    //     });
    // });
}

function findAssessmentById(data) {
    return new Promise(function(resolve, reject) {
        Assessment.findById(data.assessmentId, function(err, doc) {
            if (err) reject({ flag: true, message: 'Unable to fetch assessment details, please try again later.' });
            resolve({ flag: false, doc: doc });
        });
    });
}

async function findAssessmentResult(data) {
    try {
        let doc = await Result.findOne({ _id: ObjectId(data._id) }).lean();
        return { flag: false, doc: doc };
    } catch (err) {
        return { flag: true, message: 'Assessment not found' };
    }


    // return new Promise(function(resolve, reject) {
    //     Result.findOne({ _id: ObjectId(data._id) }, function(err, doc) {
    //         if (err) reject({ flag: true, message: 'Assessment not found' });
    //         resolve({ flag: false, doc: doc });
    //     });
    // });
}

/*function checkAnswer(data) {
    return new Promise(function (resolve, reject) {
        var isCorrect = false;
        Question.findById(ObjectId(data._id), function (err, doc) {
            if (err || !doc) reject({ flag: true, message: 'Question Not Found' });
            doc.optoins.forEach(function (item) {
                if (data.ans === item.key) isCorrect = true;
            });
            resolve({ flag: false, isCorrect: isCorrect });
        });
    });
}*/

function getQuestions(assessmentId, userId) {
    return new Promise(function(resolve, reject) {
        Question.find({ assessmentId: assessmentId }, function(err, docs) {
            if (err) reject({ flag: true, message: 'Questions not found' });
            resolve({ flag: false, doc: docs });
        });
    });
}

async function getQuestionByAssessment(req, res, next) {
    try {
        let questions = await Question.find({ assessmentId: ObjectId(req.params.id) });
        if (questions && questions.length > 0) {
            return res.status(200).send(questions);
        } else {
            return res.status(200).send([]);
        }
    } catch (err) {
        log.info({ message: 'Assessment getQuestionByAssessment Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error' + err.toString());
    }
}

async function getQuestionById(req, res, next) {
    try {
        let question = await Question.findOne({ _id: ObjectId(req.params.id) });
        if (question) {
            return res.status(200).send(question);
        } else {
            return res.status(500).send('There are no Questions');
        }
    } catch (err) {
        log.info({ message: 'Assessment getQuestionById Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error' + err.toString());
    }
}

async function fetchAll(req, res, next) {
    try {
        let assessments = await Assessment.find({});
        return res.status(200).send(assessments);
    } catch (err) {
        log.info({ message: 'Assessment fetchAll Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error' + err.toString());
    }
}

function findAssessment(data) {
    return new Promise(function(resolve, reject) {
        Result.findOne({ assessmentId: data.assessmentId, userId: data.userId }, function(err, doc) {
            if (err) reject({ flag: true, message: 'Assessment not found' });
            resolve({ flag: false, doc: doc });
        });
    });
}

function checkAnswer(data) {
    return new Promise(function(resolve, reject) {
        var isCorrect = false;
        if (!data.qType) {
            resolve({ flag: false, isCorrect: false });
        }
        Question.findById(data._id, function(err, doc) {
            console.log("err", err, doc);
            if (err || !doc) reject({ flag: true, message: 'Question Not Found' });
            if (data.ans === doc.answer) isCorrect = true;
            // doc.options.forEach(function(item) {
            //     if (data.ans === item.answer) isCorrect = true;
            // });
            resolve({ flag: false, isCorrect: isCorrect });
        });
    });
}

async function getAssessementById(req, res, next) {
    try {
        let assessment = await Assessment.findOne({ _id: ObjectId(req.params.id) });
        return res.status(200).send(assessment);
    } catch (err) {
        log.info({ message: 'Assessment getAssessementById Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error' + err.toString());
    }
}

async function getAssessementsByUser(req, res, next) {
    try {
        let assessments = await Assessment.find({ createdBy: ObjectId(req.params.id) });
        return res.status(200).send(assessment);
    } catch (err) {
        log.info({ message: 'Assessment getAssessementsByUser Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error' + err.toString());
    }
}

async function saveAssessmentDetails(req, res, next) {
    try {
        if (req.body._id) {
            let assessment = await Assessment.findByIdAndUpdate(req.body._id, req.body, { new: true });
            if (!assessment) {
                return res.status(500).send('Assessment Not Found');
            }
            return res.status(200).send(assessment);
        } else {
            req.body.status = 'open';
            let assessment = new Assessment(req.body);
            let savedDoc = await assessment.save();
            return res.status(200).send(savedDoc);
        }
    } catch (err) {
        log.info({ message: 'Assessment saveAssessmentDetails Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error' + err.toString());
    }
}

async function saveQuestions(req, res, next) {
    try {
        if (req.body._id) {
            let question = await Question.findByIdAndUpdate(req.body._id, req.body, { new: true });
            if (!question) {
                return res.status(500).send('Questions Not Found');
            }
            return res.status(200).send(question);

        } else {
            let questions = await Question.find({ assessmentId: ObjectId(req.body.assessmentId) });
            req.body.number = (questions.length + 1);
            let question = new Question(req.body);
            let savedDoc = await question.save();
            return res.status(200).send(savedDoc);
        }
    } catch (err) {
        log.info({ message: 'Assessment saveQuestions Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error' + err.toString());
    }
}

async function removeAssessment(req, res, next) {
    try {
        let assessment = await Assessment.findOneAndRemove({ _id: ObjectId(req.params.id) });
        return res.status(200).send({ status: 200, message: 'Assessment successfully removed' });
    } catch (err) {
        log.info({ message: 'Assessment removeAssessment Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error' + err.toString());
    }
}

async function removeQuestion(req, res, next) {
    try {
        let question = await Question.findOneAndRemove({ _id: ObjectId(req.params.id) });
        if (!question) {
            return res.status(500).send('Questions Not Found');
        }
        return res.status(200).send({ status: 200, message: 'Question successfully removed' });
    } catch (err) {
        log.info({ message: 'Assessment removeQuestion Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error' + err.toString());
    }
}

async function addAssessmemntToStudents(req, res, next) {
    try {
        //console.log("body", req.body);
        let students = req.body.students;
        let questions = await Question.find({ assessmentId: ObjectId(req.body.assessmentId) });
        for (var i in students) {
            let result = {};
            result.name = req.body.name;
            result.assessmentId = req.body.assessmentId;
            result.category = req.body.category;
            result.subject = req.body.subject;
            result.userId = students[i]._id;
            result.duration = req.body.duration;
            result.answers = [];
            for (var j in questions) {
                result.answers.push({
                    question: questions[j]._id
                });
            }
            let notificModel = { type: 'Assessment' };
            if (req.userId) {
                notificModel.createdBy = req.userId;
            }
            notificModel.to = students[i]._id;
            notificModel.msg = 'Please check your new assessment.';
            await NotiModel.create(notificModel);
            var student = await Result.findOne({ assessmentId: ObjectId(req.body.assessmentId), userId: students[i]._id });
            if (!student) {
                let resultDoc = await new Result(result).save();
                if (resultDoc) {
                    return res.status(200).send({ status: 200, message: 'Assessment added successfully' });
                } else {
                    return res.status(500).send('Unable to add assessment');
                }
            } else {
                return res.status(200).send({ status: 200, message: 'Assessment added successfully' });
            }

        }
    } catch (err) {
        log.info({ message: 'Assessment removeQuestion Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error' + err.toString());
    }
}

async function completeAssessment(req, res, next) {
    try {
        console.log('req.body', req.body);
        let total = 0;
        let score = 0;
        var grade = '';
        let status = '';
        let query = { grade: grade, score: score, status: status }
        if (req.body.duration) {
            query.duration = req.body.duration;
        }

        var assessment = await Result.findById(req.body._id).populate({ path: 'answers.question', model: 'questions' }).lean();
        if (!assessment) return res.status(500).send('Assessment not found');
        if ((req.body || {}).status === 'Completed') {
            status = "Completed";
        } else {
            var getAssmnt = await Assessment.findById(assessment.assessmentId);
            if (getAssmnt.type = "Essay") {
                status = 'UnderChecking';
            } else {
                status = "Completed";
            }
        }
        if ((req.body || {}).type === "Essay") {
            if (req.body.score) {
                score = req.body.score;
            }
            if (req.body.grade) {
                grade = req.body.grade;
            }
        } else {
            total = ((assessment || {}).answers || {}).length;
            for (let item of assessment.answers) {
                if (item.ans) {
                    if (item.userAns === ((item || {}).question || {}).answer)
                        score++;
                }
            }
            let percentage = (score / total) * 100;
            if (percentage >= 75) {
                grade = 'A';
            } else if (percentage < 75 && percentage >= 50) {
                grade = 'B';
            } else if (percentage < 50 && percentage >= 35) {
                grade = 'C';
            } else {
                grade = 'D';
            }
        }
        query.score = score;
        query.grade = grade;
        query.status = status;
        let assessmentResult = await Result.findByIdAndUpdate(req.body._id, query, { new: true });
        if (!assessmentResult) return res.status(500).send('There is a problem with updating assessment.')
        res.status(200).send(assessmentResult);
    } catch (err) {
        log.info({ message: 'Assessment completeAssessment Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error' + err.toString());
    }
}

async function publishAss(req, res) {
    try {
        let findAss = await Assessment.findById(req.body._id);
        findAss.status = 'published';
        await findAss.save();
        return res.status(200).send({ publish: 'success' })
    } catch (err) {
        log.info({ message: 'Assessment publishAss Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error' + err.toString());
    }
}

async function assignQuiz(req, res) {
    try {
        let findAss = await Assessment.findById(req.body._id);
        let query = {};
        if (req.body.rootCategory) {
            query.rootCategoryName = req.body.rootCategory;
        }
        if (req.body.category) {
            query.categoryName = req.body.category;
        }
        if (req.body.subject) {
            query.subjectName = req.body.subject;
        }
        let orderCourses = await Order.find({}).populate({
            path: 'course',
            match: query
        }).populate('payment');
        for (let order of orderCourses) {
            if (((order || {}).payment || {}).status === 'success' && ((order || {}).course || {}).length && ((order || {}).course || {}).length > 0) {
                let questions = await Question.find({ assessmentId: ObjectId(findAss._id) });
                let result = {};
                result.name = findAss.name;
                result.assessmentId = findAss._id;
                result.category = findAss.category || '';
                result.subject = findAss.subject || '';
                result.userId = order.purchasedBy;
                result.duration = findAss.duration;
                result.answers = [];
                for (var j in questions) {
                    result.answers.push({
                        question: questions[j]._id
                    });
                }
                var assResult = await Result.findOne({ assessmentId: findAss._id, userId: order.purchasedBy });
                if (!assResult) {
                    await new Result(result).save();
                }
            }
        }
        return res.status(200).send({ publish: 'success' })
    } catch (err) {
        log.info({ message: 'Assessment assignQuiz Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error' + err.toString());
    }
}

async function assUnderChecking(req, res) {
    try {
        let resultUnderCheckin = await Result.find({ status: 'UnderChecking' }).populate({ path: 'assessmentId', model: Assessment }).populate({ path: 'userId', model: UserModel, select: { '_id': 1, 'email': 1, 'firstName': 1, 'username': 1 }, }).lean();
        let resBody = [];
        let assessNames = [];
        for (let item of resultUnderCheckin) {
            if (assessNames.indexOf(item.assessmentId.name) < 0) {
                assessNames.push(item.assessmentId.name);
            }
        }
        for (let assName of assessNames) {
            let stdArray = [];
            let assDetails = {};
            for (let item of resultUnderCheckin) {
                if (assName === item.assessmentId.name) {
                    assDetails = item.assessmentId;
                    stdArray.push(item);
                }
            }
            resBody.push({ assName: assDetails, userList: stdArray });
        }
        return res.status(200).send(resBody);
    } catch (err) {
        log.info({ message: 'Assessment assUnderChecking Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error' + err.toString());
    }
}