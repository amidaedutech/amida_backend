const mongoose = require('mongoose');
const async = require('async');
const ObjectId = mongoose.Types.ObjectId;
const Cart = require('../models/cart');
const utils = require('../utils/utils');
const log = require('../utils/winston');
module.exports = {
    fetch: fetch,
    save: save,
    fetchById: fetchById,
    fetchByOne: fetchByOne,
    remove: remove,
    fetchCartCount: fetchCartCount,
    deleteAll: deleteAll
}

function fetch(req, res, next) {
    Cart.find({}, function(err, carts) {
        if (err) {
            log.info({ message: 'Cart fetch Error', label: 'ERROR', msg: err.stack });
            return res.status(500).send('Internal Server Error : ' + err.toString())
        } else if (!carts) return res.status(401).send("There was a problem finding the carts.");
        res.status(200).send(carts);
    });
}
async function fetchCartCount(req, res) {
    try {
        let cartCount = await Cart.findOne(req.body);
        let count = 0;
        if (((cartCount || {}).courses || {}).length) {
            count = cartCount.courses.length;
        }
        res.status(200).send({ cartCount: count });
    } catch (err) {
        log.info({ message: 'Cart fetchCartCount Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error : ' + err.toString())
    }
}

async function save(req, res, next) {
    try {
        let cart = await Cart.findOne({ userId: req.body.userId });
        let newCart = [];
        let cartArray = ((cart || {}).courses || {}).length === 0 ? [] : (cart || {}).courses;
        if ((cart || {}).courses) {
            for (let courseId of req.body.courses) {
                let index = cartArray.indexOf(ObjectId(courseId));
                if (index === -1) {
                    newCart.push(courseId);
                }
            }
            if (newCart.length === 0)
                return res.status(200).send({ added: newCart });
            cart.courses = cart.courses.concat(newCart);
            let savedCart = await cart.save();
            res.status(200).send(savedCart);
        } else {
            let cartModel = new Cart({ userId: req.body.userId, courses: req.body.courses });
            let doc = await cartModel.save();
            res.status(200).send(doc);
        }
    } catch (err) {
        log.info({ message: 'Cart save Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error : ' + err.toString())
    }
}

function fetchById(req, res, next) {
    Cart.findById(req.params.id, function(err, carts) {
        if (err) {
            log.info({ message: 'Cart fetchById Error', label: 'ERROR', msg: err.stack });
            return res.status(500).send('Internal Server Error : ' + err.toString())
        } else if (!carts) return res.status(404).send("There was a problem finding the carts.");
        res.status(200).send(carts);
    });
}

function fetchByOne(req, res, next) {
    Cart.findOne(req.body).populate({ path: 'courses', model: 'courses' }).exec(function(err, carts) {
        if (err) {
            log.info({ message: 'Cart fetchByOne Error', label: 'ERROR', msg: err.stack });
            return res.status(500).send('Internal Server Error : ' + err.toString())
        } else if (!carts) return res.status(404).send("You cart is empty, please add some product and try.");
        res.status(200).send(carts);
    });
}

async function remove(req, res, next) {
    try {
        let cart = await Cart.findOne({ userId: req.body.userId });
        for (let courseId of req.body.courses) {
            let index = cart.courses.indexOf(ObjectId(courseId));
            if (index > -1) {
                cart.courses.splice(index, 1);
            }
        }
        await cart.save();
        res.send({ status: 200, statusText: 'Sucessfully Deleted', message: 'Product removed from cart list.' });
    } catch (err) {
        log.info({ message: 'Cart remove Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error : ' + err.toString())
    }
}

function deleteAll(req, res, next) {
    Cart.remove({ userId: req.params.id }, function(err, doc) {
        if (err) {
            log.info({ message: 'Cart deleteAll Error', label: 'ERROR', msg: err.stack });
            return res.status(500).send('Internal Server Error : ' + err.toString())
        }
        res.send({ status: 200, statusText: 'Sucessfully Cart Cleared', message: 'Product removed from cart list.' });
    });
}