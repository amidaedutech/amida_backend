const mongoose = require('mongoose');
const ObjectId = mongoose.Types.ObjectId;
const RootCategory = require('../models/root-category');
const Category = require('../models/category');
const Subject = require('../models/subject');
const Course = require('../models/course');
const Content = require('../models/content');
var rimraf = require("rimraf");
const log = require('../utils/winston');
const fs = require('fs');
const path = require('path');
module.exports = {
    fetch: fetch,
    fetchById: fetchById,
    deleteById: deleteById
}

function fetch(req, res, next) {
    RootCategory.find({}, function(err, categories) {
        if (err) {
            log.info({ message: 'Root Category fetch Error', label: 'ERROR', msg: err.stack });
            return res.status(500).send('Internal Server Error: ' + err.toString());
        } else if (!categories) return res.status(404).send("There was a problem finding the root categories.");
        else res.status(200).send(categories);
    });
}

function fetchById(req, res, next) {
    RootCategory.findById(req.params.id, function(err, category) {
        if (err) {
            log.info({ message: 'Root Category fetchById Error', label: 'ERROR', msg: err.stack });
            return res.status(500).send('Internal Server Error: ' + err.toString());
        } else if (!categories) return res.status(404).send("There was a problem finding the root category.");
        else res.status(200).send(category);
    });
}

async function deleteById(req, res) {
    try {
        let cdoc = await Course.find({ rootCategory: ObjectId(req.params.id) });
        for (let item of cdoc) {
            if (item.image && fs.existsSync(item.image)) {
                fs.unlinkSync(item.image);
            }
            if (item.htmlpath && fs.existsSync(item.htmlpath)) {
                rimraf.sync(item.htmlpath);
                // fs.unlinkSync(item.htmlpath);
            }
            let vpath = path.join('../' + '/SkillAdda_videos/' + item.name);
            if (fs.existsSync(vpath)) {
                var deleteFolderRecursive = function(path) {
                    if (fs.existsSync(path)) {
                        fs.readdirSync(path).forEach(function(file, index) {
                            var curPath = path + "/" + file;
                            if (fs.lstatSync(curPath).isDirectory()) { // recurse
                                deleteFolderRecursive(curPath);
                            } else { // delete file
                                fs.unlinkSync(curPath);
                            }
                        });
                        fs.rmdirSync(path);
                    }
                };
                // rimraf.sync(vpath); 
                deleteFolderRecursive(vpath);
            }
            await Content.deleteMany({ courseId: item._id });
        }

        await Course.deleteMany({ rootCategory: ObjectId(req.params.id) });
        await Subject.deleteMany({ rootCategoryId: ObjectId(req.params.id) });
        let ctdoc = await Category.find({ rootCategoryId: ObjectId(req.params.id) });
        for (let item of ctdoc) {
            if (item.image && fs.existsSync(item.image)) {
                fs.unlinkSync(item.image);
            }
        }
        await Category.deleteMany({ rootCategoryId: ObjectId(req.params.id) });
        await RootCategory.deleteOne({ _id: ObjectId(req.params.id) });
        return res.status(200).send({ status: 200, message: 'Root Category successfully removed' });
    } catch (err) {
        log.info({ message: 'Root Category deleteById Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error: ' + err.toString());
    }
}