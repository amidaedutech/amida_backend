const mongoose = require('mongoose');
const ObjectId = mongoose.Types.ObjectId;
const Subject = require('../models/subject');
const RootCategory = require('../models/root-category');
const Category = require('../models/category');
const Course = require('../models/course');
const Content = require('../models/content');
var rimraf = require("rimraf");
const fs = require('fs');
const path = require('path');
const log = require('../utils/winston');
module.exports = {
    fetch: fetch,
    fetchById: fetchById,
    fetchByOne: fetchByOne,
    fetchByCategory: fetchByCategory,
    deleteById: deleteById
}

function fetch(req, res, next) {
    Subject.find({})
        .populate({ path: 'rootCategoryId', model: RootCategory })
        .populate({ path: 'categoryId', model: Category })
        .exec(function(err, subjects) {
            if (err) {
                log.info({ message: 'Subject fetch Error', label: 'ERROR', msg: err.stack });
                return res.status(500).send('There was a problem finding the subject: ' + err.toString());
            } else if (!subjects) return res.status(401).send("There was a problem finding the subjects.");
            res.status(200).send(subjects);
        });
}

function fetchById(req, res, next) {
    Subject.findById(req.params.id, function(err, subject) {
        if (err) {
            log.info({ message: 'Subject fetchById Error', label: 'ERROR', msg: err.stack });
            return res.status(500).send('There was a problem finding the subject: ' + err.toString());
        } else if (!subject) return res.status(401).send("There was a problem finding the subject.");
        res.status(200).send(subject);
    });
}

function fetchByOne(req, res, next) {
    Subject.find(req.body, function(err, subjects) {
        if (err) {
            log.info({ message: 'Subject fetchByOne Error', label: 'ERROR', msg: err.stack });
            return res.status(500).send('There was a problem finding the subject: ' + err.toString());
        } else if (!subjects) return res.status(401).send("There was a problem finding the subjects.");
        res.status(200).send(subjects);
    });
}

function fetchByCategory(req, res, next) {
    Subject.find({ categoryId: ObjectId(req.params.id) }, function(err, subjects) {
        if (err) {
            log.info({ message: 'Subject fetchByCategory Error', label: 'ERROR', msg: err.stack });
            return res.status(500).send('Internal Server Error: ' + err.toString());
        } else if (!subjects) return res.status(401).send("There was a problem finding the subjects.");
        res.status(200).send(subjects);
    });
}

async function deleteById(req, res, next) {
    try {
        let cdoc = await Course.find({ subject: ObjectId(req.params.id) });
        for (let item of cdoc) {
            if (item.image && fs.existsSync(item.image)) {
                fs.unlinkSync(item.image);
            }
            if (item.htmlpath && fs.existsSync(item.htmlpath)) {
                rimraf.sync(item.htmlpath);
            }
            let vpath = path.join('../' + '/SkillAdda_videos/' + item.name);
            if (fs.existsSync(vpath)) {
                var deleteFolderRecursive = function(path) {
                    if (fs.existsSync(path)) {
                        fs.readdirSync(path).forEach(function(file, index) {
                            var curPath = path + "/" + file;
                            if (fs.lstatSync(curPath).isDirectory()) { // recurse
                                deleteFolderRecursive(curPath);
                            } else { // delete file
                                fs.unlinkSync(curPath);
                            }
                        });
                        fs.rmdirSync(path);
                    }
                };
                deleteFolderRecursive(vpath);
            }
            await Content.deleteMany({ courseId: item._id });
        }

        await Course.deleteMany({ subject: ObjectId(req.params.id) });
        await Subject.deleteOne({ _id: ObjectId(req.params.id) });
        return res.status(200).send({ status: 200, message: 'Subject successfully removed' });
    } catch (err) {
        log.info({ message: 'Subject deleteById Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error: ' + err.toString());
    }
}