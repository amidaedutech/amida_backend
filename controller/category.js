const async = require('async');
const mongoose = require('mongoose');
const ObjectId = mongoose.Types.ObjectId;
const formidable = require('formidable');
const fs = require('fs');
const path = require('path');
const uniqid = require('uniqid');
const Category = require('../models/category');
const RootCategory = require('../models/root-category');
const Subject = require('../models/subject');
const Course = require('../models/course');
const Utils = require('../utils/utils');
const Search = require('../models/global-search');
const fileopr = require('../utils/fileoperation');
const Content = require('../models/content');
var rimraf = require("rimraf");
const log = require('../utils/winston');
module.exports = {
    fetch: fetch,
    fetchById: fetchById,
    fetchByOne: fetchByOne,
    fetchByRootCategory: fetchByRootCategory,
    rootCategoriesWithCategories: rootCategoriesWithCategories,
    SaveDetails: SaveDetails,
    deleteById: deleteById,
    uploadCategoryImage: uploadCategoryImage,
    updateById: updateById

}

async function fetch(req, res, next) {
    try {
        let categories = await Category.find({}).sort({ created: -1 });;
        res.status(200).send(categories);
    } catch (err) {
        log.info({ message: 'Category fetch Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error : ' + err.toString())
    }
}

function fetchById(req, res, next) {
    Category.findById(req.params.id, function(err, category) {
        if (err) {
            log.info({ message: 'Category fetchById Error', label: 'ERROR', msg: err.stack });
            return res.status(500).send('Internal Server Error : ' + err.toString())
        } else if (!category) { return res.status(401).send("There was a problem finding the category.") } else { res.status(200).send(category); };

    });
}

function fetchByOne(req, res, next) {
    Category.find(req.body, function(err, categories) {
        if (err) {
            log.info({ message: 'Category fetchByOne Error', label: 'ERROR', msg: err.stack });
            return res.status(500).send('Internal Server Error : ' + err.toString())
        } else if (!categories) return res.status(401).send("There was a problem finding the categories.");
        res.status(200).send(categories);
    });
}
async function updateById(req, res) {
    console.log('body', req.body);
    try {
        let updateModel = await Category.updateOne({ _id: req.body._id }, req.body);
        return res.status(200).send(updateModel);
    } catch (err) {
        log.info({ message: 'Category updateOne Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error : ' + err.toString())
    }
}

function fetchByRootCategory(req, res, next) {
    Category.find({ rootCategoryId: ObjectId(req.params.id) }, function(err, categories) {
        if (err) {
            log.info({ message: 'Category fetchByRootCategory Error', label: 'ERROR', msg: err.stack });
            return res.status(500).send('Internal Server Error : ' + err.toString())
        } else if (!categories) return res.status(401).send("There was a problem finding the categories.");
        res.status(200).send(categories);
    });
}

function rootCategoriesWithCategories(req, res, next) {
    RootCategory.find({}, function(err, rootCategories) {
        if (err) {
            log.info({ message: 'Category rootCategoriesWithCategories find Error', label: 'ERROR', msg: err.stack });
            return res.status(500).send('Internal Server Error : ' + err.toString());
        } else if (!rootCategories) {
            return res.status(404).send("There was a problem finding the categories.");
        } else {
            let result = [];
            async.each(rootCategories, function(rootCategory, callback) {
                if (rootCategory) {
                    let obj = { name: rootCategory.name, code: rootCategory.code, id: rootCategory._id };
                    Category.find({ rootCategoryId: rootCategory._id }, function(err, categories) {
                        // console.log(categories);
                        if (err) {
                            callback(err);
                        } else if (!categories) {
                            callback({ status: 404, message: "There was a problem finding the categories." })
                        } else {
                            var categoryArray = [];
                            async.each(categories, function(category, callback) {
                                Subject.find({ categoryId: category._id }, { name: 1 }, function(err, subDocs) {
                                    if (err) {
                                        callback(err);
                                    } else if (!subDocs) {
                                        callback({ status: 404, message: "There was a problem finding the subjects." })
                                    } else {
                                        let catObj = { _id: category._id, code: category.code, name: category.name, subjects: subDocs };
                                        categoryArray.push(catObj);
                                        callback();
                                    }
                                })
                            }, function(err) {
                                if (err) {
                                    callback({ status: 500, message: "There was a problem finding the subjects.", statusText: "Internal server problem" })
                                } else {
                                    obj.categories = categoryArray;
                                    result.push(obj);
                                    callback();
                                }
                            });
                        }
                    });
                } else {
                    callback();
                }
            }, function(err) {
                //console.log(result);
                if (err) {
                    log.info({ message: 'Category rootCategoriesWithCategories Error', label: 'ERROR', msg: err.stack });
                    return res.status(500).send('Internal Server Error : ' + err.toString());
                } else {
                    res.status(200).send(result);
                }
            });
        }
    });
}

async function SaveDetails(req, res, next) {
    try {
        let searchText = [];
        let resDoc = {};
        if (req.body.type === 'root-category') {
            var obj = {};
            obj.name = req.body.rootCategory;
            searchText.push({ text: obj.name });
            obj.code = obj.name.toLowerCase();
            obj.code = obj.code.replace(' ', '-');
            var rootCategory = new RootCategory(obj);
            resDoc = await rootCategory.save();
        } else if (req.body.type === 'category') {
            let rootDoc = await RootCategory.findOne({ _id: req.body.rootCategoryValue });
            var obj = {};
            obj.name = req.body.category;
            searchText.push({ text: req.body.category });
            searchText.push({ text: rootDoc.name + ', ' + req.body.category });
            obj.rootCategoryId = rootDoc._id;
            obj.code = obj.name.toLowerCase();
            obj.code = obj.code.replace(' ', '-');
            var category = new Category(obj);
            resDoc = await category.save();
        } else if (req.body.type === 'subject') {
            let catDoc = await Category.findOne({ name: req.body.categoryValue });
            let rootDoc = await RootCategory.findOne({ _id: req.body.rootCategoryValue });
            var obj = {};
            obj.name = req.body.subject;
            searchText.push({ text: req.body.subject });
            searchText.push({ text: catDoc.name + ', ' + req.body.subject });
            searchText.push({ text: rootDoc.name + ', ' + catDoc.name + ', ' + req.body.subject });
            obj.rootCategoryId = rootDoc._id;
            obj.categoryId = catDoc._id;
            obj.code = obj.name.toLowerCase();
            obj.code = obj.code.replace(' ', '-');
            console.log("obj", obj);
            var subject = new Subject(obj);
            resDoc = await subject.save();
        }

        if (resDoc) {
            Search.create(searchText, function(err, doc) {

            });
        }
        res.status(200).send(resDoc);
    } catch (err) {
        log.info({ message: 'Category SaveDetails Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error : ' + err.toString());
    }
}

async function deleteById(req, res, next) {
    try {
        let cdoc = await Course.find({ category: ObjectId(req.params.id) });
        for (let item of cdoc) {
            if (item.image && fs.existsSync(item.image)) {
                fs.unlinkSync(item.image);
            }
            if (item.htmlpath && fs.existsSync(item.htmlpath)) {
                rimraf.sync(item.htmlpath);
                // fs.unlinkSync(item.htmlpath);
            }
            let vpath = path.join('../' + '/SkillAdda_videos/' + item.name);
            if (fs.existsSync(vpath)) {
                var deleteFolderRecursive = function(path) {
                    if (fs.existsSync(path)) {
                        fs.readdirSync(path).forEach(function(file, index) {
                            var curPath = path + "/" + file;
                            if (fs.lstatSync(curPath).isDirectory()) { // recurse
                                deleteFolderRecursive(curPath);
                            } else { // delete file
                                fs.unlinkSync(curPath);
                            }
                        });
                        fs.rmdirSync(path);
                    }
                };
                // rimraf.sync(vpath); 
                deleteFolderRecursive(vpath);
            }
            await Content.deleteMany({ courseId: item._id });
        }

        await Course.deleteMany({ category: ObjectId(req.params.id) });
        await Subject.deleteMany({ categoryId: ObjectId(req.params.id) });
        let ctdoc = await Category.findById(ObjectId(req.params.id));
        if (ctdoc.image && fs.existsSync(ctdoc.image)) {
            fs.unlinkSync(ctdoc.image);
        }
        await Category.deleteOne({ _id: ObjectId(req.params.id) });
        return res.status(200).send({ status: 200, message: 'Category successfully removed' });
    } catch (err) {
        log.info({ message: 'Category deleteById Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error : ' + err.toString());
    }
}

function uploadCategoryImage(req, res, next) {
    Category.findOne({ _id: ObjectId(req.body.categoryId) }, function(err, category) {
        if (category) {
            category.image = req.body.image;
            category.save(function(err, savedCategory) {
                if (err) {
                    log.info({ message: 'Category uploadCategoryImage c Error', label: 'ERROR', msg: err.stack });
                    return res.status(500).send('Internal Server Error : ' + err.toString());
                } else {
                    res.status(200).send(savedCategory);
                }
            });
        } else {
            log.info({ message: 'Category uploadCategoryImage Error', label: 'ERROR', msg: err.stack });
            return res.status(500).send('Internal Server Error : ' + err.toString());
        }
    });

    // var filePath = '';
    // var reqBody = {};
    // var form = new formidable.IncomingForm();
    // form.parse(req);
    // form.on('field', function(name, value) {
    //     reqBody[name] = value;
    //     //console.log("name value", name, value);
    // });

    // form.on('fileBegin', function(name, file) {
    //     filePath = path.join('../SkillAdda_images/');
    //     if (!fs.existsSync(filePath)) {
    //         fs.mkdirSync(filePath);
    //     }
    //     filePath = path.join('../SkillAdda_images/category/');
    //     if (!fs.existsSync(filePath)) {
    //         fs.mkdirSync(filePath);
    //     }
    //     var fileName = file.name;
    //     var ext = fileName.substring(fileName.lastIndexOf('.') + 1);
    //     var uniqueName = uniqid() + '.' + ext;
    //     file.path = filePath + uniqueName;
    //     file.path = file.path.replace(/ /g, '_')
    //     filePath = file.path;
    // });

    // form.on('file', function(name, file) {});

    // form.on('error', function(err) {
    //     if (err) return res.status(500).send("The max File Size exceeded, please reduce the size of file the default size is 200 mb.");
    // });

    // form.on('aborted', function() {
    //     return res.status(500).send("The request aborted, please try again.");
    // });
    // form.on('end', function() {
    //     Category.findOne({ _id: ObjectId(reqBody.categoryId) }, function(err, category) {
    //         if (category) {
    //             if (category.image && fs.existsSync(category.image)) {
    //                 fs.unlinkSync(category.image);
    //             }
    //             category.image = filePath;
    //             category.save(function(err, savedCategory) {
    //                 if (err) {
    //                     log.info({ message: 'Category uploadCategoryImage c Error', label: 'ERROR', msg: err.stack });
    //                     return res.status(500).send('Internal Server Error : ' + err.toString());
    //                 } else {
    //                     res.status(200).send(savedCategory);
    //                 }
    //             });
    //         } else {
    //             log.info({ message: 'Category uploadCategoryImage Error', label: 'ERROR', msg: err.stack });
    //             return res.status(500).send('Internal Server Error : ' + err.toString());
    //         }
    //     });
    // });
}