const mongoose = require('mongoose');
const formidable = require('formidable');
const path = require('path');
const fs = require('fs');
const uniqid = require('uniqid');
const ObjectId = mongoose.Types.ObjectId;
const _ = require('lodash');
const Content = require('../models/content');
const log = require('../utils/winston');
module.exports = {
    fetch: fetch,
    save: save,
    upload: upload,
    fetchById: fetchById,
    getDemoUrl: getDemoUrl,
    fetchByOne: fetchByOne,
    remove: remove,
    deleteAll: deleteAll,
    deleteV: deleteV,
    deleteContent: deleteContent,
    saveContent: saveContent
}

function saveContent(req, res) {
    Content.find({ courseId: ObjectId(req.body.courseId) }, function(err, contents) {
        var contentData = {};
        var contentArray = _.filter(contents, function(o) { return o._id.toString() === req.body.contentId.toString() });
        if (contentArray.length > 0) {
            contentData = contentArray[0];
        }
        if (err) {
            if (err) return res.status(500).send("There was a problem while save to Content.");
        } else if (!(contentData || {})._id) {
            //first content and first video
            //new video also
            var cont = new Content();
            cont.title = req.body.title;
            cont.seq = req.body.seq;
            cont.videoUrl = [];
            if (req.body.path) {
                cont.videoUrl.push({
                    title: req.body.contentTitle,
                    vseq: req.body.vseq,
                    path: req.body.path
                        //vFlag: req.body.vFlag
                        // duration: req.body.duration
                });
            }
            cont.courseId = ObjectId(req.body.courseId);
            cont.save(function(err, doc) {
                if (err) {
                    log.info({ message: 'Content save Error', label: 'ERROR', msg: err.stack });
                    return res.status(500).send('Internal Server Error : ' + err.toString());
                }
                res.send(doc);
            });
        } else {
            //from second video
            if (!req.body._id) {
                //new chapter for existing content 
                contentData.title = req.body.title;
                if (req.body.seq) {
                    contentData.seq = req.body.seq;
                }
                contentData.videoUrl.push({
                    title: req.body.contentTitle,
                    vseq: req.body.vseq,
                    vFlag: req.body.vFlag,
                    path: req.body.path
                });
            } else if (req.body._id) {
                contentData.title = req.body.title;
                if (req.body.seq) {
                    contentData.seq = req.body.seq;
                }
                //existing content chapter edit
                var videoData = contentData.videoUrl;
                var index = _.findIndex(contentData.videoUrl, function(o) { return o._id.toString() === req.body._id.toString() });
                // let duration = videoData[index].duration;
                videoData[index].title = req.body.contentTitle;
                videoData[index].vseq = req.body.vseq;
                videoData[index].vFlag = req.body.vFlag
                    // videoData[index].duration = req.body.duration;
                if (req.body.path) {
                    videoData[index].path = req.body.path;
                }
                contentData.videoUrl = videoData;
                // contentData.duration = Number(contentData.duration) + Number(req.body.duration) - duration;
            }
            contentData.save(function(err, doc) {
                if (err) {
                    log.info({ message: 'Content save 1 Error', label: 'ERROR', msg: err.stack });
                    return res.status(500).send('Internal Server Error : ' + err.toString());
                }
                res.send(doc);
            });
        }
    });

}

function fetch(req, res, next) {
    Content.find({}, function(err, Contents) {
        if (err) {
            log.info({ message: 'Content fetch Error', label: 'ERROR', msg: err.stack });
            return res.status(500).send('Internal Server Error : ' + err.toString());
        } else if (!Contents) return res.status(401).send("There was a problem finding the Contents.");
        res.status(200).send(Contents);
    });
}

function save(req, res, next) {

}

function upload(req, res, next) {
    var filePath = '';
    var reqBody = {};
    var form = new formidable.IncomingForm();
    form.parse(req);
    form.on('field', function(name, value) {
        reqBody[name] = value;
        //console.log("name value", name, value);
    });

    form.on('fileBegin', function(name, file) {
        var fileName = file.name;
        var ext = fileName.substring(fileName.lastIndexOf('.') + 1);
        var uniqueName = uniqid() + '.' + ext;
        file.path = path.join('../' + '/SkillAdda_videos/') + uniqueName;
        filePath = uniqueName;
    });

    form.on('file', function(name, file) {});

    form.on('error', function(err) {
        if (err) return res.status(500).send("The max File Size exceeded, please reduce the size of file the default size is 200 mb.");
    });

    form.on('aborted', function() {
        if (err) return res.status(500).send("The request aborted, please try again.");
    });
    form.on('end', function() {
        var filePathIs = filePath;
        Content.find({ courseId: ObjectId(reqBody.courseId) }, function(err, contents) {
            //contentId is main content id
            //_id is part id
            var contentData = {};
            var contentArray = _.filter(contents, function(o) { return o._id.toString() === reqBody.contentId.toString() });
            if (contentArray.length > 0) {
                contentData = contentArray[0];
            }
            if (err) {
                log.info({ message: 'Content upload Error', label: 'ERROR', msg: err.stack });
                return res.status(500).send('Internal Server Error : ' + err.toString());
            } else if (!contentData._id) {
                //first content and first video
                //new video also
                var cont = new Content();
                cont.title = reqBody.title;
                cont.duration = reqBody.duration;
                cont.order = 1;
                if (contents.length > 0) {
                    cont.order = contents.length + 1;
                }
                cont.videoUrl = [];
                if (filePathIs) {
                    cont.videoUrl.push({
                        title: reqBody.videotitle,
                        path: filePathIs,
                        duration: reqBody.duration
                    });
                }
                cont.courseId = ObjectId(reqBody.courseId)
                cont.save(function(err, doc) {
                    if (err) {
                        log.info({ message: 'Content save upload Error', label: 'ERROR', msg: err.stack });
                        return res.status(500).send('Internal Server Error : ' + err.toString());
                    }
                    res.status(200).send(doc);
                });
            } else {
                //from second video
                if (!reqBody._id) {
                    //new chapter for existing content
                    //contentData.title = reqBody.title;
                    contentData.duration = Number(contentData.duration) + Number(reqBody.duration);
                    contentData.videoUrl.push({
                        title: reqBody.videotitle,
                        path: filePathIs,
                        duration: reqBody.duration
                    });
                } else if (reqBody._id) {
                    //existing content chapter edit
                    var videoData = contentData.videoUrl;
                    var index = _.findIndex(contentData.videoUrl, function(o) { return o._id.toString() === reqBody._id.toString() });
                    let duration = videoData[index].duration;
                    videoData[index].title = reqBody.videotitle;
                    videoData[index].duration = reqBody.duration;
                    if (filePathIs) {
                        videoData[index].path = filePathIs;
                    }
                    contentData.videoUrl = videoData;
                    contentData.duration = Number(contentData.duration) + Number(reqBody.duration) - duration;
                }
                contentData.save(function(err, doc) {
                    if (err) {
                        log.info({ message: 'Content save Error', label: 'ERROR', msg: err.stack });
                        return res.status(500).send('Internal Server Error : ' + err.toString());
                    }
                    res.status(200).send(doc);
                });
            }
        });
    });
}

async function fetchById(req, res, next) {
    try {
        let Contents = await Content.find({ courseId: req.params.id }).lean();
        if (!Contents) return res.status(200).send({});
        Contents = _.orderBy(Contents, 'seq', 'asc');
        let resArray = [];
        for (let item of Contents) {
            item.videoUrl = _.orderBy(item.videoUrl, 'vseq', 'asc');
            resArray.push(item)
        }
        res.status(200).send(resArray);
    } catch (err) {
        log.info({ message: 'Content fetchById Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error : ' + err.toString());
    }
}

function getDemoUrl(req, res, next) {
    Content.find({ courseId: req.params.id })
        .exec(function(err, Contents) {
            if (err) {
                log.info({ message: 'Content getDemoUrl Error', label: 'ERROR', msg: err.stack });
                return res.status(500).send('Internal Server Error : ' + err.toString());
            } else if (!Contents) {
                return res.status(200).send({});
            } else {
                Contents = _.orderBy(Contents, 'order', 'asc');
                if (Contents.length > 0 && Contents[0] && Contents[0].videoUrl.length > 0) {
                    res.status(200).send(Contents[0].videoUrl[0]);
                } else {
                    return res.status(200).send({});
                }
            }

        });
}

function fetchByOne(req, res, next) {
    Content.find(req.body).populate({ path: 'courses', model: 'courses' }).exec(function(err, Contents) {
        if (err) {
            log.info({ message: 'Content fetchByOne Error', label: 'ERROR', msg: err.stack });
            return res.status(500).send('Internal Server Error : ' + err.toString());
        } else if (!Contents || Contents.length === 0) return res.status(404).send("You Content is empty, please add some product and try.");
        res.status(200).send(Contents[0]);
    });
}

function remove(req, res, next) {
    Content.findOne({ userId: req.body.userId }, function(err, Content) {
        let ContentArray = Content.courses;
        var index = ContentArray.indexOf(req.body.courseId);
        if (index > -1) {
            ContentArray.splice(index, 1);
        }
        Content.courses = ContentArray;
        Content.save(function(err, savedContent) {
            if (err) {
                log.info({ message: 'Content remove Error', label: 'ERROR', msg: err.stack });
                return res.status(500).send('Internal Server Error : ' + err.toString());
            }
            res.send({ status: 200, statusText: 'Sucessfully Deleted', message: 'Product removed from Content list.' });
        });
    });
}

function deleteAll(req, res, next) {
    Content.remove({ userId: req.params.id }, function(err, doc) {
        if (err) {
            log.info({ message: 'Content deleteAll Error', label: 'ERROR', msg: err.stack });
            return res.status(500).send('Internal Server Error : ' + err.toString());
        }
        res.send({ status: 200, statusText: 'Sucessfully Content Cleared', message: 'Product removed from Content list.' });
    });
}

async function deleteV(req, res) {
    try {
        let content = await Content.findById(req.body._id);
        let vurl = [];
        for (let v of content.videoUrl) {
            if (((v || {})._id || {}).toString() === req.body.vId) {
                if (fs.existsSync(v.path)) {
                    await fs.unlinkSync(v.path);
                }
            } else {
                vurl.push(v);
            }
        }
        content.videoUrl = vurl;
        await content.save();
        return res.status(200).send(content);
    } catch (err) {
        log.info({ message: 'Content deleteV Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error : ' + err.toString());
    }
}

async function deleteContent(req, res) {
    try {
        let contentDoc = await Content.findById(req.body._id);
        for (let item of contentDoc.videoUrl) {
            if (fs.existsSync(item.path)) {
                await fs.unlinkSync(item.path);
            }
        }
        let content = await Content.deleteOne({ _id: req.body._id });
        return res.status(200).send(content);
    } catch (err) {
        log.info({ message: 'Content deleteContent Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error : ' + err.toString());
    }
}