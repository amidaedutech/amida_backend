const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = mongoose.Types.ObjectId;
const fs = require('fs');
const FreeMatModel = require('../models/free-material');
const UploadImage = require('../utils/fileoperation');
const log = require('../utils/winston');
module.exports = {
    fetch: fetch,
    save: save,
    fetchById: fetchById,
    uploadBImage: uploadBImage,
    deleteById: deleteById
}

async function fetch(req, res, next) {
    try {
        var filterObj = {};
        if (req.body.rootCategoryName)
            filterObj.rootCategoryName = req.body.rootCategoryName;
        if (req.body.categoryName)
            filterObj.categoryName = req.body.categoryName;
        if (req.body.filterValue) {
            var re = new RegExp(req.body.filterValue, 'i');
            var matchFields = [];
            matchFields.push({ 'title': { $regex: re } }, { 'rootCategoryName': { $regex: re } }, { 'categoryName': { $regex: re } }, { 'subjectName': { $regex: re } }, );
            filterObj.$or = matchFields;
        }
        let freemat = await FreeMatModel.paginate(filterObj, { page: parseInt(req.body.skip), limit: parseInt(req.body.limit) });
        res.status(200).send(freemat);
    } catch (err) {
        log.info({ message: 'Freemat fetch Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error : ' + err.toString());
    }
}
async function save(req, res, next) {
    try {
        if (req.body._id) {
            let freeMatM = await FreeMatModel.findOne({ _id: req.body._id });
            for (let key of Object.keys(req.body)) {
                freeMatM[key] = req.body[key];
            }
            let saveData = freeMatM.save();
            res.status(200).send(saveData);
        } else {
            let freeMatModel = new FreeMatModel(req.body);
            let saveData = freeMatModel.save();
            res.status(200).send(saveData);
        }
    } catch (err) {
        log.info({ message: 'Freemat save Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error : ' + err.toString());

    }
}
async function fetchById(req, res, next) {
    try {
        let freeMat = await FreeMatModel.findById(req.body._id);
        res.status(200).send(freeMat);
    } catch (err) {
        log.info({ message: 'Freemat fetchById Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error : ' + err.toString());
    }
}

async function uploadBImage(req, res, next) {
    try {
        req.body.folderPath = '/course';
        const up = await UploadImage.uploadFile(req);
        return res.status(200).send({ data: up });
    } catch (err) {
        log.info({ message: 'Freemat uploadBImage Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error : ' + err.toString());
    }
}

async function deleteById(req, res, next) {
    try {
        let fmat = await FreeMatModel.findById(req.body._id);
        if (fmat.image && fs.existsSync(fmat.image)) {
            fs.unlinkSync(fmat.image);
        }
        await FreeMatModel.deleteOne({ _id: req.body._id });
        return res.status(200).send({ de: true });
    } catch (err) {
        log.info({ message: 'Freemat deleteById Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error : ' + err.toString());
    }
}