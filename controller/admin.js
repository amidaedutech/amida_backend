const hconfigModel = require('../models/hconfig');
const fileopr = require('../utils/fileoperation');
const fs = require('fs');
const path = require('path');
const Courses = require('../models/course');
var StreamZip = require('node-stream-zip');
var log = require('../utils/winston');
module.exports = {
    homeSlideDown: homeSlideDown,
    homeSlideUp: homeSlideUp,
    deleteHSlider: deleteHSlider,
    imageByPath: imageByPath,
    htmlContent: htmlContent,
    uploadHtmlContent: uploadHtmlContent,
    downlFile: downlFile,
    deleteFile: deleteFile
}

async function homeSlideDown(req, res, next) {
    try {
        let confModel = await hconfigModel.find({ type: 'homeslider' });
        return res.status(200).send(confModel);
    } catch (err) {
        log.info({ message: 'Admin Home Slide Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error : ' + err.toString());
    }
}

async function homeSlideUp(req, res, next) {
    try {
        // req.body.folderPath = req.query.folderPath;
        // req.body.type = req.query.type;
        // let imageup = await fileopr.uploadFile(req);
        // if (!imageup) return res.status(500).send('Unable to upload image');
        // for (let img of imageup) {
        // req.body.image = img;
        let hconModel = new hconfigModel(req.body);
        await hconModel.save();
        // }
        res.status(200).send({ upload: true });
    } catch (err) {
        log.info({ message: 'Admin Home Slide Upload Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error : ' + err.toString());
    }
}

async function deleteHSlider(req, res) {
    try {
        let configDoc = await hconfigModel.findById(req.body._id);
        if (fs.existsSync((configDoc || {}).image)) {
            await fs.unlinkSync(configDoc.image);
        }
        let model = await hconfigModel.deleteOne({ _id: req.body._id });
        res.status(200).send(model);
    } catch (err) {
        log.info({ message: 'Admin Home Slide Delete Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error : ' + err.toString());
    }
}

async function imageByPath(req, res, next) {
    try {
        let image = await fileopr.imageBase64(req.body.imagePath);
        return res.status(200).send({ image: image });
    } catch (err) {
        log.info({ message: 'Admin Home Get Image Path Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error : ' + err.toString());
    }
}

function htmlContent(req, res) {
    // fs.createReadStream('path/to/archive.zip')
    //     .pipe(unzip.Parse())
    //     .on('entry', function(entry) {
    //         var fileName = entry.path;
    //         var type = entry.type; // 'Directory' or 'File'
    //         var size = entry.size;
    //         if (fileName === "this IS the file I'm looking for") {
    //             entry.pipe(fs.createWriteStream('output/path'));
    //         } else {
    //             entry.autodrain();
    //         }
    //     });
}

async function uploadHtmlContent(req, res) {
    try {
        fileopr.uploadHtmlCont(req).then(function(fileDetails) {
            var zip = new StreamZip({ file: fileDetails.path, storeEntries: true });
            zip.on('error', function(err) { console.error('[ERROR]', err); });
            zip.on('ready', function() {
                let unzipFolder = path.join('../' + '/SkillAdda_images/htmlcontentunzip/');
                zip.extract(null, unzipFolder, (err, count) => {
                    console.log(err ? 'Extract error' : `Extracted ${count} entries`);
                    if (fs.existsSync(fileDetails.path)) {
                        fs.unlinkSync(fileDetails.path);
                    }
                    zip.close();
                    res.status(200).send(fileDetails);
                });
            });
        });
    } catch (err) {
        log.info({ message: 'Admin Home File Upload Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error : ' + err.toString());
    }
}

async function downlFile(req, res) {
    try {
        let base64 = await fileopr.docsBase64(req.body.path);
        res.send({ base64Data: base64 });
    } catch (err) {
        log.info({ message: 'Admin Home Unable to Read Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error : ' + err.toString());
    }
}

async function deleteFile(req, res) {
    try {
        let course = await Courses.findById(req.body._id);
        if (fs.existsSync(req.body.path)) {
            fs.unlinkSync(req.body.path);
            let docArray = [];
            for (let co of course.docs) {
                if (req.body.path !== co) {
                    docArray.push(co);
                }
            }
            course.docs = docArray;
            await course.save();
        }
        res.send(course);
    } catch (err) {
        log.info({ message: 'Admin Home File Delete Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error : ' + err.toString());
    }
}