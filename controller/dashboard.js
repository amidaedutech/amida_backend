const AssmtModel = require('../models/assessment');
const OrderModel = require('../models/order');
const ReviewModel = require('../models/review');
const ResultModel = require('../models/result');
const UserModel = require('../models/users');
const CourseModel = require('../models/course');
const RootCatModel = require('../models/root-category');
const CatModel = require('../models/category');
const SubjectModel = require('../models/subject');
const BlogModel = require('../models/blog');
const ContactUsModel = require('../models/contactus');
const PaymentModel = require('../models/payment');
const log = require('../utils/winston');
module.exports = {
    student: student,
    admin: admin,
    getCounts: getCounts
}

async function student(req, res) {
    try {
        let resObj = { count: [] };
        const myorder = new Promise(function(resolve, reject) {
            OrderModel.find({ purchasedBy: req.body.userId })
                .populate({ path: 'course', model: 'courses' })
                .populate({ path: 'payment', model: 'payments' }).sort([
                    ['created', -1]
                ])
                .exec(function(err, orderlist) {
                    if (err) {
                        reject(false)
                    } else {
                        var courseList = [];
                        let totalPrice = 0;
                        orderlist.forEach(element => {
                            totalPrice += element.amount;
                            courseList = courseList.concat(element.course);
                        });
                        resObj.count.push({ countLabel: 'No Of Courses', countVal: orderlist.length, });
                        resObj.myorder = orderlist;
                        resolve(true);
                    }
                });
        });
        const assmnt = new Promise(function(resolve, reject) {
            ResultModel.find({ userId: req.body.userId }).exec(function(err, docs) {
                if (err) {
                    reject(false)
                } else {
                    resObj.count.push({ countLabel: 'No Of Assessment', countVal: docs.length });
                    resObj.myassmnt = docs;
                    resolve(true);
                }
            });
        });
        const myreview = new Promise(function(resolve, reject) {
            ReviewModel.find({ reviwedBy: req.body.userId }, function(err, docs) {
                if (err) {
                    reject(false);
                } else {
                    resObj.myreview = docs;
                    resolve(true);
                }
            });
        });


        Promise.all([myorder, assmnt, myreview]).then(function(values) {
            res.status(200).send(resObj);
        });
    } catch (err) {
        log.info({ message: 'Dashboard student Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error : ' + err.toString());
    }
}

async function admin(req, res) {
    try {
        let resObj = { count: [] };
        const stdCreated = new Promise(function(resolve, reject) {
            UserModel.aggregate([{ $match: { role: 'student' } },
                {
                    $group: {
                        _id: { year: { $year: "$created" }, month: { $month: "$created" }, day: { $dayOfMonth: "$created" } },
                        count: { $sum: 1 }
                    }
                },
                {
                    $group: {
                        _id: { year: "$_id.year", month: "$_id.month" },
                        dailyusage: { $push: { day: "$_id.day", count: "$count" } }
                    }
                },
                {
                    $group: {
                        _id: { year: "$_id.year" },
                        monthlyusage: { $push: { month: "$_id.month", dailyusage: "$dailyusage" } }
                    }
                }
            ], function(err, result) {
                if (err) {
                    reject(false);
                } else {
                    resObj.stdCreated = result;
                    resolve(true);
                }
            });
        });
        const courseCreated = new Promise(function(resolve, reject) {
            CourseModel.aggregate([{
                    $group: {
                        _id: { year: { $year: "$created" }, month: { $month: "$created" }, day: { $dayOfMonth: "$created" } },
                        count: { $sum: 1 }
                    }
                },
                {
                    $group: {
                        _id: { year: "$_id.year", month: "$_id.month" },
                        dailyusage: { $push: { day: "$_id.day", count: "$count" } }
                    }
                },
                {
                    $group: {
                        _id: { year: "$_id.year" },
                        monthlyusage: { $push: { month: "$_id.month", dailyusage: "$dailyusage" } }
                    }
                }
            ], function(err, result) {
                if (err) {
                    reject(false);
                } else {
                    resObj.courseCreated = result;
                    resolve(true);
                }
            });
        });
        const orderCreated = new Promise(function(resolve, reject) {
            OrderModel.aggregate([{
                    $group: {
                        _id: { year: { $year: "$created" }, month: { $month: "$created" }, day: { $dayOfMonth: "$created" } },
                        count: { $sum: 1 }
                    }
                },
                {
                    $group: {
                        _id: { year: "$_id.year", month: "$_id.month" },
                        dailyusage: { $push: { day: "$_id.day", count: "$count" } }
                    }
                },
                {
                    $group: {
                        _id: { year: "$_id.year" },
                        monthlyusage: { $push: { month: "$_id.month", dailyusage: "$dailyusage" } }
                    }
                }
            ], function(err, result) {
                if (err) {
                    reject(false);
                } else {
                    resObj.orderCreated = result;
                    resolve(true);
                }
            });
        });
        const paymentAmount = new Promise(function(resolve, reject) {
            PaymentModel.aggregate([{
                    $group: {
                        _id: { year: { $year: "$created" }, month: { $month: "$created" }, day: { $dayOfMonth: "$created" } },
                        count: { $sum: '$amount' }
                    }
                },
                {
                    $group: {
                        _id: { year: "$_id.year", month: "$_id.month" },
                        dailyusage: { $push: { day: "$_id.day", count: "$count" } }
                    }
                },
                {
                    $group: {
                        _id: { year: "$_id.year" },
                        monthlyusage: { $push: { month: "$_id.month", dailyusage: "$dailyusage" } }
                    }
                }
            ], function(err, result) {
                if (err) {
                    reject(false);
                } else {
                    resObj.paymentAmount = result;
                    resolve(true);
                }
            });
        });
        const inquiryReport = new Promise((resolve, reject) => {
            ContactUsModel.aggregate([{
                    $group: {
                        _id: { year: { $year: '$created' }, month: { $month: '$created' }, day: { $dayOfMonth: '$created' } },
                        count: { $sum: 1 }
                    }
                },
                {
                    $group: {
                        _id: { year: '$_id.year', month: '$_id.month' },
                        dailyusage: { $push: { day: '$_id.day', count: '$count' } }
                    }
                },
                {
                    $group: {
                        _id: { year: '$_id.year' },
                        monthlyusage: { $push: { month: '$_id.month', dailyusage: '$dailyusage' } }
                    }
                }
            ], (err, result) => {
                if (err) {
                    reject(false);
                } else {
                    resObj.inquiryReport = result;
                    resolve(true);
                }
            });
        });
        const asmntStatus = new Promise(function(resolve, reject) {
            ResultModel.aggregate([{ $group: { _id: '$status', count: { $sum: 1 } } }], function(err, result) {
                if (err) {
                    reject(false);
                } else {
                    resObj.asmntStatus = result;
                    resolve(true);
                }
            });
        });
        Promise.all([stdCreated, courseCreated, orderCreated, paymentAmount, inquiryReport, asmntStatus]).then(function(values) {
            res.status(200).send(resObj);
        });
    } catch (err) {
        log.info({ message: 'Dashboard admin Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error : ' + err.toString());

    }

}

async function getCounts(req, res) {
    try {
        const student = new Promise(function(resolve, reject) {
            UserModel.count({ role: 'student' }, (err, count) => {
                if (err) {
                    reject(0)
                } else {
                    resolve({ stdent: count });
                }
            })
        })
        const rootCat = new Promise((resolve, reject) => {
            RootCatModel.count({}, (err, count) => {
                if (err) {
                    reject(0)
                } else {
                    resolve({ rootCat: count });
                }
            })
        })

        const cat = new Promise((resolve, reject) => {
            CatModel.count({}, (err, count) => {
                if (err) {
                    reject(0)
                } else {
                    resolve({ cat: count });
                }
            })
        })
        const blog = new Promise((resolve, reject) => {
            BlogModel.count({}, (err, count) => {
                if (err) {
                    reject(0)
                } else {
                    resolve({ blog: count });
                }
            })
        })
        const subject = new Promise((resolve, reject) => {
            SubjectModel.count({}, (err, count) => {
                if (err) {
                    reject(0)
                } else {
                    resolve({ subject: count });
                }
            })
        })
        const course = new Promise((resolve, reject) => {
            CourseModel.count({}, (err, count) => {
                if (err) {
                    reject(0)
                } else {
                    resolve({ course: count });
                }
            })
        })
        const asmnt = new Promise((resolve, reject) => {
            AssmtModel.count({}, (err, count) => {
                if (err) {
                    reject(0)
                } else {
                    resolve({ asmnt: count });
                }
            })
        })
        Promise.all([student, rootCat, cat, blog, subject, course, asmnt]).then(function(values) {
            let resObj = {};
            for (let item of values) {
                let keys = Object.keys(item);
                resObj[keys[0]] = item[keys[0]];
            }
            res.status(200).send(resObj);
        });
    } catch (err) {
        log.info({ message: 'Dashboard getCounts Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error : ' + err.toString());
    }
}