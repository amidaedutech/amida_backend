const BlogModel = require('../models/blog');
const UploadImage = require('../utils/fileoperation');
const BReview = require('../models/blogreview');
const BlogCatModel = require('../models/blog-category');
const BlogRootCatModel = require('../models/blog-root-cat');
const fs = require('fs');
const log = require('../utils/winston');
module.exports = {
    fetch: fetch,
    fetchById: fetchById,
    deleteById: deleteById,
    addBlog: addBlog,
    uploadBImage: uploadBImage,
    createReview: createReview,
    getReviews: getReviews,
    fetchAllReviews: fetchAllReviews,
    deleteReview: deleteReview,
    saveCat: saveCat,
    saveRooCat: saveRooCat,
    deleteCat: deleteCat,
    deleteRooCat: deleteRooCat,
    fetchCat: fetchCat,
    fetchRootCat: fetchRootCat,
    fetchCatRootCat: fetchCatRootCat
}

async function fetch(req, res, next) {
    try {
        let limit = 20;
        if (req.body.limit) {
            limit = req.body.limit;
        }
        let query = {};
        if ((req.body || {}).rootCategory) {
            query.rootCategory = req.body.rootCategory;
        }
        if ((req.body || {}).category) {
            query.category = req.body.category;
        }
        let blogModel = await BlogModel.find(query).select('title image category rootCategory summary created').sort({ 'created': -1 }).limit(limit);
        return res.status(200).send(blogModel);
    } catch (err) {
        log.info({ message: 'Blog fetch Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error : ' + err.toString());
    }
}
async function fetchById(req, res, next) {
    try {
        let blogModel = await BlogModel.findById(req.params.id);
        return res.status(200).send(blogModel);
    } catch (err) {
        log.info({ message: 'Blog fetchById Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error : ' + err.toString());
    }
}
async function deleteById(req, res, next) {
    try {
        let blog = await BlogModel.findById(req.body._id);
        if (blog.image && fs.existsSync(blog.image)) {
            fs.unlinkSync(blog.image);
        }
        await BlogModel.deleteOne({ _id: req.body._id });
        return res.status(200).send({ de: true });
    } catch (err) {
        log.info({ message: 'Blog deleteById Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error : ' + err.toString())
    }
}
async function addBlog(req, res, next) {
    try {
        let blogModel = {};
        if (req.body._id) {
            if (req.userId) {
                req.body.modifiedBy = req.userId;
                req.body.modified = new Date();
            }
            blogModel = await BlogModel.findById(req.body._id);
            for (const key in req.body) {
                blogModel[key] = req.body[key];
            }
        } else {
            if (req.userId) {
                req.body.createdBy = req.userId;
            }
            blogModel = new BlogModel(req.body);
        }
        await blogModel.save();
        return res.status(200).send({ sent: true });
    } catch (err) {
        log.info({ message: 'Blog addBlog Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error : ' + err.toString())
    }
}

async function uploadBImage(req, res) {
    try {
        req.body.folderPath = 'admin/blog';
        const up = await UploadImage.uploadFile(req);
        return res.status(200).send({ data: up });
    } catch (err) {
        log.info({ message: 'Blog uploadBImage Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error : ' + err.toString())
    }
}

async function createReview(req, res, next) {
    try {
        let reviews = await BReview.find({ blog: req.body.blog, reviwedBy: req.body.reviwedBy });
        if (reviews && reviews.length > 0) return res.status(302).send("Already reviewed by you.");
        let reviewModel = new BReview(req.body);
        let review = await reviewModel.save();
        let reviewList = await BReview.find({ blog: req.body.blog });
        await BlogModel.update({ _id: req.body.blog }, { $set: getReviewAvg(reviewList) });
        res.status(200).send(review);
    } catch (err) {
        log.info({ message: 'Blog createReview Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error : ' + err.toString())
    }
}

function getReviewAvg(reviewList) {
    var rating1 = 0;
    var rating2 = 0;
    var rating3 = 0;
    var rating4 = 0;
    var rating5 = 0;
    var avgRate = 1;
    reviewList.forEach(function(item) {
        if (item.rating === 1) {
            rating1++;
        } else if (item.rating === 2) {
            rating2++;
        } else if (item.rating === 3) {
            rating3++;
        } else if (item.rating === 4) {
            rating4++;
        } else if (item.rating === 5) {
            rating5++;
        }
    });

    avgRate = (5 * rating5 + 4 * rating4 + 3 * rating3 + 2 * rating2 + 1 * rating1) / reviewList.length;
    return { aggregateRating: avgRate, totalRating: reviewList.length };
}
async function getReviews(req, res, next) {
    try {
        let reviews = await BReview.find({ blog: req.params.id }).populate({ path: 'reviwedBy', model: 'user' });
        res.status(200).send(reviews);
    } catch (err) {
        log.info({ message: 'Blog getReviews Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error : ' + err.toString())
    }
}

async function fetchAllReviews(req, res) {
    try {
        let reviewQuery = {};
        if (((req.body || {}).filter || {}).filterText) {
            var re = new RegExp(req.body.filter.filterText, 'i');
            reviewQuery.review = { $regex: re };
        }

        if (((req.body || {}).filter || {}).username) {
            reviewQuery['reviwedBy.username'] = req.body.filter.username
        }
        let reviewcount = await BReview.aggregate([
            { $lookup: { from: "users", localField: "reviwedBy", foreignField: "_id", as: "reviwedBy" } },
            { $match: reviewQuery }
        ]);
        let reviews = await BReview.aggregate([
            { $lookup: { from: "users", localField: "reviwedBy", foreignField: "_id", as: "reviwedBy" } },
            { $match: reviewQuery },
            { $sort: { created: -1 } },
            { $limit: req.body.skip + req.body.limit },
            { $skip: req.body.skip }

        ]);
        return res.send({
            limit: req.body.limit,
            offset: req.body.offset,
            page: req.body.offset,
            skip: req.body.skip,
            total: reviewcount.length,
            docs: reviews
        });
    } catch (err) {
        log.info({ message: 'Blog fetchAllReviews Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error : ' + err.toString())
    }
}

async function deleteReview(req, res) {
    try {
        let review = await BReview.remove({ _id: req.params.id });
        return res.status(200).send(review);
    } catch (err) {
        log.info({ message: 'Blog deleteReview Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error : ' + err.toString())
    }
}

async function saveCat(req, res) {
    try {
        let blogModel = {};
        if (req.body.type === 'root-category') {
            if (req.body._id) {
                if (req.userId) {
                    req.body.modifiedBy = req.userId;
                    req.body.modified = new Date();
                }
                blogModel = await BlogRootCatModel.findById(req.body._id);
                for (const key in req.body) {
                    blogModel[key] = req.body[key];
                }
            } else {
                if (req.userId) {
                    req.body.createdBy = req.userId;
                }
                blogModel = new BlogRootCatModel(req.body);
            }
        } else if (req.body.type === 'category') {
            if (req.body._id) {
                if (req.userId) {
                    req.body.modifiedBy = req.userId;
                    req.body.modified = new Date();
                }
                blogModel = await BlogCatModel.findById(req.body._id);
                for (const key in req.body) {
                    blogModel[key] = req.body[key];
                }
            } else {
                if (req.userId) {
                    req.body.createdBy = req.userId;
                }
                blogModel = new BlogCatModel(req.body);
            }
        }

        await blogModel.save();
        return res.status(200).send({ sent: true });
    } catch (err) {
        log.info({ message: 'Blog saveCat Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error : ' + err.toString())
    }
}
async function saveRooCat(req, res) {
    try {
        let blogModel = {};
        if (req.body._id) {
            if (req.userId) {
                req.body.modifiedBy = req.userId;
                req.body.modified = new Date();
            }
            blogModel = await BlogRootCatModel.findById(req.body._id);
            for (const key in req.body) {
                blogModel[key] = req.body[key];
            }
        } else {
            if (req.userId) {
                req.body.createdBy = req.userId;
            }
            blogModel = new BlogRootCatModel(req.body);
        }
        await blogModel.save();
        return res.status(200).send({ sent: true });
    } catch (err) {
        log.info({ message: 'Blog saveRootCat Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error : ' + err.toString())
    }
}
async function deleteCat(req, res) {
    try {
        let blogCat = await BlogCatModel.remove({ _id: req.params.id });
        return res.status(200).send(blogCat);
    } catch (err) {
        log.info({ message: 'Blog deleteCat Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error : ' + err.toString())
    }
}
async function deleteRooCat(req, res) {
    try {
        await BlogCatModel.deleteMany({ blogrootcat: req.params.id });
        let blogRootCat = await BlogRootCatModel.remove({ _id: req.params.id });
        return res.status(200).send(blogRootCat);
    } catch (err) {
        log.info({ message: 'Blog deleteRooCat Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error : ' + err.toString())
    }
}
async function fetchCat(req, res) {
    try {
        let query = {};
        if (req.params) {
            query = req.query;
        }
        let blogList = await BlogCatModel.find(query).lean();
        return res.status(200).send(blogList);
    } catch (err) {
        log.info({ message: 'Blog fetchCat Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error : ' + err.toString())
    }
}
async function fetchRootCat(req, res) {
    try {
        let blogList = await BlogRootCatModel.find({});
        return res.status(200).send(blogList);
    } catch (err) {
        log.info({ message: 'Blog fetchRootCat Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error : ' + err.toString())
    }
}

async function fetchCatRootCat(req, res) {
    try {
        let blogRootType = await BlogRootCatModel.find({}).lean();
        let blogList = await BlogCatModel.find({}).lean();
        let blogObj = {};
        let finalArray = [];
        for (let rc of blogRootType) {
            let subCat = [];
            for (let blog of blogList) {
                if (rc._id.equals(blog.blogrootcat))
                    subCat.push(blog);
            }
            blogObj[rc.name] = subCat;
        }

        for (let key in blogObj) {
            finalArray.push({ rootCat: key, cat: blogObj[key] })
        }
        return res.status(200).send(finalArray);
    } catch (err) {
        log.info({ message: 'Blog fetchCatRootCat Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error : ' + err.toString())
    }
}