const mongoose = require('mongoose');
const ObjectId = mongoose.Types.ObjectId;
const dialogflow = require('dialogflow').v2beta1;
const moment = require('moment');
const request = require('request');
const log = require('../utils/winston');
const ChatMsgModel = require('../models/chatmsgs');
const Courses = require('../models/course');
const ChatModel = require('../models/chatbox');

module.exports = {
    saveMessage: saveMessage,
    runMainBot: runMainBot,
    runChapterBot: runChapterBot,
    getCourses: getCourses,
    getWikiAnswers: getWikiAnswers,
    getChats: getChats,
    saveMessages: saveMessages
}

async function saveMessage(req, res) {
    try {
        return res.status(200).send(chatUsers);
    } catch (err) {
        return res.status(500).send('Unable to fetch chat users/' + err.toString());
    }
}


/**
 * Send a query to the dialogflow agent, and return the query result.
 * @param {string} projectId The project to be used
 */
async function runMainBot(req, res) {
    console.log("body", req.body);
    let body = req.body;
    const projectId = 'SkillAddaagent-jpgshu';
    // A unique identifier for the given session
    //const sessionId = uuid.v4();
    const sessionId = body.chatId;
    let config = {
            credentials: {
                private_key: '-----BEGIN PRIVATE KEY-----\nMIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQDCyB6tfgpeBBhe\n7F57o6xR36CkryLD7cdhCom+g2I2d3pDGCaD3KVT2LpqGnW3GcB3D46F68hE7OW7\nO5Qv6BG/W+19EhVQQrI/jzMe5bridj3QbMgtfIcTM1+MTEQv4C+3cCxOBttrvNK/\ncZ407EsXLHHjvec9+0AdlCi6RqtOgRx3NPpB138mGNTgha1MM7quzcpCp27Ehg9l\nxogwhIH7eUJVnNeiN/8XytXTxcJZH1QMGP0Sg6cabPuJSlTlwDcNfLsiPucc9H00\n4IDvt0MGNDEkR3mhAKSx/m/D7/Mo9RcV6kM2LhxnoS3r1lU/OSbd/N/dNn7dkCsZ\n1nKXGc0fAgMBAAECggEAGXrupxkWXRnMYFqBD6EnxnwVD9td67iGvPc7xqwkObt5\nHHQj0bBtq4wYnWRXDXv1FVN30aVi85VJMNbd5Jhx/eZA09F87/IRpEaPfuq5fwIj\nGvP9XxSWYkN1q9eW50ESAj1Oe783jp6gXm02r3X7wA0khuYi7RTu3GUIdjm5aNGO\nLDWDAptITTFKaeEnBmu13kIepdCMnw4LSuWgu0AGtQQySBhD+J1a4qlo8+FvtfFv\ntsSgdBSPM6cpOFmh8jNyupUtlGxfRCxOi1l4dQNDusGpBUbvgEOJW7jFytp5TwHl\nyCt/Rbwbj+8kZdUnWLlza9XckBKmdoijF+P7AUN+uQKBgQDoj85AtxShtwKxmUyD\nu5jVqjRj7xRSobzprG+oEgCKxKYaEw18X87HIiMIMYlBtZ87geBX8kwJMJxHaeVI\n00n7t7nHRva4dHKeFZxidpYjQFmrLTYRLj5Ndvp8esqi7fjb9ri3lLhoY828Zy/0\nb+IbG+s0IA9XCGZ/kWYjOBDfSQKBgQDWaZK2fiLniFSz27QFj4xE6o+l5tOND4VM\ndHfeG3UAdvac4cKrg21/pPuyvZT/JZGdR3e4DSGGyPFcJyclsAJ0TuGmPu2iXeNw\nieSV9f1p3cpJKSuh2rUxhhdPPiESxdnSqE9jSsNYyh6DVzm0f3gMKaCMBh2wT2EX\nr/EtN+SBJwKBgB2hZOf965UHGkLCg2stVbWzb+VjqrU0gjtLixZg5MzJbVhZIRi9\npDze7pO2rARBPb55N7o81s888ExxA5OZyA1YRUVMl1a9D3Vwz3IEJfDr3ADhS8g4\nmhuIkzWU94h2xYRRFQve2PgD70yX4RZFwwstLXSAuTOfkF+kiNzn4c5xAoGAPIqY\n05CwOGIlUJfDsCJFQBxfLUANEfKE9/6W7g8grV3p6ohQmG9G1tj1/RVM4fJwuFE6\nu6xxgAZzKEsh4aFeQAsZ6gAwxzztgJRfB/zqbh8jBB9s44nGydIL+OMZIZzeEX5W\n1DmDCSohDYLul0m60uxaGc+MSIFRcAfM+LCkg9cCgYARCzz1iNAZJIDcpWJV48au\nfMyUH/2dv6IOYjPjoUN9UgJSzF8lHFSHK+/XJg8XExrR9fiqI5m813W388AWWuA1\nSmcLy3gB5zJYxKvrM6tw67QIFCuJMuk+raWm9X3py5FxiIXlJmMj4Fpwr03hB2zS\ndhjxWxQ3qzA2Uh2wjwPY9A==\n-----END PRIVATE KEY-----\n',
                client_email: 'SkillAddaagent-jpgshu@appspot.gserviceaccount.com'
            }
        }
        // Create a new session
    const sessionClient = new dialogflow.SessionsClient(config);

    const sessionPath = sessionClient.sessionPath(projectId, sessionId);

    // The text query request.
    const request = {
        session: sessionPath,
        queryInput: {
            text: {
                // The query to send to the dialogflow agent
                text: body.text,
                // The language used by the client (en-US)
                languageCode: 'en-US',
            },
        }
    };
    console.log("request", request);
    // Send request and log result
    const responses = await sessionClient.detectIntent(request);
    const result = responses[0].queryResult;
    console.log('Detected intent', responses);
    if (result.intent) {
        return res.status(200).send({ message: result.fulfillmentText, created: new Date() });
    } else {
        console.log(`  No intent matched.`);
        return res.status(200).send({ message: "Sorry, I didn't get you", created: new Date() });
    }
}
async function runChapterBot(req, res) {
    let body = req.body;
    const projectId = 'SkillAddaagent-jpgshu';
    // A unique identifier for the given session
    //const sessionId = uuid.v4();
    const sessionId = body.chatId;
    let config = {
        credentials: {
            private_key: '-----BEGIN PRIVATE KEY-----\nMIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQDCyB6tfgpeBBhe\n7F57o6xR36CkryLD7cdhCom+g2I2d3pDGCaD3KVT2LpqGnW3GcB3D46F68hE7OW7\nO5Qv6BG/W+19EhVQQrI/jzMe5bridj3QbMgtfIcTM1+MTEQv4C+3cCxOBttrvNK/\ncZ407EsXLHHjvec9+0AdlCi6RqtOgRx3NPpB138mGNTgha1MM7quzcpCp27Ehg9l\nxogwhIH7eUJVnNeiN/8XytXTxcJZH1QMGP0Sg6cabPuJSlTlwDcNfLsiPucc9H00\n4IDvt0MGNDEkR3mhAKSx/m/D7/Mo9RcV6kM2LhxnoS3r1lU/OSbd/N/dNn7dkCsZ\n1nKXGc0fAgMBAAECggEAGXrupxkWXRnMYFqBD6EnxnwVD9td67iGvPc7xqwkObt5\nHHQj0bBtq4wYnWRXDXv1FVN30aVi85VJMNbd5Jhx/eZA09F87/IRpEaPfuq5fwIj\nGvP9XxSWYkN1q9eW50ESAj1Oe783jp6gXm02r3X7wA0khuYi7RTu3GUIdjm5aNGO\nLDWDAptITTFKaeEnBmu13kIepdCMnw4LSuWgu0AGtQQySBhD+J1a4qlo8+FvtfFv\ntsSgdBSPM6cpOFmh8jNyupUtlGxfRCxOi1l4dQNDusGpBUbvgEOJW7jFytp5TwHl\nyCt/Rbwbj+8kZdUnWLlza9XckBKmdoijF+P7AUN+uQKBgQDoj85AtxShtwKxmUyD\nu5jVqjRj7xRSobzprG+oEgCKxKYaEw18X87HIiMIMYlBtZ87geBX8kwJMJxHaeVI\n00n7t7nHRva4dHKeFZxidpYjQFmrLTYRLj5Ndvp8esqi7fjb9ri3lLhoY828Zy/0\nb+IbG+s0IA9XCGZ/kWYjOBDfSQKBgQDWaZK2fiLniFSz27QFj4xE6o+l5tOND4VM\ndHfeG3UAdvac4cKrg21/pPuyvZT/JZGdR3e4DSGGyPFcJyclsAJ0TuGmPu2iXeNw\nieSV9f1p3cpJKSuh2rUxhhdPPiESxdnSqE9jSsNYyh6DVzm0f3gMKaCMBh2wT2EX\nr/EtN+SBJwKBgB2hZOf965UHGkLCg2stVbWzb+VjqrU0gjtLixZg5MzJbVhZIRi9\npDze7pO2rARBPb55N7o81s888ExxA5OZyA1YRUVMl1a9D3Vwz3IEJfDr3ADhS8g4\nmhuIkzWU94h2xYRRFQve2PgD70yX4RZFwwstLXSAuTOfkF+kiNzn4c5xAoGAPIqY\n05CwOGIlUJfDsCJFQBxfLUANEfKE9/6W7g8grV3p6ohQmG9G1tj1/RVM4fJwuFE6\nu6xxgAZzKEsh4aFeQAsZ6gAwxzztgJRfB/zqbh8jBB9s44nGydIL+OMZIZzeEX5W\n1DmDCSohDYLul0m60uxaGc+MSIFRcAfM+LCkg9cCgYARCzz1iNAZJIDcpWJV48au\nfMyUH/2dv6IOYjPjoUN9UgJSzF8lHFSHK+/XJg8XExrR9fiqI5m813W388AWWuA1\nSmcLy3gB5zJYxKvrM6tw67QIFCuJMuk+raWm9X3py5FxiIXlJmMj4Fpwr03hB2zS\ndhjxWxQ3qzA2Uh2wjwPY9A==\n-----END PRIVATE KEY-----\n',
            client_email: 'SkillAddaagent-jpgshu@appspot.gserviceaccount.com'
        }
    }

    // Create a new session
    const sessionClient = new dialogflow.SessionsClient(config);
    const sessionPath = sessionClient.sessionPath(projectId, sessionId);

    // The text query request.
    const request = {
        session: sessionPath,
        queryInput: {
            text: {
                // The query to send to the dialogflow agent
                text: body.message,
                // The language used by the client (en-US)
                languageCode: 'en-US',
            },
        },
    };

    // Send request and log result
    const responses = await sessionClient.detectIntent(request);
    const result = responses[0].queryResult;
    console.log('Detected intent', responses);
    if (result.intent) {
        return res.status(200).send({ message: result.fulfillmentText });
    } else {
        console.log(`  No intent matched.`);
        return res.status(200).send({ message: 'No intent matched' });
    }
}

function getCourses(req, res) {
    var param = ((req.body.queryResult || {}).parameters || {}).coursename;
    if (!param) {
        param = "upsc";
    }
    //console.log("body", req.body);
    let obj = {
        "fulfillmentText": "The course is on the way!!",
        "fulfillmentMessages": [{
            "text": { "text": ["The course is on the way!!"] }
        }],
        "source": ""
    }
    console.log("param", param);
    const regex = new RegExp(param.toLowerCase().trim(), 'i');
    console.log("regex", regex);
    Courses.find({ keywords: { $regex: regex } },
        function(err, courses) {
            console.log("err", err, courses.length);
            if (courses && courses.length > 0) {
                let coursestr = "Here are the results:";
                for (var i in courses) {
                    let course = courses[i];
                    if (course && course.name) {
                        coursestr += '<br>';
                        coursestr += "<u><a href='https://www.SkillAddaedutech.com/#/course/products/" + course._id + "'>" + course.name + "</a></u>";
                    } else {
                        coursestr = "Sorry, No course found!!";
                    }
                }
                obj.fulfillmentText = coursestr;
                obj.fulfillmentMessages[0].text.text = [coursestr];

            } else {
                obj.fulfillmentMessages[0].text.text = ["Sorry, No course found!!"];
                obj.fulfillmentText = "Sorry, No course found!!";
            }
            res.status(200).send(obj);
        });

}

function getWikiAnswers(req, res) {
    var param = ((req.body.queryResult || {}).parameters || {}).any;
    if (!param) {
        param = "web development";
    }
    //console.log("body", req.body);
    let obj = {
        "fulfillmentText": "Sorry, Can't answer for your question!!",
        "fulfillmentMessages": [{
            "text": { "text": ["Sorry, Can't answer for your question!!"] }
        }],
        "source": ""
    }
    console.log("param", param);
    request('https://en.wikipedia.org/w/api.php?format=json&action=query&prop=extracts&exintro&explaintext&redirects=1&titles=' + param, function(err, response, body) {
        console.error('error:', err, JSON.parse(body));
        body = JSON.parse(body);
        if (body && body.query && body.query.pages) {
            var key = Object.keys(body.query.pages);
            if (key && body.query.pages[key] && body.query.pages[key].extract) {
                obj.fulfillmentText = body.query.pages[key].extract;
                obj.fulfillmentMessages[0].text.text = [obj.fulfillmentText];
            } else {
                obj.fulfillmentMessages[0].text.text = ["Sorry, Can't answer for your question!!"];
                obj.fulfillmentText = "Sorry, Can't answer for your question!!";
            }
        } else {
            obj.fulfillmentMessages[0].text.text = ["Sorry, Can't answer for your question!!"];
            obj.fulfillmentText = "Sorry, Can't answer for your question!!";
        }
        res.status(200).send(obj);
    });

}

async function getChats(req, res) {
    console.log("params", req.params.id);
    let chats = await ChatModel.find({ chatId: req.params.id }).sort({ created: 1 }).exec();
    res.status(200).send(chats);
}
async function saveMessages(req, res) {
    var body = req.body;
    body.created = moment().format();
    // let obj = body;
    // obj.createdBy = 'SkillAddaBot';
    // obj.created = moment().format();
    // obj.text = result.fulfillmentText;
    let chatModel = new ChatModel(body);
    let savedObj = await chatModel.save();
    res.status(200).send(savedObj);
}