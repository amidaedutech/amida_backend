const mongoose = require('mongoose');
const ObjectId = mongoose.Types.ObjectId;
const ChatModel = require('../models/chatbox');
const ChatMsgModel = require('../models/chatmsgs');
const log = require('../utils/winston');
module.exports = {
    getMsgsByChatId: getMsgsByChatId,
    getChatUsers: getChatUsers,
    getCurrentUser: getCurrentUser
}

async function getChatUsers(req, res) {
    try {
        let chatUsers = await ChatModel.find({}).populate({ model: 'user', path: 'createdBy' }).sort({ status: -1 });
        return res.status(200).send(chatUsers);
    } catch (err) {
        log.info({ message: 'ChatBox getChatUsers Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error : ' + err.toString());
    }
}

async function getMsgsByChatId(req, res) {
    try {
        let chatMsgs = await ChatMsgModel.find({ chatId: req.body.chatId });
        return res.status(200).send(chatMsgs);
    } catch (err) {
        log.info({ message: 'ChatBox getMsgsByChatId Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error : ' + err.toString());
    }
}

async function getCurrentUser(req, res) {
    try {
        let chatUser = await ChatModel.findOne({ createdBy: req.body.createdBy }).populate({ model: 'user', path: 'createdBy' });
        return res.status(200).send(chatUser);
    } catch (err) {
        log.info({ message: 'ChatBox getCurrentUser Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error : ' + err.toString());
    }
}