const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = mongoose.Types.ObjectId;
const JobsModel = require('../models/jobs');
const JobjApplyModel = require('../models/jobapply');
const UploadImage = require('../utils/fileoperation');
const JobConfigModel = require('../models/jobconfig');
const log = require('../utils/winston');
module.exports = {
    fetch: fetch,
    fetchById: fetchById,
    deleteById: deleteById,
    addJobs: addJobs,
    uploadResume: uploadResume,
    applyJob: applyJob,
    deleteApplyJob: deleteApplyJob,
    fetchApplyJobs: fetchApplyJobs,
    jobConfigAdd: jobConfigAdd,
    jobConfigRemove: jobConfigRemove,
    jobFetchConfig: jobFetchConfig,
    updateApplyJob: updateApplyJob
}

async function fetch(req, res, next) {
    try {
        let limit = 20;
        if (req.body.limit) {
            limit = req.body.limit;
        }
        let query = {};
        if (req.body.searchString) {
            var re = new RegExp(req.body.searchString, 'i');
            var matchFields = [];
            matchFields.push({ 'title': { $regex: re } });
            query.$or = matchFields;
        }
        if (req.body.status) {
            query.status = req.body.status;
        }
        if ((req.body || {}).category) {
            query.category = req.body.category;
        }
        if ((req.body || {}).location) {
            query.location = req.body.location;
        }
        if (req.body.type) {
            query.type = req.body.type;
        }
        let jobsModel = await JobsModel.paginate(query, { select: 'status title summary category location type created', sort: { created: -1 }, page: parseInt(req.body.skip), limit: parseInt(req.body.limit) });
        return res.status(200).send(jobsModel);
    } catch (err) {
        log.info({ message: 'Jobs fetch Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error : ' + err.toString());
    }
}
async function fetchApplyJobs(req, res, next) {
    try {
        let limit = 20;
        if (req.body.limit) {
            limit = req.body.limit;
        }
        let query = {};
        if ((req.body || {}).status) {
            query.status = req.body.status;
        }
        let jobsModel = await JobjApplyModel.paginate(query, { sort: { 'created': -1 }, populate: { model: 'jobs', path: 'jobfor', select: 'status title summary category location type created' }, lean: true, page: parseInt(req.body.skip), limit: parseInt(req.body.limit) });
        return res.status(200).send(jobsModel);
    } catch (err) {
        log.info({ message: 'Jobs fetchApplyJobs Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error : ' + err.toString());
    }
}
async function fetchById(req, res, next) {
    try {
        let jobsModel = await JobsModel.findById(req.params.id);
        res.status(200).send(jobsModel);
    } catch (err) {
        log.info({ message: 'Jobs fetchById Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error : ' + err.toString());
    }
}
async function deleteById(req, res, next) {
    try {
        await JobsModel.deleteOne({ _id: req.body._id });
        return res.status(200).send({ de: true });
    } catch (err) {
        log.info({ message: 'Jobs deleteById Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error : ' + err.toString());
    }
}
async function addJobs(req, res, next) {
    try {
        let jobModel = {};
        if (req.body._id) {
            if (req.userId) {
                req.body.modifiedBy = req.userId;
                req.body.modified = new Date();
            }
            jobModel = await JobsModel.findById(req.body._id);
            for (const key in req.body) {
                jobModel[key] = req.body[key];
            }
        } else {
            if (req.userId) {
                req.body.createdBy = req.userId;
            }
            jobModel = new JobsModel(req.body);
        }
        await jobModel.save();
        return res.status(200).send({ sent: true });
    } catch (err) {
        log.info({ message: 'Jobs addJobs Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error : ' + err.toString());
    }
}

async function uploadResume(req, res) {
    try {
        req.body.folderPath = 'career/resume';
        const up = await UploadImage.uploadFile(req);
        return res.status(200).send({ data: up });
    } catch (err) {
        log.info({ message: 'Jobs uploadResume Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error : ' + err.toString());
    }
}

async function applyJob(req, res, next) {
    try {
        let findApplyJob = await JobjApplyModel.findOne({ email: req.body.email, jobfor: ObjectId(req.body.jobfor) }).lean();
        if ((findApplyJob || {}).email) return res.status(500).send('You have already applied');
        let jobModel = new JobjApplyModel(req.body);
        let jobDetails = await jobModel.save();
        return res.status(200).send(jobDetails);
    } catch (err) {
        log.info({ message: 'Jobs applyJob Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error : ' + err.toString());
    }
}
async function updateApplyJob(req, res, next) {
    try {
        let findApplyJob = await JobjApplyModel.findById(req.body._id);
        for (const key in req.body) {
            findApplyJob[key] = req.body[key];
        }
        await findApplyJob.save();
        return res.status(200).send({ sent: true });
    } catch (err) {
        log.info({ message: 'Jobs updateApplyJob Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error : ' + err.toString());
    }
}
async function deleteApplyJob(req, res, next) {
    try {
        await JobjApplyModel.deleteOne({ _id: req.body._id });
        return res.status(200).send({ de: true });
    } catch (err) {
        log.info({ message: 'Jobs deleteApplyJob Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error : ' + err.toString());
    }
}

async function jobFetchConfig(req, res, next) {
    try {
        let jobCon = await JobConfigModel.findOne({});
        return res.status(200).send(jobCon);
    } catch (err) {
        log.info({ message: 'Jobs jobFetchConfig Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error : ' + err.toString());
    }
}

async function jobConfigRemove(req, res, next) {
    try {
        let jobModel = await JobConfigModel.findOne({});
        if (req.body.flag === 'location' && req.body.location) {
            jobModel.locations.splice(jobModel.locations.indexOf(req.body.location), 1);
        } else if (req.body.flag === 'category' && req.body.category) {
            jobModel.categories.splice(jobModel.categories.indexOf(req.body.category), 1);
        }
        await jobModel.save();
        return res.status(200).send({ de: true });
    } catch (err) {
        log.info({ message: 'Jobs jobConfigRemove Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error : ' + err.toString());
    }
}

async function jobConfigAdd(req, res, next) {
    try {
        let jobModel = {};
        jobModel = await JobConfigModel.findOne({});
        if (jobModel) {
            if (req.body.flag === 'location') {
                if (((jobModel || {}).locations || {}).length > 0) {
                    if (jobModel.locations.indexOf(req.body.location) < 0) {
                        jobModel.locations.push(req.body.location);
                    } else {
                        return res.status(500).send('Location already available in list');
                    }
                } else {
                    jobModel.locations = [req.body.location];
                }
            } else if (req.body.flag === 'category') {
                if (((jobModel || {}).categories || {}).length > 0) {
                    if (jobModel.categories.indexOf(req.body.category) < 0) {
                        jobModel.categories.push(req.body.category);
                    } else {
                        return res.status(500).send('Category already available in list');
                    }
                } else {
                    jobModel.categories = [req.body.category];
                }
            } else if (req.body.flag === 'type') {
                if (((jobModel || {}).types || {}).length > 0) {
                    if (jobModel.types.indexOf(req.body.type) < 0) {
                        jobModel.types.push(req.body.type);
                    } else {
                        return res.status(500).send('Type already available in list');
                    }
                } else {
                    jobModel.types = [req.body.type];
                }
            }

        } else {
            let data = {};
            if (req.body.flag === 'location') {
                data = {
                    locations: [req.body.location]
                }
            } else {
                data = {
                    categories: [req.body.category]
                }
            }

            jobModel = new JobConfigModel(data);
        }

        await jobModel.save();
        return res.status(200).send({ sent: true });
    } catch (err) {
        log.info({ message: 'Jobs saveJob Error', label: 'ERROR', msg: err.stack });
        return res.status(500).send('Internal Server Error : ' + err.toString());
    }
}