'use strict';
var nodemailer = require('nodemailer');
var Q = require('q');
var path = require('path');
var config = require('../bin/server-config');
const { google } = require("googleapis");
// const OAuth2 = google.auth.OAuth2;
//model

module.exports = {
    sendingMail: sendingMail
};
// const oauth2Client = new OAuth2(
//     config.mail.client_id,
//     config.mail.client_secret, // Client Secret
//     "https://developers.google.com/oauthplayground" // Redirect URL
// );
// oauth2Client.setCredentials({
//     refresh_token: config.mail.refresh_token
// });
// const accessToken = oauth2Client.getAccessToken();

function sendingMail(template, data) {
    return new Promise(function(resolve, reject) {
        if (global.LOCAL_ENVIRONMENT === 'DEV') {
            emailAddress = config.mail.email;
        }
        // create reusable transporter object using the default SMTP transport
        var attachment = {};
        var transporter = nodemailer.createTransport({
            // sets automatically host, port and connection security settings  //we can use other services like "Yahoo"
            // service: "gmail",
            host: "smtp.yandex.com", // hostname can use server.massmediums.com
            port: 465,
            secure: true, // true for 465, false for other ports
            auth: {
                // type: 'OAuth2',
                user: config.mail.email,
                pass: config.mail.pwd,
                // clientId: config.mail.client_id,
                // clientSecret: config.mail.client_secret,
                // refreshToken: config.mail.refresh_token,
                // accessToken: accessToken
            },
            pool: true,
            buffer: true
                //    logger: true, // log to console
                //    debug: true // include SMTP traffic in the logs
        });

        // verify connection configuration
        transporter.verify(function(error, success) {
            if (error) {
                console.log(error);
            } else {
                console.log('Server is ready to take our messages');
            }
        });

        var mailOptions = {};
        // var attachment = {};

        mailOptions = {
            from: 'SkillAdda Edu <' + config.mail.email + '>', // sender address.  Must be the same as authenticated user if using Gmail.
            to: data.emailAddress, // list of receivers
            subject: data.subject, // Subject line
            html: '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml">' +
                '<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" />' +
                '<meta name="viewport" content="width=device-width, initial-scale=1.0"/>' +
                '<style type="text/css">.sym-email{color:#333;font:14px/130% Arial,Helvetica,sans-serif;margin:0;padding:20px}.sym-email p{display:block;margin:0 0 10px;padding:0}hr{border:0;border-top:1px solid #aaa;margin:25px 0;height:1px}</style>' +
                '</head><body><div class="sym-email">' + getTamplate(template, data) + '</div></body></html>' // html body
        };

        // if (settingsDoc.globalBCC) {
        //   mailOptions.bcc = settingsDoc.globalBCC;
        // }

        // attachment = {
        //   filename: 'ShippingLabel',
        //   path: path.resolve(__dirname, '../barcode_documents/pdf/' + data.itemId + '.pdf'),
        //   contentType: 'application/pdf'
        // };
        //   if (attachment) {
        //     mailOptions.attachments = [attachment];
        //   }

        // send mail with defined transport object
        transporter.sendMail(mailOptions, function(err, info) {
            if (err) {
                console.log('Error occurred when sending mail..');
                console.log(err.message);
                reject(err);
                return;
            } else {
                console.log('Message sent successfully!');
                console.log('Message sent: ' + info);
                resolve(info);
            }
            transporter.close(); // shut down the connection pool, no more messages.  Comment this line out to continue sending emails.
        });
    });
}

function getTamplate(template, data) {
    if (template === 'resetPassword') {
        return 'Dear ' + data.username + '<div><p>' +
            'You have requested a password reset, please follow the link below to reset your password.</p>' +
            '<p>Please ignore this email if you did not request a password change.</p>' +
            '<p><a href="' + data.url + '">Follow this link to reset your password.</a></p></div>';
    } else if (template === 'currentPassword') {
        return 'Dear ' + data.username + '<div><p>' +
            'Welcome to SkillAdda E-Learning, your current password is: <b>SkillAdda123$</b>' +
            '<p>Please change your password by current password, login with current password, go to profile page.</p>' +
            '<p><a href="' + data.url + '">Follow this link to reset your password.</a></p></div>';
    } else if (template === 'mailverify') {
        return 'Dear ' + data.username + '<div><p>' +
            'Welcome to SkillAdda E-Learning, Please verify your email address by clicking on the link below or by opening it in your browser:' +
            '<p><a href="' + data.url + '">Follow this link to verify email.</a></p></div>';
    } else if (template === 'forgotpwd') {
        return 'Dear ' + data.username + '<div><p> Your new password is ' + data.hashedPassword + ' for SkillAddaEdu Login. Please Reset your password after login.</p></div>'
    }
}