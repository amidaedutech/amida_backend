var request = require('request').defaults({ encoding: null });
const fs = require('fs');
const path = require('path');
module.exports = {
    encode_base64: encode_base64,
    sms: sms
};

function encode_base64(filename, callback) {
    return new Promise((resolve, reject) => {
        if (!filename) {
            fs.readFile(path.join('/public/images/', 'inf.png'), (err, data) => {
                if (err) {
                    resolve('false');
                } else {
                    const buf = Buffer.from(data);
                    const base64 = buf.toString('base64');
                    resolve(base64);
                }
            });
        } else {
            if (filename.indexOf("http") > -1) {
                request.get(filename, function(error, response, body) {
                    if (!error && response.statusCode == 200) {
                        data = new Buffer(body).toString('base64');
                        resolve(data);
                    } else {
                        resolve('false');
                    }
                });
            } else {
                fs.readFile(path.join(filename), function(error, data) {
                    if (error) {
                        resolve('false');
                    } else {
                        var buf = Buffer.from(data);
                        var base64 = buf.toString('base64');
                        resolve(base64);
                    }
                });
            }
        }
    });
}

function sms(data) {
    console.log(data);
    return new Promise((resolve, reject) => {
        let options = {
            json: true,
            url: `http://103.16.101.52:8080/bulksms/bulksms?username=ints-SkillAddaedu&password=edu12333&type=0&dlr=1&destination=${data.phone}&source=AMDAED&message=${data.msg}`
        };
        request.post(options, (err, res, body) => {
            if (err) {
                reject(err);
            } else {
                console.log(body);
                resolve(body);
            }
        });
    })

}