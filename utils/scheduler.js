const schedule = require('node-schedule');
const OrderModel = require('../models/order');
const Payment = require('../models/payment');
var payumoney = require('payumoney-node');
const PAYMENT = require('../bin/server-config').PAYMENT;
const Assessment = require('../models/assessment');
const Question = require('../models/questions');
const Result = require('../models/result');
const NotiModel = require('../models/notification');

var rule = new schedule.RecurrenceRule();
rule.minute = new schedule.Range(0, 59, 5);
schedule.scheduleJob(rule, async() => {
    console.log('------payment api call------');
    try {
        let order = await OrderModel.find({ payment: { $exists: false } });
        for (let p of order) {
            console.log('orderid-----', p.orderId);
            let payStatus = await payment(p.orderId);
            if (payStatus && payStatus.length > 0) {
                let payModel = new Payment(payStatus[0].postBackParam);
                let paymentDoc = await payModel.save();
                await OrderModel.update({ orderId: p.orderId }, { $set: { payment: paymentDoc._id } });
            } else if (payStatus === null) {
                // incase some unknown issue while payment then dummy payment here.
                let payModel = new Payment({
                    txnid: p.orderId,
                    status: 'failure',
                });
                let paymentDoc = await payModel.save();
                await OrderModel.update({ orderId: p.orderId }, { $set: { payment: paymentDoc._id } });
            }
        }
    } catch (err) {
        console.log('Payment scheduler err', err);
    }
});

var rule = new schedule.RecurrenceRule();
rule.minute = new schedule.Range(0, 59, 50);
schedule.scheduleJob(rule, async() => {
    console.log('------payment refund status api call------');
    try {
        let updatePayment = await Payment.find({ status: 'Refund Initiated' });
        if ((updatePayment || {}).length > 0) {
            for (let pay of updatePayment) {
                console.log('pay.payuMoneyId', pay.payuMoneyId);
                let paymentDoc = await RefundStatusCall(pay.payuMoneyId);
                console.log('paymentDoc', paymentDoc);
                // if (paymentDoc.status === 0) {
                //     pay.status = paymentDoc.message;
                //     await pay.save();
                // }
            }

        }
    } catch (err) {
        console.log('Payment scheduler err', err);
    }
});

var rule2 = new schedule.RecurrenceRule();
rule2.minute = new schedule.Range(0, 59, 50); //50 min
// rule2.minute = new schedule.Range(0, 59, 1); // 1min
schedule.scheduleJob(rule2, async() => {
    console.log('------Assessment assign scheduler------');
    try {
        let assFlag = false;
        let start = new Date();
        start.setHours(0, 0, 0, 0);
        let end = new Date();
        end.setHours(23, 59, 59, 999);
        let order = await OrderModel.find({ created: { $gte: start, $lte: end } }).populate({ path: 'payment', model: 'payments' }).lean();
        if ((order || {}).length > 0) {
            for (let o of order) {
                assFlag = false;
                for (let course of o.course) {
                    if (((o || {}).payment || {}).status === 'success') {
                        let findAsments = await Assessment.find({ courseId: course, status: 'published' }).lean();
                        if ((findAsments || {}).length > 0) {
                            for (let ass of findAsments) {
                                let questions = await Question.find({ assessmentId: ass._id }).lean();
                                let result = {};
                                result.name = ass.name;
                                result.assessmentId = ass._id;
                                result.category = ass.category;
                                result.subject = ass.subject;
                                result.userId = o.purchasedBy;
                                result.courseId = ass.courseId;
                                result.duration = ass.duration;
                                result.answers = [];
                                for (var j in questions) {
                                    result.answers.push({
                                        question: questions[j]._id
                                    });
                                }

                                var assResult = await Result.findOne({ assessmentId: ass._id, userId: o.purchasedBy });
                                if (!(assResult || {}).userId) {
                                    assFlag = true;
                                    await new Result(result).save();
                                }
                            }
                        }
                    }
                }

                if (assFlag) {
                    let notificModel = { type: 'Assessment' }
                    notificModel.to = o.purchasedBy;
                    notificModel.msg = 'Please check your new assessment.';
                    await NotiModel.create(notificModel);
                }
            }
        }
    } catch (err) {
        console.log('Assessment scheduler err', err);
    }
});

function payment(txid) {
    return new Promise((resolve, reject) => {
        payumoney.paymentResponse(txid, function(err, response) {
            if (err) {
                reject(err)
            } else {
                resolve(response);
            }
        });
    })
}

function RefundStatusCall(paymentId) {
    return new Promise((resolve, reject) => {
        payumoney.refundStatus(paymentId, function(err, response) {
            if (err) {
                reject(err)
            } else {
                resolve(response);
            }
        });
    })
}