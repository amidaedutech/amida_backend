/**
 * Created by user1 on 4/4/16.
 */
// const { combine, timestamp, label, printf } = format;
const path = require('path');
var appRoot = require('app-root-path');
var winston = require('winston');
const { combine, timestamp, label, json, prettyPrint } = winston.format;
var options = {
    file: {
        level: 'info',
        filename: path.join('../' + '/logs/backend.log'),
        handleExceptions: true,
        json: true,
        maxsize: 5242880, // 5MB
        maxFiles: 5,
        colorize: false,
    },
    console: {
        level: 'debug',
        handleExceptions: true,
        json: false,
        colorize: true,
    },
};

var logger = winston.createLogger({
    format: combine(
        timestamp(),
        json()
    ),
    transports: [
        new winston.transports.File(options.file),
        // new winston.transports.Console(options.console)
    ],
    exitOnError: false, // do not exit on handled exceptions
});
logger.stream = {
    write: function(message, encoding) {
        // use the 'info' log level so the output will be picked up by both transports (file and console)
        logger.info(message);
    },
};
module.exports = logger;