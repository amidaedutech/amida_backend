const formidable = require('formidable');
const fs = require('fs');
const path = require('path');
const http = require('http');
module.exports = {
    uploadFile: uploadFile,
    imageBase64: imageBase64,
    uploadHtmlCont: uploadHtmlCont,
    uploadDocs: uploadDocs,
    docsBase64: docsBase64
};

function uploadFile(req) {
    return new Promise((resolve, reject) => {
        let imagePath = '';
        const dataArray = [];
        let reqBody = {};
        const form = new formidable.IncomingForm();

        form.parse(req);
        form.on('field', function(name, value) {
            reqBody[name] = value;

            // if there is any id unlink first the previouse image
        });
        form.on('fileBegin', (name, file) => {
            imagePath = path.join('../' + '/SkillAdda_images/');
            if (!fs.existsSync(imagePath)) {
                fs.mkdirSync(imagePath);
            }
            const paths = req.body.folderPath.split('/');
            for (let path of paths) {
                imagePath += path + '/';
                if (!fs.existsSync(imagePath)) {
                    fs.mkdirSync(imagePath);
                }
            }
            form.uploadDir = imagePath;
            console.log('imagePath', imagePath);
            const filename = path.parse(file.name).name; // hello
            const ext = path.parse(file.name).ext; // .html
            const uniqueName = filename + Math.floor(100000 + Math.random() * 900000) + ext;
            file.path = imagePath + uniqueName;
            file.path = file.path.replace(/ /g, '_')
            dataArray.push(file.path);
        });
        form.on('file', function(name, file) {
            console.log('Uploaded ' + file.name);
        });
        form.on('error', (err) => {
            console.log(err)
            reject('Error while uploading Image on server, please try again');
        });

        form.on('aborted', () => {
            reject('Error while uploading Image on server, please try again');
        });
        form.on('end', () => {
            if ((reqBody || {})._id) {
                fs.unlinkSync(reqBody.imagePath);
            }
            resolve(dataArray);
        });
    });
}

function uploadDocs(req) {
    return new Promise((resolve, reject) => {
        let imagePath = '';
        const dataArray = [];
        let reqBody = {};
        const form = new formidable.IncomingForm();

        form.parse(req);
        form.on('field', function(name, value) {
            reqBody[name] = value;

            // if there is any id unlink first the previouse image
        });
        form.on('fileBegin', (name, file) => {
            console.log('name', name);
            console.log('file', file);
            imagePath = path.join('../' + '/SkillAdda_images/');
            if (!fs.existsSync(imagePath)) {
                fs.mkdirSync(imagePath);
            }
            const paths = name.split('/');
            for (let path of paths) {
                imagePath += path + '/';
                if (!fs.existsSync(imagePath)) {
                    fs.mkdirSync(imagePath);
                }
            }
            form.uploadDir = imagePath;
            const filename = path.parse(file.name).name; // hello
            const ext = path.parse(file.name).ext; // .html
            const uniqueName = filename + ext;
            file.path = imagePath + uniqueName;
            dataArray.push(file.path);
        });
        form.on('file', function(name, file) {});
        form.on('error', (err) => {
            console.log(err)
            reject('Error while uploading Image on server, please try again');
        });

        form.on('aborted', () => {
            reject('Error while uploading Image on server, please try again');
        });
        form.on('end', () => {
            resolve({ docs: dataArray, _id: (reqBody || {})._id });
        });
    });
}

function imageBase64(filename) {
    return new Promise((resolve, reject) => {
        if (!filename) {
            fs.readFile('./public/images/inf.png', (err, data) => {
                if (err) {
                    resolve('image');
                } else {
                    const buf = Buffer.from(data);
                    const base64 = buf.toString('base64');
                    const imageFile = 'data:Image/*;base64,' + base64;
                    resolve(imageFile);
                }
            });
        } else {
            fs.readFile(path.join(filename), (err, data) => {
                if (err) {
                    fs.readFile('./public/images/inf.png', (error, datanext) => {
                        if (error) {
                            resolve('image');
                        } else {
                            const buf = Buffer.from(datanext);
                            const base64 = buf.toString('base64');
                            const imageFile = 'data:Image/*;base64,' + base64;
                            resolve(imageFile);
                        }
                    });
                } else {
                    const buf = Buffer.from(data);
                    const base64 = buf.toString('base64');
                    const imageFile = 'data:Image/*;base64,' + base64;
                    resolve(imageFile);
                }
            });
        }
    });
}

function docsBase64(filename) {
    return new Promise(function(resolve, reject) {
        fs.readFile(filename,
            function(error, data) {
                if (error) {
                    reject('Unable to fetch file, please try again');
                } else {
                    var buf = Buffer.from(data);
                    resolve(buf);
                }
            });
    });
}

function uploadHtmlCont(req) {
    return new Promise((resolve, reject) => {
        var docObj = {};
        let imagePath = '';
        let reqBody = {};
        const form = new formidable.IncomingForm();
        form.parse(req);
        form.on('field', function(name, value) {
            reqBody[name] = value;
        });
        form.on('fileBegin', (name, file) => {
            imagePath = path.join('../' + '/temp_zip/');
            if (!fs.existsSync(imagePath)) {
                fs.mkdirSync(imagePath);
            }
            imagePath = path.join('../' + '/temp_zip/htmlcontent/');
            if (!fs.existsSync(imagePath)) {
                fs.mkdirSync(imagePath);
            }
            let unzipFolder = path.join('../' + '/SkillAdda_images/');
            if (!fs.existsSync(unzipFolder)) {
                fs.mkdirSync(unzipFolder);
            }
            unzipFolder = path.join('../' + '/SkillAdda_images/htmlcontentunzip/');
            if (!fs.existsSync(unzipFolder)) {
                fs.mkdirSync(unzipFolder);
            }
            form.uploadDir = imagePath;
            file.path = imagePath + file.name;
            let filename = path.parse(file.name).name; // hello
            let fpath = unzipFolder + filename;
            filename = 'htmlcontentunzip/' + filename + '/index.html';
            // const ext = path.parse(file.name).ext; // .html

            docObj = { name: file.name, linkpoint: filename, path: file.path, folderpath: unzipFolder, htmlpath: fpath };
        });
        form.on('file', function(name, file) {});
        form.on('error', (err) => {
            console.log(err)
            reject('Error while uploading Image on server, please try again');
        });

        form.on('aborted', () => {
            reject('Error while uploading Image on server, please try again');
        });
        form.on('end', () => {
            console.log('docObj', docObj);
            resolve(docObj);
        });
    });
}